$jq=jQuery.noConflict();
var CURRENT_PRODUCT_ID = '';
var urlUpdateVarnish = '/varnish/update/index';
var Ebvarnish	=	function(){
    return {
        onUpdate:function(){
            new Ajax.Request(urlUpdateVarnish,
                {parameters:'',
                    method:'POST',
                    contentType: "application/json",
                    dataType: "text",
                    onLoading:function(data){
                    },
                    onComplete:function(data){
                        if(200!=data.status){
                            $jq("#top .user-menu").show();
                            return false;
                        }else{
                            var htmls = data.responseText.evalJSON();
                            for(var property in htmls){
                                if ('formkey' === property) {
                                    var real_key = htmls[property];
                                    $$('input[name="form_key"]').each(function(e){e.value=real_key});
                                    $$('form[action*="form_key"]').each(function(e){
                                        actionurl=e.action;
                                        actionurl=actionurl.replace(/\/form_key\/[^\/]+/, '/form_key/'+real_key);
                                        actionurl=actionurl.replace(/([\&\?])form_key=([^\&])/, '$1form_key='+real_key + '$2');
                                        //console.log(actionurl);
                                        e.action=actionurl;
                                    });
                                    $$('a[href*="form_key"]').each(function(e){
                                        link=e.href;
                                        link=link.replace(/\/form_key\/[^\/]+/, '/form_key/'+real_key);
                                        link=link.replace(/([\&\?])form_key=([^\&])/, '$1form_key='+real_key + '$2');
                                        //console.log(link);
                                        e.href=link;
                                    });
                                    $$('form').each(function(e){e.enable();});
                                }
                                if('usermenu' === property){
                                    $jq("#top .user-menu").html(htmls[property]);
                                    $jq("#top .user-menu").css('opacity', 1);

                                }

                                if('related_download' === property){
                                    $jq(".related-download-std").html(htmls[property]);
                                }
                                if('count_down' === property){
                                    $jq("#product-count-down").html(htmls[property]);
                                }
                                if('globalmsg' === property || 'msg' === property){
                                    if (htmls['msg'] != '' && 'globalmsg' === property) {
                                        continue;
                                    }
                                    if ($jq('.col-main ul.messages').length<=0) {
                                        $jq('.main.container').prepend(htmls[property]);
                                    } else {
                                        if (htmls[property] != '') {
                                            $jq('.col-main ul.messages').replaceWith(htmls[property]);
                                        } else {
                                            /* @todo improve this point if duplicate message some pages*/
                                            if ($jq('.contacts-index-index').length<=0 && $jq('.newsletter-subscriber-form').length<=0) {
                                                $jq('.col-main ul.messages').remove();
                                            }
                                        }
                                    }
                                }
                            }
                            jQuery('#product_addtocart_form button').each(function(){
                                jQuery(this).removeAttr('disabled');
                                jQuery(this).removeClass('disabled');
                            });
                            $jq('.col-main ul.messages').show();
                        }
                    }

                });
        }
    }
}();
Prototype.Browser.IE?Event.observe(window,"load",function(){if (CURRENT_PRODUCT_ID!=''){urlUpdateVarnish+='/id/'+CURRENT_PRODUCT_ID;}Ebvarnish.onUpdate()}):document.observe("dom:loaded",function(){if (CURRENT_PRODUCT_ID!=''){urlUpdateVarnish+='/id/'+CURRENT_PRODUCT_ID;}Ebvarnish.onUpdate()});
