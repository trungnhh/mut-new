-- MySQL dump 10.17  Distrib 10.3.16-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: mutdb
-- ------------------------------------------------------
-- Server version	10.3.16-MariaDB-1:10.3.16+maria~bionic

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `eadesign_pdfgenerator_template`
--

DROP TABLE IF EXISTS `eadesign_pdfgenerator_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eadesign_pdfgenerator_template` (
  `pdftemplate_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pdftemplate_name` varchar(255) NOT NULL DEFAULT '',
  `pdftemplate_desc` varchar(255) NOT NULL DEFAULT '',
  `pdftemplate_body` mediumtext NOT NULL,
  `pdft_type` varchar(255) NOT NULL DEFAULT '',
  `pdft_filename` varchar(100) NOT NULL DEFAULT '',
  `pdftp_format` varchar(255) NOT NULL DEFAULT '',
  `pdft_orientation` varchar(255) NOT NULL DEFAULT '',
  `created_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `template_store_id` smallint(5) unsigned NOT NULL DEFAULT 0 COMMENT 'The store id',
  `pdft_is_active` smallint(5) unsigned NOT NULL DEFAULT 0 COMMENT 'Active setting',
  `pdft_default` smallint(5) unsigned NOT NULL DEFAULT 0 COMMENT 'Default setting',
  `pdfth_header` text NOT NULL COMMENT 'Header body',
  `pdftf_footer` text NOT NULL COMMENT 'Footer body',
  `pdft_css` text NOT NULL COMMENT 'Css body',
  `pdftc_customchek` smallint(5) unsigned NOT NULL DEFAULT 0 COMMENT 'Paper custom check',
  `pdft_customwidth` decimal(12,4) NOT NULL DEFAULT 0.0000 COMMENT 'Paper custom Width',
  `pdft_customheight` decimal(12,4) NOT NULL DEFAULT 0.0000 COMMENT 'Paper custom Height',
  `pdftm_top` decimal(12,4) NOT NULL DEFAULT 0.0000 COMMENT 'Paper margins top',
  `pdftm_bottom` decimal(12,4) NOT NULL DEFAULT 0.0000 COMMENT 'Paper margins bottom',
  `pdftm_left` decimal(12,4) NOT NULL DEFAULT 0.0000 COMMENT 'Paper margins left',
  `pdftm_right` decimal(12,4) NOT NULL DEFAULT 0.0000 COMMENT 'Paper margins right',
  PRIMARY KEY (`pdftemplate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eadesign_pdfgenerator_template`
--

LOCK TABLES `eadesign_pdfgenerator_template` WRITE;
/*!40000 ALTER TABLE `eadesign_pdfgenerator_template` DISABLE KEYS */;
INSERT INTO `eadesign_pdfgenerator_template` VALUES (1,'Invoice Template','The template for all stores.','<table  style=\"width:100%;border-color: #000000; border-width: 1px;\">\r\n    <tr style=\"border-top: 0px;border-bottom: 0px;\">\r\n        <td align=\"left\">\r\n            <p style=\"text-align: left;\">{{var billing_address}}</p>\r\n        </td>\r\n        <td></td>\r\n    </tr>\r\n</table>\r\n<p>&nbsp;</p>\r\n<table style=\"width:100%\" class=\"no-border\">\r\n    <tr style=\"border-top: 1px solid #000; border-bottom: 1px solid #000;border-collapse:collapse\" class=\"bottom-border\">\r\n        <td align=\"left\"><h1>RECHNUNG <span style=\"font-size: 18px;font-weight: normal;\">Nr.: {{var ea_invoice_id}}</span></h1></td>\r\n        <td align=\"left\" class=\"barcode\"></td>\r\n    </tr>\r\n    <tr>\r\n        <td align=\"left\">\r\n            <p>Datum:&nbsp;{{var ea_invoice_date}}</p>\r\n            {{var ccustomer_id}}\r\n            <p>Bestell-Nummer: &nbsp;{{var ea_order_number}}</p>\r\n            <p>Zahlungskonditionen:&nbsp;{{var billing_method_text}}</p>\r\n            <p>&nbsp;</p>\r\n            <p>Lieferdatum entspricht Rechnungsdatum</p>\r\n            <p>Die Ware bleibt bis zur vollständigen Bezahlung Eigentum der Fa. Markt+Technik Verlag GmbH</p>\r\n        </td>\r\n        <td></td>\r\n    </tr>\r\n</table>\r\n\r\n<table style=\"width:100%;border-color: #000000; border-width: 1px;\">\r\n    <thead>\r\n    <tr style=\"border-collapse:collapse;border-bottom: 1px solid #000;\">\r\n        <th>Pos</th>\r\n        <th>Menge</th>\r\n        <th>ME</th>\r\n        <th>Artikel</th>\r\n        <th colspan=\"2\">Bezeichnung</th>\r\n        <th>E-Preis</th>\r\n        <th>MwSt.</th>\r\n        <th>Gesamt</th>\r\n    </tr>\r\n    </thead>\r\n    <tbody>\r\n    <tr style=\"border:0;\">\r\n        <td colspan=\"11\">##productlist_start##</td>\r\n    </tr>\r\n    <tr style=\"border:0\">\r\n        <td height=\"30\">{{var items_position}}</td>\r\n        <td  height=\"30\" align=\"center\">\r\n            <p>{{var items_qty}}</p>\r\n        </td>\r\n        <td height=\"30\" align=\"center\">Stück</td>\r\n        <td height=\"30\" align=\"center\"><p>{{var items_sku}}</p></td>\r\n        <td height=\"30\" colspan=\"2\" align=\"center\">\r\n            <p>{{var items_name}}</p>\r\n            <p>{{var bundle_items_option}}</p>\r\n            <p>{{var product_options}}</p>\r\n        </td>\r\n        <td height=\"30\" align=\"center\">\r\n            <p>{{var itemcarptice}}</p>\r\n        </td>\r\n        <td height=\"30\" align=\"center\">\r\n            <p><span>{{var items_tax_percent}}</span></p>\r\n        </td>\r\n        <td align=\"center\"><p>{{var itemcarpticeicl}}</p></td>\r\n    </tr>\r\n    <tr>\r\n        <td colspan=\"11\">##productlist_end##</td>\r\n    </tr>\r\n    </tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n\r\n<table style=\"width: 30%\" align=\"right\">\r\n    <tr>\r\n        <td colspan=\"10\" align=\"left\">Zwischensumme</td>\r\n        <td align=\"right\">{{var subtotalincludingtax}}</td>\r\n    </tr>\r\n{{var discountammount}}\r\n    <tr>\r\n        <td colspan=\"10\" align=\"left\">Versandkosten</td>\r\n        <td align=\"right\">{{var shipping_amount}}</td>\r\n    </tr>\r\n    <tr>\r\n        <td colspan=\"10\" align=\"left\">Gesamtsumme (netto)</td>\r\n        <td align=\"right\">{{var grandtotalexcludingtax}}</td>\r\n    </tr>\r\n    {{var all_taxes}}\r\n    <tr>\r\n        <td colspan=\"10\" align=\"left\">Gesamtsumme (inkl. Mwst.)</td>\r\n        <td align=\"right\">{{var grandtotalincludingtax}}</td>\r\n    </tr>\r\n</table>','1','MuT_{{var ea_invoice_date}}_{{var ea_invoice_id}}','0','portrait','2013-05-13 08:22:18','2015-08-29 05:54:40',0,1,0,'<div style=\"width: 50%;display:block;float:left;clear: both;border-collapse: collapse;padding-bottom:5px\" border=\"1\" class=\"no-border\">\r\n<p style=\"border-top:0px; border-bottom: 1px solid #000;\">\r\n    Markt+Technik Verlag GmbH<br/>\r\n    Espenpark 1a<br/>\r\n    DE 90559 Burgthann<br/>\r\n    Telefon: +49 9188 307 12-0<br/>\r\n    Telefax: +49 9188 307 12-29<br/>\r\n<br/>\r\n</p>\r\n</div>\r\n\r\n<div style=\"float:right\">\r\n    <p style=\"float:right;text-align:right;\">{{var ea_logo_store}}</p>\r\n</div>\r\n','<table style=\"width: 100%; font-size:10px; border-top:1px solid #000\">\r\n    <tr>\r\n        <td>\r\n            Bankverbindung: Oberbank Nürnberg<br />\r\n            BLZ: 70120700 • Konto: 108 115 5226<br />\r\n            IBAN: DE41701207001081155226<br />\r\n            BIC: OBKLDEMXXXX<br />\r\n        </td>\r\n        <td>\r\n            Geschäftsführer: Peter Braun, Christian Braun<br />\r\n            Mail: info@mut.de<br />\r\n            Web: www.mut.de<br />\r\n            EORI-Nummer: DE689182340489646<br />\r\n        </td>\r\n        <td>\r\n            Ust-Id: DE294617659<br />\r\n            Registergericht: Nürnberg<br />\r\n            Amtsgericht: Nürnberg<br />\r\n            HRB: 30574<br />\r\n        </td>\r\n    </tr>\r\n</table>','table {\r\nborder-collapse:collapse !important;\r\ncellspacing: 1;\r\n}\r\n\r\n.tbl {background-color:#000;}\r\n.tbl td,th,caption{background-color:#fff}\r\ntr {padding-bottom: 1px}\r\n\r\ntr {\r\nborder: 1px solid #000;\r\nborder-left: 0;\r\nborder-right: 0;\r\n}\r\ntd {\r\nborder:0px;\r\n}\r\n\r\n.no-border tr {\r\nborder: 0;\r\n}',0,1.0000,1.0000,50.0000,20.0000,10.0000,10.0000);
/*!40000 ALTER TABLE `eadesign_pdfgenerator_template` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-11 11:52:02
