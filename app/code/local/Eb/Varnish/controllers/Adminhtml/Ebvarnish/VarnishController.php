<?php

class Eb_Varnish_Adminhtml_Ebvarnish_VarnishController extends Mage_Adminhtml_Controller_Action {
    public function purgeAction() {
        $adminSession = $this->_getAdminSession();
        $helper = Mage::helper('eb_varnish');
        try {
            $isEnabled = $helper->isEnabled();
            if (true === $isEnabled) {
                //shell_exec('varnishadm -T $1:6082 ban.url "^/.*"');
                //shell_exec('varnishadm -T $1:6082 ban.url "^.*"');
                shell_exec('varnishadm ban.url "^/.*"');
                shell_exec('varnishadm ban.url "^.*"');
                $adminSession->addSuccess($helper->__('Varnish has been purged.'));
            }
        }
        catch(Mage_Core_Exception $e) {
            $adminSession->addError($e->getMessage());
        }
        catch(Exception $e) {
            $adminSession->addException($e, $helper->__('An error occurred while clearing the Varnish cache.'));
        }
        $this->_redirect('*/cache/index');
    }

    protected function _getAdminSession() {
        return Mage::getSingleton('adminhtml/session');
    }
}