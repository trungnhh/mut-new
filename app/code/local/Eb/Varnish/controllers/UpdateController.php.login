<?php
class Eb_Varnish_UpdateController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $this->loadLayout('default');
        $this->getLayout()->getUpdate()->addHandle('checkout_cart_index');
        $blocks = array();
        $userMenu = $this->getLayout()->getBlock('header');
        if(is_object($userMenu)){
            $blocks['usermenu'] = $userMenu->setTemplate('page/html/header_varnish.phtml')->toHtml();
        }
        $blocks['formkey'] = Mage::getSingleton('core/session')->getFormKey();
        @header('Content-type: application/json');
        echo json_encode($blocks);
        exit;
    }

    public function pdpUpdateAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $id = $this->getRequest()->getParams('id');
        $block = Mage::getBlockSingleton('files/product_view');
        $response = array();
        if(is_object($block)){
            $block->setProductId($id);
            $response['related_download'] = $block->setTemplate('files/files.phtml')->toHtml();
        }
        $response['formkey'] = Mage::getSingleton('core/session')->getFormKey();
        @header('Content-type: application/json');
        echo json_encode($response);
        exit;
    }
    public function tryloginAction(){
	$customer = Mage::getModel('customer/customer')->load(21819);
    if ($customer->getWebsiteId()) {
//        Mage::init($customer->getWebsiteId(), 'website');
       $session = Mage::getSingleton('customer/session');
       $session->loginById(21819);
        return $session;
    }
//    throw new Exception('Login failed');
}
}
