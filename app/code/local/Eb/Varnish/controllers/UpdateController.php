<?php
class Eb_Varnish_UpdateController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $id = $this->getRequest()->getParams('id');
        $handleUpdate = 'checkout_cart_index';
        if ($id) {
            $handleUpdate = 'catalog_product_view';
        }
        if (!is_object(Mage::registry('current_product'))) {
            $product = Mage::getModel('catalog/product')->load($id);
            Mage::register('current_product', $product);
            Mage::register('product', $product);
        }

        $this->loadLayout('default');
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
        $this->getLayout()->getUpdate()->addHandle($handleUpdate);

        $blocks = array();
        $userMenu = $this->getLayout()->getBlock('header');
        if(is_object($userMenu)){
            $blocks['usermenu'] = $userMenu->setTemplate('page/html/header_varnish.phtml')->toHtml();
        }
        $block = $this->getLayout()->createBlock('jimmy_countdown/countDown');
        if(is_object($block) && $id) {
            $blocks['count_down'] = $block->toHtml();
        }

        $block = Mage::getBlockSingleton('files/product_view');
        if(is_object($block) && $id){
            $block->setProductId($id);
            $blocks['related_download'] = $block->setTemplate('files/files.phtml')->toHtml();
        }

        
        $globalMessages = $this->getLayout()->getBlock('global_messages');
        $message = $this->getLayout()->getBlock('messages');
        if(is_object($globalMessages)){
            $blocks['globalmsg'] = $globalMessages->toHtml();
        }
        if(is_object($message)){
            $blocks['msg'] = $message->toHtml();
        }
        $blocks['formkey'] = Mage::getSingleton('core/session')->getFormKey();
        @header('Content-type: application/json');
        echo json_encode($blocks);
        exit;
    }

    public function pdpUpdateAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $id = $this->getRequest()->getParams('id');
        $block = Mage::getBlockSingleton('files/product_view');
        $response = array();
        if(is_object($block)){
            $block->setProductId($id);
            $response['related_download'] = $block->setTemplate('files/files.phtml')->toHtml();
        }
        $response['formkey'] = Mage::getSingleton('core/session')->getFormKey();
        @header('Content-type: application/json');
        echo json_encode($response);
        exit;
    }
}