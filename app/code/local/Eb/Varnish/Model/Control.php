<?php

class Eb_Varnish_Model_Control
{
    const XML_PATH_VARNISH_SERVERS = 'system/varnish/servers';
    const XML_PATH_VARNISH_PORT    = 'system/varnish/port';

    const CONTENT_TYPE_HTML = 'text/html';
    const CONTENT_TYPE_CSS = 'text/css';
    const CONTENT_TYPE_JS = 'javascript';
    const CONTENT_TYPE_IMAGE = 'image/';

    const VARNISH_HEADER_REGEX = 'X-Purge-Regex';
    const VARNISH_HEADER_HOST = 'X-Purge-Host';
    const VARNISH_HEADER_CONTENT_TYPE = 'X-Purge-Content-Type';


    /**
     * Get content types as option array.
     *
     * @return array
     */
    public function getContentTypes()
    {
        $contentTypes = array(
            self::CONTENT_TYPE_HTML     => Mage::helper('eb_varnish')->__('HTML'),
            self::CONTENT_TYPE_CSS      => Mage::helper('eb_varnish')->__('CSS'),
            self::CONTENT_TYPE_JS       => Mage::helper('eb_varnish')->__('JavaScript'),
            self::CONTENT_TYPE_IMAGE    => Mage::helper('eb_varnish')->__('Images')
        );

        return $contentTypes;
    }


    /**
     * Clean Varnish cache.
     *
     * @param string $domains     Names for cleaning.
     * @param string $urlRegEx    Pattern for url matching.
     * @param string $contentType Type to clean.
     *
     * @return  void
     */
    public function clean($domains, $urlRegEx = '.*', $contentType = '.*')
    {
        try {
            $headers = array(
                            self::VARNISH_HEADER_HOST   =>  '^('.str_replace('.', '.', $domains).')$',
                            self::VARNISH_HEADER_REGEX  =>  (empty($urlRegEx) ? '.*' : $urlRegEx),
                            self::VARNISH_HEADER_CONTENT_TYPE => (empty($contentType) ? '.*' : $contentType)
                        );

            $this->_purgeCacheServers($headers);

            Mage::helper('eb_varnish')->debug('Purged Varnish items with parameters '.var_export($headers, true));
        } catch (Exception $e) {
            Mage::helper('eb_varnish')->debug('Error during purging: '.$e->getMessage());
            return false;
        }

        return true;
    }


    /**
     * Process all servers.
     *
     * @param array $headers The headers.
     *
     * @throws Exception
     *
     * @return void
     */
    protected function _purgeCacheServers(Array $headers)
    {
        $servers = $this->_getVarnishServers();
        if (empty($servers)) {
            return;
        }

        // process all servers
        foreach ($servers as $server) {
            // compile url string with scheme, domain/server and port
            $uri = 'http://'.$server;
            if ($port = trim(Mage::getStoreConfig(self::XML_PATH_VARNISH_PORT))) {
                $uri .= ':'.$port;
            }
            $uri .= '/';

            try {
                // create HTTP client
                $client = new Zend_Http_Client();
                $client->setUri($uri)
                    ->setHeaders($headers)
                    ->setConfig(array('timeout'=>15));

                // send PURGE request
                $response = $client->request('PURGE');

                // check response
                if ($response->getStatus() != '200') {
                    throw new Exception('Return status '.$response->getStatus());
                }
            } catch (Exception $e) {
                Mage::helper('eb_varnish')->debug('Purging on server '.$server.' failed ('.$e->getMessage().').');
            }
        }
    }

    /**
     * Get Varnish servers for purge.
     *
     * @return string
     */
    protected function _getVarnishServers()
    {
        return explode(';', Mage::getStoreConfig(self::XML_PATH_VARNISH_SERVERS));
    }
}
