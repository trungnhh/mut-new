<?php

class Eb_Varnish_Model_Catalog_Category_Product extends Mage_Core_Model_Abstract
{
    /**
     * Constructor.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('eb_varnish/catalog_category_product');
    }
}
