<?php

class Eb_Varnish_Model_Cms_Page_Store extends Mage_Core_Model_Abstract
{
    /**
     * Constructor.
     *
     * @return void.
     */
    protected function _construct()
    {
        $this->_init('eb_varnish/cms_page_store');
    }
}
