<?php

class Eb_Varnish_Block_Core_Messages extends Mage_Core_Block_Messages
{
    /**
     * Get Html value based on Ajax config.
     *
     * @return string
     */
    public function getGroupedHtml()
    {
        /*if (!Mage::helper('eb_varnish')->isAjax()
            && !Mage::helper('eb_varnish')->shouldSkip()
            && !Mage::app()->getStore()->isAdmin()
        ) {
            return '';
        }
        return parent::getGroupedHtml();
        */
        if (Mage::helper('eb_varnish')->shouldSkip() || Mage::app()->getStore()->isAdmin()) {
            return parent::getGroupedHtml();
        } else {
            return '';
        }
    }

    /**
     * Rewrite prepareLayout based on ajax configuration.
     *
     * @return void
     */
    public function _prepareLayout()
    {
        if (Mage::helper('eb_varnish')->shouldSkip() || Mage::app()->getStore()->isAdmin()) {
            parent::_prepareLayout();
        }
    }
}
