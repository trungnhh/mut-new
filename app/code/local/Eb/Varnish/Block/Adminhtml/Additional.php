<?php

class Eb_Varnish_Block_Adminhtml_Additional extends Mage_Adminhtml_Block_Template
{
    public function getPurgeUrl()
    {
        return $this->getUrl('adminhtml/ebvarnish_varnish/purge');
    }
    
    public function isEnabled()
    {
        return Mage::helper('eb_varnish')->isEnabled();
    }
    
    public function getStoreOptions()
    {
        $options = array(array('value' => '', 'label' => Mage::helper('eb_varnish')->__('All stores')));
        $stores = Mage::getModel('adminhtml/system_config_source_store')->toOptionArray();
        return array_merge($options, $stores);
    }
}
