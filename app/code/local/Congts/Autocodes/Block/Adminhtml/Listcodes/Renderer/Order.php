<?php

class Congts_Autocodes_Block_Adminhtml_Listcodes_Renderer_Order extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value =  $row->getData($this->getColumn()->getIndex());
        $order = Mage::getModel('sales/order')->loadByIncrementId($value);
        $orderUrl = Mage::helper('adminhtml')->getUrl("adminhtml/sales_order/view", array('order_id'=> $order->getId()));
        return '<a target="_blank" href="' . $orderUrl . '">' . $value . '</a>';

    }
}