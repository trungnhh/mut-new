<?php

/**
 * Product Press Extension
 *
 * @category   Congts
 * @package    Congts_ProductPress
 * @author     CongTS
 */
class Congts_ProductPress_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * save product news image
     * @return bool
     */
    public function saveImage()
    {
        if (isset($_FILES['image']['name']) and (file_exists($_FILES['image']['tmp_name']))) {
            try {
                $path = Mage::getBaseDir('media') . DS . 'catalog' . DS . 'product' . DS . 'press' . DS;
                $uploader = new Mage_Core_Model_File_Uploader('image');
                $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                $uploader->setAllowRenameFiles(true);
                $result = $uploader->save($path);
                return $result['file'];
            } catch (Mage_Core_Exception $e) {
                Mage::log($e->getMessage());
                return false;
            } catch (Exception $e) {
                if ($e->getCode() != Mage_Core_Model_File_Uploader::TMP_NAME_EMPTY) {
                    Mage::logException($e);
                }
                return false;
            }
        }

        return false;
    }

    /**
     * get News Image
     *
     * @param $image
     * @return bool|string
     */
    public function getNewsImage($image)
    {
        if ($image) {
            $path = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'catalog' . DS . 'product' . DS . 'press' . DS . $image;
            return $path;
        }

        return false;
    }
}