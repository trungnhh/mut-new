<?php

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = Mage::getResourceModel('catalog/setup','catalog_setup');

$installer->startSetup();
$installer->updateAttribute('catalog_product', 'product_press', 'is_visible', '0');

$installer->endSetup();