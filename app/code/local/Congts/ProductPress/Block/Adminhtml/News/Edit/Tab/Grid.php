<?php

/**
 * Product Press Extension
 *
 * @category   Congts
 * @package    Congts_ProductPress
 * @author     CongTS
 */
class Congts_ProductPress_Block_Adminhtml_News_Edit_Tab_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setUseAjax(true);
        $this->setId('entity_id');
        $this->setDefaultSort('entity_id');
		$this->setDefaultFilter(array('press_news_products'=>1));
        $this->setSaveParametersInSession(false);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('type_id');

        $collection->addAttributeToSelect('price');
        $collection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner');
        $collection->joinAttribute('visibility', 'catalog_product/visibility', 'entity_id', null, 'inner');
        $collection->addAttributeToFilter('status', array('eq' => '1'));

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('press_news_products', array(
            'header_css_class' => 'a-center',
            'type' => 'radio',
            'html_name' => 'product_id',
            'values' => $this->_getSelectedProducts(),
            'align' => 'center',
            'index' => 'entity_id'
        ));

        $this->addColumn('entity_id', array(
            'header' => Mage::helper('catalog')->__('ID'),
            'sortable' => true,
            'width' => 60,
            'index' => 'entity_id'
        ));
        $this->addColumn('name', array(
            'header' => Mage::helper('catalog')->__('Name'),
            'index' => 'name'
        ));

        $this->addColumn('sku', array(
            'header' => Mage::helper('catalog')->__('SKU'),
            'width' => 80,
            'index' => 'sku'
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->_getData('grid_url') ? $this->_getData('grid_url') : $this->getUrl('*/*/products', array('_current' => true));
    }

    protected function _getSelectedProducts()
    {
        // Product Data
        $tm_id = $this->getRequest()->getParam('id');
        if (!isset($tm_id)) {
            $tm_id = 0;
        }

        $collection = Mage::getModel('congts_productpress/news')->getCollection();
        $collection->addFieldToFilter('id', $tm_id);

        $productIds = array();
        foreach ($collection as $obj) {
            $productIds[] = $obj->getProductId();
        }
        return $productIds;
    }

    /**
     * Add filter
     * @access protected
     * @param object $column
     * @return Congts_ProductPress_Block_Adminhtml_Catalog_Product_Edit_Tab_Press
     *
     */
    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'press_news_products') {
            $newsIds = $this->_getSelectedProducts();
            if (empty($newsIds)) {
                $newsIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', array('in' => $newsIds));
            } else {
                if ($newsIds) {
                    $this->getCollection()->addFieldToFilter('entity_id', array('nin' => $newsIds));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }
}