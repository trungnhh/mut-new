<?php

/**
 * Product Press Extension
 *
 * @category   Congts
 * @package    Congts_ProductPress
 * @author     CongTS
 */
class Congts_ProductPress_Block_Adminhtml_News_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _getModel()
    {
        return Mage::registry('congts_product_press_model');
    }

    protected function _getHelper()
    {
        return Mage::helper('congts_productpress');
    }

    protected function _getModelTitle()
    {
        return 'Press News';
    }

    protected function _prepareForm()
    {
        $model = $this->_getModel();
        $modelTitle = $this->_getModelTitle();
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save'),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
        ));

        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend' => $this->_getHelper()->__("$modelTitle Information"),
            'class' => 'fieldset-wide',
        ));

        if ($model && $model->getId()) {
            $modelPk = $model->getResource()->getIdFieldName();
            $fieldset->addField($modelPk, 'hidden', array(
                'name' => $modelPk,
            ));
        }
        $fieldset->addField('title', 'text', array(
            'name' => 'title',
            'label' => $this->_getHelper()->__('Title'),
            'class' => 'input-text required-entry',
            'required' => true,
        ));

        $fieldset->addType('image', 'Congts_ProductPress_Block_Adminhtml_News_Helper_Image');
        $fieldset->addField('image', 'image', array(
            'name' => 'image',
            'label' => $this->_getHelper()->__('Image'),
            'class' => '',
            'required' => false,
        ));

        $fieldset->addField('status', 'select', array(
            'name' => 'status',
            'label' => $this->_getHelper()->__('Status'),
            'class' => 'input-text required-entry',
            'required' => true,
            'values' => array(1 => 'Enable', 0 => 'Disable')
        ));

        $fieldset->addField('link', 'text', array(
            'name' => 'link',
            'label' => $this->_getHelper()->__('Link'),
            'class' => 'input-text',
            'required' => false,
        ));

        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();
        $fieldset->addField('description', 'editor', array(
            'name' => 'description',
            'label' => $this->_getHelper()->__('Description'),
            'class' => '',
            'required' => false,
            'config' => $wysiwygConfig
        ));

        if ($model) {
            $form->setValues($model->getData());
        }
        $this->setForm($form);

        return parent::_prepareForm();
    }

}
