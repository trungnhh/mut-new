<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->run("
  ALTER TABLE {$this->getTable('sales_flat_order')} ADD COLUMN `order_type` varchar(255);
");

$installer->endSetup();