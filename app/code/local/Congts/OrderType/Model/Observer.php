<?php

class Congts_OrderType_Model_Observer
{
    /**
     * hook to sales_order_save_before event
     * add value to order_type
     * @param $observer
     */
    public function salesOrderSaveBefore($observer)
    {

        $order = $observer->getEvent()->getOrder();
        $payment = $order->getPayment();

        $orderType = '';
        if ($this->_getOrderType($order)) {
            $orderType = $this->_getOrderType($order);
        }

        $orderPaymentCode = '';
        if ($payment->getMethod() == 'checkmo') {
            $orderPaymentCode = 'rechnung';
        } else if ($payment->getMethod() == 'banktransfer') {
            $orderPaymentCode = 'vorkasse';
        } else if (strpos($payment->getMethod(),'pnsofortueberweisung') !== false) {
            $orderPaymentCode = 'sofort';
        } else if ($payment->getMethod() == 'ccsave') {
            $orderPaymentCode = 'creditcard';
        } else if (strpos($payment->getMethod(),'paypal') !== false) {
            $orderPaymentCode = 'paypal';
        } else if (strpos($payment->getMethod(),'saferpaycw') !== false) {
            $orderPaymentCode = 'saferpay';
        } else if ($payment->getMethod() == 'vaimo_klarna_invoice') {
            $orderPaymentCode = 'klarnainvoice';
        } else if (strpos($payment->getMethod(),'m2epropayment') !== false) {
            $orderPaymentCode = 'ebay';
        }

        if ($orderPaymentCode != '') {
            $orderType .= '-' . $orderPaymentCode;
        }
        $order->setData('order_type',$orderType);
    }

    /**
     * hook to catalog_product_save_before event
     * re-update sku with logic: last digit of the company ISBN (isbn) and the first 3 digits of the product ISBN (isbn_product)
     *
     * @param $observer
     */
    public function catalogProductSaveBefore($observer)
    {

        $product = $observer->getEvent()->getProduct();

        /* update for ebay */
        $companyISBN = $product->getAttributeText('isbn');
        $productISBN = $product->getIsbnProduct();
        if ($product->getIsbn() !=''
            && $product->getIsbn()!=null
            && $product->getIsbn() !=' ') {
            $attributeSetModel = Mage::getModel("eav/entity_attribute_set");
            $attributeSetModel->load($product->getAttributeSetId());
            $attributeSetName = strtolower($attributeSetModel->getAttributeSetName());
            if (strtolower($attributeSetName) == 'buch' || strtolower($attributeSetName) == 'e-book') {
                if ($product->getEbayIsbn() == ''
                    || $product->getEbayIsbn() == null
                    || $product->getEbayIsbn() == ' '
                ) {
                    $product->setEbayIsbn($companyISBN.'-'.$productISBN);
                }
            } elseif($product->getEbayEan() == ''
                || $product->getEbayEan() == null
                || $product->getEbayEan() == ' '
            ) {
                $product->setEbayEan($companyISBN.'-'.$productISBN);
            }
        }

        if ($product->getOrigData('ebay_price')=='' || $product->getOrigData('ebay_price')==null || $product->getEbayPrice()=='') {
            $product->setEbayPrice($product->getPrice());
        }
        /* end update for ebay */


        $changeSku = 0;
        if ($product->getData('isbn') != $product->getOrigData('isbn')) {
            $changeSku = 1;
        }
        if ($product->getData('isbn_product') != $product->getOrigData('isbn_product')) {
            $changeSku = 1;
        }

        if($product->getSku() && $product->getSku() != '' && $changeSku == 0){
            return;
        }
        $companyISBN = $product->getIsbn();
        $productISBN = $product->getIsbnProduct();

        if ($companyISBN != '' && $productISBN != '') {
            $companyISBN = $product->getResource()->getAttribute('isbn')->getSource()->getOptionText($companyISBN);

            if(strpos($companyISBN, '-') !== false) {
                if($companyISBN == '978-3-945384'){
                    $isbnArr = explode('-',$companyISBN);
                    $isbnProductArr = explode('-',$productISBN);
                    $last = array_pop($isbnArr);
                    $first = reset($isbnProductArr);
                    $first2Chars = substr($first,0,2);
                    $last2Chars = substr($last, -2);
                    $newSku = $last2Chars.$first2Chars;
                }else{
                    $isbnArr = explode('-',$companyISBN);
                    $isbnProductArr = explode('-',$productISBN);
                    $last = array_pop($isbnArr);
                    $first = reset($isbnProductArr);
                    $first2Chars = substr($first,0,3);
                    $last2Chars = substr($last, -1);
                    $newSku = $last2Chars.$first2Chars;
                }
            } else {
                //EAN
                $isbnProductArr = explode('-',$productISBN);
                $newSku = $isbnProductArr[0];

            }
            if($product->getTypeId() == 'grouped'){
                $newSku = 'G-'.$newSku;
            }
            //$newSku = substr($companyISBN, -1) . substr($productISBN, 0, 3);
            $product->setSku($newSku);
        }
    }

    protected function _getOrderType($order)
    {
        $items = $order->getAllItems();
        $textBox = 'box';
        $textDownload = 'download';

        $isBox = false;
        $isDownload = false;
        foreach ($items as $item) {
            $product = Mage::getModel('catalog/product')->load($item->getProductId());
            $companyIsbn = $product->getIsbn();
            $isbn = $product->getIsbnProduct();
            if ($companyIsbn != '' && $isbn != '') {
                $productIsbn = $companyIsbn . $isbn;
                $item->setData('product_isbn', str_replace('-','', $productIsbn));
            }

            if ($item->getProductType() == 'simple') {
                $isBox = true;
                continue;
            }
            if ($item->getProductType() == 'downloadable') {
                $isDownload = true;
                continue;
            }
        }

        if ($isBox && $isDownload) return $textBox . '+' . $textDownload;
        if ($isBox) return $textBox;
        if ($isDownload) return $textDownload;

        return false;
    }
}