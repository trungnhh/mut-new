<?php

/**
 * Product Author Extension
 *
 * @category   Congts
 * @package    Congts_ProductAuthor
 * @author     CongTS
 */
class Congts_ProductAuthor_Model_Authors extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('congts_productauthor/authors');
    }

}