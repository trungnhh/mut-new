<?php

/**
 * Product Author Extension
 *
 * @category   Congts
 * @package    Congts_ProductAuthor
 * @author     CongTS
 */
class Congts_ProductAuthor_Block_Adminhtml_Authors extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_blockGroup = 'congts_productauthor';
        $this->_controller = 'adminhtml_authors';
        // $this->_headerText      = $this->__('Grid Header Text');
        // $this->_addButtonLabel  = $this->__('Add Button Label');
        parent::__construct();
    }

    public function getCreateUrl()
    {
        return $this->getUrl('*/*/new');
    }

}

