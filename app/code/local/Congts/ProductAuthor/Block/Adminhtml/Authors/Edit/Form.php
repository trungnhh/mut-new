<?php

/**
 * Product Author Extension
 *
 * @category   Congts
 * @package    Congts_ProductAuthor
 * @author     CongTS
 */
class Congts_ProductAuthor_Block_Adminhtml_Authors_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Load Wysiwyg on demand and Prepare layout
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
    }

    protected function _getModel()
    {
        return Mage::registry('congts_productauthor_model');
    }

    protected function _getHelper()
    {
        return Mage::helper('congts_productauthor');
    }

    protected function _getModelTitle()
    {
        return 'Product Author';
    }

    protected function _prepareForm()
    {
        $model = $this->_getModel();
        $modelTitle = $this->_getModelTitle();
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save'),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
        ));

        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend' => $this->_getHelper()->__("$modelTitle Information"),
            'class' => 'fieldset-wide',
        ));

        if ($model && $model->getId()) {
            $modelPk = $model->getResource()->getIdFieldName();
            $fieldset->addField($modelPk, 'hidden', array(
                'name' => $modelPk,
            ));
        }
        $fieldset->addField('name', 'text', array(
            'name' => 'name',
            'label' => $this->_getHelper()->__('Name'),
            'class' => 'input-text required-entry',
            'required' => true,
        ));

        $fieldset->addType('image', 'Congts_ProductAuthor_Block_Adminhtml_Authors_Helper_Image');
        $fieldset->addField('image', 'image', array(
            'name' => 'image',
            'label' => $this->_getHelper()->__('Image'),
            'class' => '',
            'required' => false,
        ));

        $fieldset->addField('status', 'select', array(
            'name' => 'status',
            'label' => $this->_getHelper()->__('Status'),
            'class' => 'input-text required-entry',
            'required' => true,
            'values'=>array(1=>'Enable',0=>'Disable')
        ));

        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();
        $fieldset->addField('description', 'editor', array(
            'name' => 'description',
            'label' => $this->_getHelper()->__('Description'),
            'class' => '',
            'required' => false,
            'config' => $wysiwygConfig
        ));

        if ($model) {
            $form->setValues($model->getData());
        }
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

}
