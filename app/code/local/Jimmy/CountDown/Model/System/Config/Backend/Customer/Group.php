<?php
class Jimmy_CountDown_Model_System_Config_Backend_Customer_Group extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    public function getAllOptions()
    {
        return Mage::getResourceModel('customer/group_collection')->toOptionArray();
    }

    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
}