<?php
class Jimmy_CountDown_Block_Adminhtml_Catalog_Product_CountDown extends Mage_Adminhtml_Block_Widget
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * Set the template for the block
     *
     */
    public function _construct()
    {
        parent::_construct();

        $this->setTemplate('countdown/countdown.phtml');
    }

    public function _getProduct()
    {
        return Mage::registry('current_product');
    }

    public function _getStockInfo()
    {
        $product = $this->_getProduct();
        if(!$product->getId()){
            return null;
        }
        if($product->getTypeId() != Mage_Catalog_Model_Product_Type::TYPE_GROUPED){
            $qty = $product->getStockItem()->getQty();
            return (int)$qty;
        }else{
            $result = array();
            $children = $product->getTypeInstance(true)
                ->getAssociatedProducts($product);
            foreach($children as $item){
                $result[$item->getSku()] = (int)$item->getStockItem()->getQty();
            }
            return $result;
        }
    }

    public function getCountdownStatus()
    {
        return $this->_getProduct()->getData('countdown_enabled');
    }

    public function getCountdownType()
    {
        return $this->_getProduct()->getData('countdown_type');
    }

    public function getCountdownCustomerGroups()
    {
        $countdown_customer_groups = $this->_getProduct()->getData('countdown_customer_groups');
        if($countdown_customer_groups){
            $r = explode(',',$countdown_customer_groups);
            return $r;
        }
    }

    public function getCountdownPrice()
    {
        $product = $this->_getProduct();
        if($product->getTypeId() != Mage_Catalog_Model_Product_Type::TYPE_GROUPED){
            if ($product->getSpecialPrice() == '' || $product->getSpecialPrice() == null) {
                return $product->getSpecialPrice();
            } else {
                return number_format($product->getSpecialPrice(), '2', '.', ',');
            }
            //return $product->getSpecialPrice();
        }else{
            if ($product->getData('countdown_price') == null || $product->getData('countdown_price') == '') {
                return $product->getData('countdown_price');
            } else {
                return number_format($product->getData('countdown_price'), '2', '.', ',');
            }
            //return $product->getData('countdown_price');
        }
    }

    public function getCountdownFrom()
    {
        return $this->_getProduct()->getData('countdown_from');
    }

    public function getCountdownTo()
    {
        return $this->_getProduct()->getData('countdown_to');
    }
    /**
     * Retrieve the label used for the tab relating to this block
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->__('Count Down');
    }

    /**
     * Retrieve the title used by this tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->__('Count Down');
    }

    /**
     * Determines whether to display the tab
     * Add logic here to decide whether you want the tab to display
     *
     * @return bool
     */
    public function canShowTab()
    {
        $product = Mage::registry('current_product');

        if ($product) {
            return true;
        }
        return false;
    }

    /**
     * Stops the tab being hidden
     *
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    public function getFieldSuffix()
    {
        return 'countdown';
    }
}