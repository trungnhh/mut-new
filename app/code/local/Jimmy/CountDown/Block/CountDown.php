<?php
class Jimmy_CountDown_Block_CountDown extends Mage_Core_Block_Template
{
    public function _construct()
    {
        parent::_construct();

        $this->setTemplate('catalog/product/view/countdown.phtml');
    }

    public function _getProduct()
    {
        return Mage::registry('current_product');
    }

    public function getTimeLeft()
    {
        $toDate = $this->_getProduct()->getData('countdown_to');
        $now = Mage::app()->getLocale()->date();
        $timeDiff = strtotime('+1 day', strtotime($toDate)) - strtotime($now->toString(Varien_Date::DATETIME_INTERNAL_FORMAT));
        return $timeDiff;
    }

    public function isShowCountdown()
    {
        if($this->_getProduct()->getData('countdown_enabled') != 1){
            return false;
        }
        if($this->_getProduct()->getData('countdown_type') && ($this->_getProduct()->getData('countdown_type') != 3)){
            $fromDate = $this->_getProduct()->getData('countdown_from');
            $toDate = $this->_getProduct()->getData('countdown_to');
            $now = Mage::app()->getLocale()->date();
            $timeDiff = strtotime($now->toString(Varien_Date::DATETIME_INTERNAL_FORMAT)) - strtotime($fromDate);
            if($timeDiff < 0){
                return false;
            }
            $toTimeDiff = strtotime('+1 day', strtotime($toDate)) - strtotime($now->toString(Varien_Date::DATETIME_INTERNAL_FORMAT));
            if($toTimeDiff < 0){
                return false;
            }
        }
        return true;
    }

    public function getCountdownType()
    {
        return $this->_getProduct()->getData('countdown_type');
    }

    public function getDiscountInformation()
    {
        $product = $this->_getProduct();
        if($product->getTypeId() != Mage_Catalog_Model_Product_Type::TYPE_GROUPED){
            return $product->getSpecialPrice();
        }else{
            $result = array();
            $result['label'] = Mage::helper('catalog')->__('Discount percent: ');
            $result['value'] = $product->getCountdownPrice();
            return $result;
        }
    }

    public function checkCustomerGroupAccess()
    {
        $groups = $this->_getProduct()->getData('countdown_customer_groups');
        if(!$groups){
            return true;
        }
        $currentGroup = Mage::getSingleton('customer/session')->getCustomerGroupId();;
        $groups = explode(',',$groups);
        if(in_array($currentGroup, $groups)){
            return true;
        }
        return false;
    }

    public function getStockInfo()
    {
        $product = $this->_getProduct();
        if($product->getTypeId() != Mage_Catalog_Model_Product_Type::TYPE_GROUPED){
            $qty = $product->getStockItem()->getQty();
            return (int)$qty;
        }else{
            $result = array();
            $children = $product->getTypeInstance(true)
                ->getAssociatedProducts($product);
            foreach($children as $item){
                $result[$item->getId()] = (int)$item->getStockItem()->getQty();
            }
            return $result;
        }
    }
}