<?php
class Jimmy_Custom_Block_Product_View extends Mage_Catalog_Block_Product_View
{
    /**
     * Get product attribute value in frontend
     * @param string $attributeCode
     * @param string $attributeOptionId
     * @return string
     */
    function getOptionValueByAttributeCode($attributeCode,$attributeOptionId){
        return Mage::helper('jcustom')->getOptionValueByAttributeCode($attributeCode,$attributeOptionId);
    }

    public function getAssociatedProducts()
    {
        return $this->getProduct()->getTypeInstance(true)
            ->getAssociatedProducts($this->getProduct());
    }
}