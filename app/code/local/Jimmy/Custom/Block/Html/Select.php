<?php
class Jimmy_Custom_Block_Html_Select extends Mage_Core_Block_Html_Select
{
    /**
     * Render HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        $continueArray = array("billing[country_id]","shipping[country_id]");
        if(!in_array($this->getName(),$continueArray)){
            return parent::_toHtml();
        }
        if (!$this->_beforeToHtml()) {
            return '';
        }

        $html = '<select name="' . $this->getName() . '" id="' . $this->getId() . '" class="'
            . $this->getClass() . '" title="' . $this->getTitle() . '" ' . $this->getExtraParams() . '>';
        $values = $this->getValue();

        if (!is_array($values)) {
            if (!is_null($values)) {
                $values = array($values);
            } else {
                $values = array();
            }
        }

        $priorityVal = array('DE','AT','CH');
        $priorityCountries = array(
            array('value' => 'DE', 'label' => 'Deutschland'),
            array('value' => 'AT', 'label' => 'Österreich'),
            array('value' => 'CH', 'label' => 'Schweiz'),
        );
        $html .= $this->_optionToHtml(
            array(
                'value' => "",
                'label' => ""

            ));
        foreach($priorityCountries as $k => $v){
            $html .= $this->_optionToHtml(
                array(
                    'value' => $v['value'],
                    'label' => $v['label']
                ),
                in_array($v['value'], $values)
            );
        }
        $html .= $this->_optionToHtml(
            array(
                'value' => "",
                'label' => "---------------",
                'params'=>array("disabled"=>"disabled")

            ));
        $isArrayOption = true;
        foreach ($this->getOptions() as $key => $option) {
            if(in_array($option['value'],$priorityVal) || $option['value'] == '') continue;
            if ($isArrayOption && is_array($option)) {
                $value = $option['value'];
                $label = (string)$option['label'];
                $params = (!empty($option['params'])) ? $option['params'] : array();
            } else {
                $value = (string)$key;
                $label = (string)$option;
                $isArrayOption = false;
                $params = array();
            }

            if (is_array($value)) {
                $html .= '<optgroup label="' . $label . '">';
                foreach ($value as $keyGroup => $optionGroup) {
                    if (!is_array($optionGroup)) {
                        $optionGroup = array(
                            'value' => $keyGroup,
                            'label' => $optionGroup
                        );
                    }
                    $html .= $this->_optionToHtml(
                        $optionGroup,
                        in_array($optionGroup['value'], $values)
                    );
                }
                $html .= '</optgroup>';
            } else {
                $html .= $this->_optionToHtml(
                    array(
                        'value' => $value,
                        'label' => $label,
                        'params' => $params
                    ),
                    in_array($value, $values)
                );
            }
        }
        $html .= '</select>';
        return $html;
    }
}