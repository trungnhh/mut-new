<?php
class Jimmy_Custom_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Convert collection items to select options array
     *
     * @return array
     */
    public function toOptionArrayInCustomSortOrder()
    {
        $priority = array('DE','AT','CH');
        $option = Mage::getResourceModel('directory/country_collection')
            ->loadData()
            ->toOptionArray(false);
        foreach($option as $k => $v){
            if(in_array($v['value'],$priority)){
                unset($option[$k]);
            }
        }

        return $option;
    }

    /**
     * Get Customer Name if logged in
     * @return string
     */
    public function getAccountTitle()
    {
        $title = $this->__('My Account');
        if(Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            $title = $customer->getFirstname() . ' ' .$customer->getLastname();
        }
        return $title;
    }

    /**
     * @return int|mixed
     */
    public function getNumberOfVottings()
    {
        $count = 8;
        if(Mage::getStoreConfig('user_vottings/general/display_count')){
            $count = Mage::getStoreConfig('user_vottings/general/display_count');
        }
        return $count;
    }

    /**
     * @return mixed
     */
    public function getReviewIds()
    {
        return Mage::getStoreConfig('user_vottings/general/review_ids');
    }

    /**
     * Get product attribute value in frontend
     * @param string $attributeCode
     * @param string $attributeOptionId
     * @return string
     */
    function getOptionValueByAttributeCode($attributeCode,$attributeOptionId){
        $attributeDetails = Mage::getSingleton("eav/config")
            ->getAttribute("catalog_product", $attributeCode);
        $optionValue = $attributeDetails->getSource()->getOptionText($attributeOptionId);
        return $optionValue;
    }

    function checkIsContainPhysic()
    {
        $session = Mage::getSingleton('checkout/session');
        foreach ($session->getQuote()->getAllItems() as $item) {
            $product = Mage::getModel('catalog/product')->load($item->getProductId());
            if ($product->getTypeId() != 'downloadable')
                return true;
        }
        return false;
    }

    function checkIsContainDownload()
    {
        $session = Mage::getSingleton('checkout/session');
        foreach ($session->getQuote()->getAllItems() as $item) {
            $product = Mage::getModel('catalog/product')->load($item->getProductId());
            if($product->getTypeId() == 'downloadable')
                return true;
        }
        return false;
    }
}