<?php

class Jimmy_Custom_TransferController extends  Mage_Core_Controller_Front_Action
{
    function topdfAction ()
    {
        require_once(Mage::getBaseDir('lib') . DS . 'Transfer2PDF' . DS.'tcpdf' . DS . 'tcpdf.php');

        $currentPageId =  $this->getRequest()->getParam('id');
        $currentCmsPage = Mage::getModel('cms/page')
            ->load($currentPageId);
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        $pdf->AddPage();
        $htmlcontent = Mage::helper('cms')
            ->getPageTemplateProcessor()
            ->filter($currentCmsPage->getContent());
        //var_dump($htmlcontent);die;

        $pdf->writeHTML($htmlcontent, true, 0, true, 0);
        //$pdf->lastPage();

        $pdf->Output('"'.$currentCmsPage->getTitle().'.pdf"', 'D');

    }
}