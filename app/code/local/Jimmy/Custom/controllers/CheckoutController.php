<?php
class Jimmy_Custom_CheckoutController extends Mage_Core_Controller_Front_Action
{
    /**
     * Get current active quote instance
     *
     * @return Mage_Sales_Model_Quote
     */
    protected function _getQuote()
    {
        return $this->_getCart()->getQuote();
    }

    public function estimateShippingAction()
    {
        if ($this->getRequest()->isPost() && $this->getRequest()->isAjax()) {
            $selectedCountry = $this->getRequest()->getPost('country');

            $quote = Mage::getSingleton('checkout/session')->getQuote();
            $amount = $quote->getShippingAddress()->getShippingAmount();
            if ($selectedCountry) {

                $address = $quote->getShippingAddress();
                $address->setCountryId($selectedCountry);
                $address->setCollectShippingRates(true);
                $quote->collectTotals()->save();

               // $address->setCollectShippingRates(true);
               // $quote->collectTotals()->save();

                if ($this->checkIsContainPhysic()) {
                    $rates = $quote->getShippingAddress()->getAllShippingRates();
                    foreach ($rates as $rate) {
                        if ($rate->getCode() == 'tablerate_bestway') {
                            $amount = $rate->getPrice();
                            $address->setShippingMethod($rate->getCode());
                            $quote->setTotalsCollectedFlag(false);
                            $address->save();
                            $quote->collectTotals()->save();
                            break;
                        }
                    }
                }

                /*
                ERROR update shipping rate last time
                $quote->getShippingAddress()->setCountryId($selectedCountry);
                $quote->collectTotals();
                $quote->getShippingAddress()->setCollectShippingRates(true);
                $quote->getShippingAddress()->collectShippingRates();

                if ($this->checkIsContainPhysic()) {
                    $rates = $quote->getShippingAddress()->getAllShippingRates();
                    foreach ($rates as $rate) {
                        if ($rate->getCode() == 'tablerate_bestway') {
                            $amount = $rate->getPrice();
                            $quote->getShippingAddress()
                                ->setShippingMethod($rate->getCode());
                            break;
                        }
                    }
                }

                $quote->collectTotals();
                $quote->save();
                */

            }
            $layout = Mage::app()->getLayout();
            $layout->getUpdate()
                ->load(array('default','opc_index_index'));
            $layout->generateXml()
                ->generateBlocks();
            $cartTotalsBlock = $layout->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml();
            $result['cart_total_data'] = $cartTotalsBlock;

            $shippingAmount = Mage::helper('core')->formatPrice($amount, true);
            $result['shipping'] = $shippingAmount;
            $this->getResponse()->setHeader('Content-type','application/json', true);
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        } else {
            $this->_redirect('onepage');
        }
    }

    function checkIsContainPhysic()
    {
        $session = Mage::getSingleton('checkout/session');
        foreach ($session->getQuote()->getAllItems() as $item) {
            $product = Mage::getModel('catalog/product')->load($item->getProductId());
            if($product->getTypeId() != 'downloadable')
                return true;
        }
        return false;
    }

    /**
     * Add selected products from checkout thankyou page to cart
     */
    public function addInterestProductsAction()
    {
        $productIds = $this->getRequest()->getPost('product_ids');
        $productIdsArr = explode(',',$productIds);
        $cart = $this->_getCart();
        foreach($productIdsArr as $id){
            if($id!=''){
                try {
                    $qty = 1;
                    $product = Mage::getModel('catalog/product')
                        ->setStoreId(Mage::app()->getStore()->getId())
                        ->load($id);
                    $cart->addProduct($product, $qty);
                    $message = $this->__('%s was successfully added to your shopping cart.', $product->getName());
                    Mage::getSingleton('checkout/session')->addSuccess($message);
                }catch (Mage_Core_Exception $e) {
                    if (Mage::getSingleton('checkout/session')->getUseNotice(true)) {
                        Mage::getSingleton('checkout/session')->addNotice($product->getName() . ': ' . $e->getMessage());
                    }
                    else {
                        Mage::getSingleton('checkout/session')->addError($product->getName() . ': ' . $e->getMessage());
                    }
                }
                catch (Exception $e) {
                    Mage::getSingleton('checkout/session')->addException($e, $this->__('Can not add item to shopping cart'));
                }
            }
        }
        $cart->save();
        $this->_redirect('onepage');
    }

    public function setShippingMethodAction()
    {
        $method = $this->getRequest()->getPost('method');
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $shippingAddress = $quote->getShippingAddress();
        $shippingAddress->setShippingMethod($method)->setCollectShippingRates(true);
        $shippingAddress->save();
        $quote->save();
        return 'ok';
    }

    /**
     * Retrieve shopping cart model object
     *
     * @return Mage_Checkout_Model_Cart
     */
    protected function _getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }
}