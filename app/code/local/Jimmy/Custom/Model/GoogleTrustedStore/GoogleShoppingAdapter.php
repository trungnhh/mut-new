<?php
class Jimmy_Custom_Model_GoogleTrustedStore_GoogleShoppingAdapter extends Mage_GoogleTrustedStore_Model_GoogleShoppingAdapter
{
    public function getItemId($product, $storeId = null)
    {
        $this->_checkIsActive();

        $f = Mage::getModel('nscexport/profile')->load(1);
        $url = $f->getUrl();
        $xml = new SimpleXMLElement(file_get_contents($url));
        $def = null;
        foreach ($xml->channel->item as $oItem)
        {
            $gId = (string) $oItem->children('g', TRUE)->id;
            if(!$product){return $gId;}
            if($product->getSku() == $gId){
                return $gId;
            }
        }
        return $def;
    }
}