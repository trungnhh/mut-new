<?php

class Jimmy_Custom_Model_Zend extends Mage_Captcha_Model_Zend
{
    /**
     * Override __construct to reduce the noise in captcha
     */
    public function __construct($params)
    {
        if (!isset($params['formId'])) {
            throw new Exception('formId is mandatory');
        }
        $this->_formId = $params['formId'];
        $this->setExpiration($this->getTimeout());
        $this->setDotNoiseLevel(3);
        $this->setLineNoiseLevel(0);
    }
}