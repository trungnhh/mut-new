<?php
class Jimmy_Custom_Model_Observer
{
    /**
     * Trigger to allow guest checkout
     * @param $observer
     * @return $this
     */
    public function isAllowedGuestCheckout($observer)
    {
        $button = Mage::app()->getRequest()->getParam(Mage_Paypal_Model_Express_Checkout::PAYMENT_INFO_BUTTON);
        if($button){
            $result = $observer->getEvent()->getResult();
            $result->setIsAllowed(true);
            if(!Mage::getSingleton('customer/session')->isLoggedIn()){
                Mage::getSingleton('checkout/type_onepage')->saveCheckoutMethod(Mage_Checkout_Model_Type_Onepage::METHOD_GUEST);
            }
            return $this;
        }
        return $this;
    }

    public function catalogProductSaveAfter($observer)
    {
        $request = Mage::app()->getRequest();
        $product = $observer->getEvent()->getProduct();
        $downloadablePath = Mage::getBaseDir('media') . DS . 'downloadable' . DS . 'files' . DS . 'links';

        if ($downloadable = $request->getPost('downloadable')) {
            $link = $downloadable['link'];
            foreach($link as $item){
                if($item['type'] == 'url'){
                    $collection = Mage::getModel('downloadable/link')
                        ->getCollection()
                        ->addFieldToFilter('product_id',$product->getId())
                        ->addFieldToFilter('link_url',$item['link_url']);
                    foreach($collection as $i){
                        $link_item = Mage::getModel('downloadable/link')->load($i->getId());
                        $link_item->setLinkUrl(false);
                        $link_item->setLinkFile($item['link_url']);
                        $link_item->setLinkType('file');
                        $link_item->save();
                        $writeConnection = Mage::getSingleton('core/resource')->getConnection('core_write');
                        $sql = "update `downloadable_link_title` set `title` = '".$item['title']."' where `link_id` = ".$i->getId()."";
                        $writeConnection->query($sql);
                        $fileName = $downloadablePath . $item['link_url'];
                        $fileSize = filesize($fileName);
                        $ext = pathinfo($fileName, PATHINFO_EXTENSION);
                        $attribute_details = Mage::getSingleton("eav/config")->getAttribute("catalog_product", 'format');
                        $options = $attribute_details->getSource()->getAllOptions(false);
                        foreach ($options as $option) {
                            if($ext == strtolower($option["label"])){
                                Mage::getSingleton('catalog/product_action')->updateAttributes(array($product->getId()),array('format'=>$option["value"]),0);
                            }
                        }
                        Mage::getSingleton('catalog/product_action')->updateAttributes(array($product->getId()),array('filesize'=>$fileSize),0);
                    }
                }
            }
        }
    }
    
    public function salesOrderPlaceAfter($observer)
    {
        $order = $observer->getEvent()->getOrder();

        $comment = Mage::app()->getRequest()->getPost('review_customer_comment');
        $customer_email = $order->getCustomerEmail();
        $emailPart = explode('@',$customer_email);
        $customer_firstname = $customer_lastname = $emailPart[0];
        if($order->getCustomerFirstname()){
            $customer_firstname = $order->getCustomerFirstname();
            $customer_lastname = $order->getCustomerLastname();
        }else if($order->getBillingAddress()->getFirstname()){
            $customer_firstname = $order->getBillingAddress()->getFirstname();
            $customer_lastname = $order->getBillingAddress()->getLastname();
        }
        if($comment && $comment !='' ){
            $order->setCustomerNote($comment);
        }
		
        if(Mage::app()->getRequest()->getPost('auto_create_account')){
            $customer = Mage::getModel('customer/customer');
            $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
            $customer->loadByEmail($customer_email);
            if(!$customer->getId()) {
                //setting data such as email, firstname, lastname, and password
                $customer->setEmail($customer_email);
                $customer->setFirstname($customer_firstname);
                $customer->setLastname($customer_lastname);
                $customer->setPassword($customer->generatePassword(6));

                //Set billing address as customer address
                $billingAddress = $order->getBillingAddress();

                $address = Mage::getModel("customer/address");
                $address->setFirstname($customer->getFirstname())
                    ->setLastname($customer->getLastname())
                    ->setCountryId($billingAddress->getCountryId())
                    ->setPostcode($billingAddress->getPostcode())
                    ->setCity($billingAddress->getCity())
                    ->setTelephone($billingAddress->getTelephone())
                    ->setFax($billingAddress->getFax())
                    ->setCompany($billingAddress->getCompany())
                    ->setStreet($billingAddress->getStreet())
                    ->setIsDefaultBilling('1')
                    ->setIsDefaultShipping('1')
                    ->setSaveInAddressBook('1');
                if(strtolower($billingAddress->getCountryId()) == 'us'){
                    $address->setRegionId($billingAddress->getRegionId()); //state/province, only needed if the country is USA
                }

                //Add address to customer
                $customer->addAddress($address);
            }
            try{
                //the save the data and send the new account email.
                $customer->save();
                $customer->setConfirmation(null);
                $customer->save();
                $customer->sendNewAccountEmail();

                Mage::getSingleton('core/session')->setPaypalExpressFlag($customer->getId());

                Mage::getSingleton('customer/session')->loginById($customer->getId());
                Mage::getSingleton('checkout/session')->addSuccess(Mage::helper('checkout')->__("Wir haben für Sie ein Konto angelegt für Ihre E-Mail: %s. Die Zugangsdaten erhalten Sie per E-Mail.",$customer_email));
            }
            catch(Exception $ex){

            }

        }
        $order->save();
        return $this;
    }

    /**
     * Assign order to customer after creating account
     * @param $observer
     */
    public function checkoutOnepageControllerSuccessAction($observer)
    {
        try {
            /* @var $order Mage_Sales_Model_Order_Invoice */
            $orderIds = $observer->getEvent()->getOrderIds();
            foreach ($orderIds as $orderId) {
                $order = Mage::getModel('sales/order')->load($orderId);
                if(Mage::getSingleton('core/session')->getPaypalExpressFlag()){
                    $order->setCustomerId(Mage::getSingleton('core/session')->getPaypalExpressFlag());
                    $order->setCustomerIsGuest(0);
                    $order->setCustomerGroupId(1);
                    $order->save();
                    Mage::getSingleton('core/session')->setPaypalExpressFlag(null);
                }
            }
        } catch (Mage_Core_Exception $e) {
            Mage::log($e->getMessage());
        } catch (Exception $e) {
            Mage::log($e->getMessage());
        }
    }
}