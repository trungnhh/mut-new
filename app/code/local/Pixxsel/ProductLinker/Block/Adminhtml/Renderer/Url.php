<?php
class Pixxsel_ProductLinker_Block_Adminhtml_Renderer_Url extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

	public function render(Varien_Object $row)
	{
		$value =  $row->getData($this->getColumn()->getIndex());
		return '<span style="color:red;">'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'productlinker/product/add/item/'.$value.'/</span>';
	}
 
}
?>