<?php

/**
 * Class Mut_Sales_Model_Entity_Items
 */
class Mut_Sales_Model_Entity_Items extends EaDesign_PdfGenerator_Model_Entity_Items
{
    /**
     * @param $item
     * @return array
     */
    public function getSandardItemVars($item)
    {
        $order = $this->getOrder();
        $nameStyle = $item->getName();
        $orderItem = $item->getOrderItem();
        if ($orderItem->getProductType() == 'downloadable') {
            $purchasedItem = Mage::getModel('downloadable/link_purchased')
                ->load($item->getInvoice()->getOrderId(), 'order_id');
            $linkPurchasedItem = Mage::getModel('downloadable/link_purchased_item')->getCollection()
                ->addFieldToFilter('order_item_id', $orderItem->getId());
            $purchasedItem->setPurchasedItems($linkPurchasedItem);
            $purchasedItem->getLinkSectionTitle();
            if ($purchasedItem && $purchasedItem->getLinkSectionTitle()) {
                if (strlen($purchasedItem->getLinkSectionTitle()) > strlen($nameStyle)) {
                    $nameStyle = $purchasedItem->getLinkSectionTitle();
                }
            }
        }
        $taxAmount = 0;
        $discountAmount = 0;
        $taxPercent = 0;
        if ($item->getOrderItem()->getParentItem())
        {
            $nameStyle = $this->getValueHtml($item);
            $bunleOptiones = $this->getSelectionAttributes($item);
        }
        if ($item->getTaxAmount() != 0)
        {
            $taxAmount = $order->formatPriceTxt($item->getTaxAmount());
            $taxPercent = round(($item->getTaxAmount()/($item->getRowTotal() - $item->getDiscountAmount() + $item->getHiddenTaxAmount())) * 100) . '%';
        }
        else
        {
            //$taxAmount = $order->formatPriceTxt(0);
        }
        if ($item->getDiscountAmount() != 0)
        {
            $discountAmount = $order->formatPriceTxt($item->getDiscountAmount());
        }
        else
        {
            //$discountAmount = $order->formatPriceTxt(0);
        }

        if ($autocode = $item->getAutocode()) {
            if ($autocode == 'outofcode') {
                $autocode = '';
            } else {
                $autocode = Mage::helper('core')->__('<em>Key:</em> %s', $autocode);
            }
        }

        $itemSku = $item->getSku();
        $standardVars = array(
            'items_name' => array(
                'value' => $nameStyle,
                'label' => 'Product Name'
            ),
            'bundle_items_option' => array(
                'value' => $bunleOptiones['option_label'],
                'label' => 'Bundle Name'
            ),
            'items_sku' => array(
                'value' => $itemSku,
                'label' => 'SKU'
            ),
            'items_qty' => array(
                'value' => $item->getQty() * 1,
                'label' => 'Qty'
            ),
            'items_tax' => array(
                'value' => $taxAmount,
                'label' => 'Tax Amount'
            ),
            'items_tax_percent' => array(
                'value' => $taxPercent,
                'label' => 'Tax Amount'
            ),
            'items_discount' => array(
                'value' => $discountAmount,
                'label' => 'Discount Amount'
            ),
            'items_autocode' => array(
                'value' => $autocode,
                'label' => 'Codes'
            )
        );

        return $standardVars;
    }
}
