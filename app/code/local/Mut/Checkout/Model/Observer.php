<?php

class Mut_Checkout_Model_Observer
{
    public function customAddressFormat($observer)
    {
        $address = $observer->getEvent()->getAddress();
        if($address->getData('is_packstation')!=null && $address->getData('is_packstation')!=0){
            $address->setStreet(array(0 => "Postnummer: ". $address->getData('postnumber'). ' Packstation: '.$address->getData('packstation')));
        }

        if ($address->getData('country_id') == 'US' || $address->getData('country_id') == 'GB') {
            $renderer = $observer->getEvent()->getType()->getRenderer()->getType();
            $defaultFormat = $renderer->getDefaultFormat();
            $defaultFormat = str_replace("{{if postcode}}{{var postcode}}{{/if}} {{if city}}{{var city}} {{/if}}{{if region}}{{var region}}{{/if}}", "{{if city}}{{var city}},{{/if}} {{if postcode}}{{var postcode}}{{/if}}", $defaultFormat);
            $renderer->setDefaultFormat($defaultFormat);
        }
        return $address;
    }

    /**
     * auto invoice when create shipment
     * @param $observer
     */
    public function autoInvoice($observer) {
        $shipment = $observer->getEvent()->getShipment();
        $order = $shipment->getOrder();
        if($order->canInvoice())
        {
            $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
            $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
            $invoice->register();
            $transactionSave = Mage::getModel('core/resource_transaction')
                ->addObject($invoice)
                ->addObject($invoice->getOrder());
            $transactionSave->save();
        }
    }
}