<?php

class Mut_Reports_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getCountryList()
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $list = $readConnection->fetchAll("SELECT DISTINCT country_id FROM `sales_flat_order_address` WHERE country_id IS NOT NULL");
        $countries = array();
        $countries[] = array(
            'label' => 'Alle',
            'value' => 'all',
        );
        foreach ($list as $item) {
            $countryCode = $item['country_id'];
            $countryName = Mage::app()->getLocale()->getCountryTranslation($countryCode);
            $countries[] = array(
                'label' => $countryName,
                'value' => $countryCode,
            );
        }
        return $countries;
    }

    public function orderTypeToArray()
    {
        return array(
            array('label' => 'alle', 'value' => 'all'),
            array('label' => 'nur downloads', 'value' => 'download'),
            array('label' => 'nur box', 'value' => 'box'),
        );
    }

    public function getProductTypes()
    {
        $productTypes = array(
            array(
                'label' => $this->__('Any'),
                'value' => 'all',
            ),
            array(
                'label' => $this->__('Physical products'),
                'value' => 'physical_products',
            ),
            array(
                'label' => $this->__('Physical books'),
                'value' => 'physical_books',
            ),
            array(
                'label' => $this->__('Download products'),
                'value' => 'download_products',
            )
        );

        return $productTypes;
    }

    public function getSortTypes()
    {
        $sortTypes = array(
            array(
                'label' => $this->__('Product Name (A-Z)'),
                'value' => 'name',
            ),
            array(
                'label' => $this->__('Bestellsumme (9-0)'),
                'value' => 'price',
            ),
            array(
                'label' => $this->__('Stück (9-0)'),
                'value' => 'qty_ordered',
            )
        );

        return $sortTypes;
    }

    public function getOrderFromList()
    {
        $fromList = array(
            array(
                'label' => $this->__('any order'),
                'value' => 'all',
            ),
            array(
                'label' => $this->__('shop only'),
                'value' => 'shop',
            ),
            array(
                'label' => $this->__('eBay only'),
                'value' => 'ebay',
            )
        );
        return $fromList;
    }

    public function orderStatusToOptionArray()
    {
        $statuses = Mage::getModel('sales/order_status')->getCollection();
        if($statuses->count() > 0){
            $result = array();
            $result[] = array(
                'label' => $this->__('Alle'),
                'value' => 'all',
            );

            foreach($statuses as $item){
                $result[] = array(
                    'label' => $item->getLabel(),
                    'value' => $item->getStatus(),
                );
            }
            return $result;
        }
        return array();
    }
}