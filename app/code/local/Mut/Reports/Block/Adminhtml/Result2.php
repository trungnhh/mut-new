<?php

class Mut_Reports_Block_Adminhtml_Result2 extends Mage_Core_Block_Template
{
    public function getFilterResult()
    {

        $filterData = $this->getFilterData()->getData();
        if (empty($filterData)) return array();

        $from = $filterData['from'] . " 00:00:00";
        $to = $filterData['to'] . " 23:59:59";
        $fromGMT = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s", $from);
        $toGMT = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s", $to);
        $countries = $filterData['countries'][0];
        $orderFrom = $filterData['order_fom'];

        $countryWithinSlash = "'" . str_replace(",", "','", $countries) . "'";
        $status = $filterData['order_status'][0];
        $statusWithinSlash = "'" . str_replace(",", "','", $status) . "'";
        $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = "
SELECT `order`.`created_at`, `order`.`order_type`, `address`.`lastname`, `address`.`email`, `order`.`increment_id`, `payment`.`method`, `order`.`grand_total`, `order`.`tax_amount`, `address`.`country_id`, `order`.`subtotal`, `order`.`subtotal_incl_tax`, `order`.`discount_amount`
FROM  sales_flat_order `order`
LEFT JOIN sales_flat_order_address `address` ON `order`.`billing_address_id` = `address`.`entity_id`
LEFT JOIN sales_flat_order_payment `payment` ON `order`.`entity_id` = `payment`.`parent_id`
WHERE `order`.`created_at` >= '{$fromGMT}' AND `order`.`created_at` <= '{$toGMT}'
";

        $type = $filterData['type'];
        if($type != 'all'){
            $sql .= " AND `order`.`order_type` like '{$type}%'";
        }

        if($orderFrom == 'shop'){
            $sql .= " AND `payment`.`method` != 'm2epropayment'";
        }elseif($orderFrom == 'ebay'){
            $sql .= " AND `payment`.`method` = 'm2epropayment'";
        }

        if (strpos($countries, "all") === false) {
            $sql .= " AND `address`.`country_id` IN ({$countryWithinSlash})";
        }

        if (strpos($status, "all") === false) {
            $sql .= " AND `order`.`status` IN ({$statusWithinSlash})";
        }

        $sql .= " ORDER BY created_at ASC";
        $orders = $readConnection->fetchAll($sql);

        $result = array();
        $result['from'] = date('d.m.Y', strtotime($from));
        $result['to'] = date('d.m.Y', strtotime($to));

        $countryArray = array();
        if (strpos($countries, "all") === false) {
            $result['land'] = $this->_countriesToLand($countries);
            $countryArray = explode(",", $countries);
        } else {
            $result['land'] = "Alle";
            $countryList = Mage::helper('adminorderreport')->getCountryList();
            array_shift($countryList);
            foreach ($countryList as $country) {
                $countryArray[] = $country['value'];
            }
        }

        $currentStoreId = Mage::app()->getStore()->getId();
        foreach ($orders as $order) {
            if($order['method'] == 'm2epropayment'){
                $order['payment_method'] = Mage::helper('adminorderreport')->__('eBay');
            }else{
                $order['payment_method'] = strtok(trim(Mage::helper('payment')->getMethodInstance($order['method'])->getTitle()), " ");
            }

            $order['tax_percent'] = round(($order['tax_amount'] * 100) / ($order['subtotal_incl_tax'] - $order['tax_amount'] - abs($order['discount_amount']))) . "%";
            foreach ($countryArray as $countryCode) {
                if(!isset($paymentValue[$countryCode])){
                    $paymentValue[$countryCode] = array();
                }
                $type = explode('-', $order['order_type']);
                $orderType = $type[1];
                $orderTypeString = $type[0];
                if($orderTypeString == 'box+download'){
                    $orderTypeString = 'mixed';
                }

                if ($order['method'] == 'm2epropayment') {
                    $orderType = 'ebay';
                }

                $countryName = (string) Mage::app()->getLocale()->getCountryTranslation($countryCode);
                if ($order['country_id'] == $countryCode) {
                    $orderMonth = Mage::app()->getLocale()->storeDate(
                        $currentStoreId,
                        Varien_Date::toTimestamp($order['created_at']),
                        true
                    )->toString("MM.YYYY");
                    $order['order_type_string'] = $orderTypeString;
                    $result['orders'][$countryName]['orders'][$orderMonth]['orders'][] = $order;
                    $result['orders'][$countryName]['orders'][$orderMonth]['total'] += $order['grand_total'];
                    $result['orders'][$countryName]['orders'][$orderMonth]['total_tax'] += $order['tax_amount'];
                    $result['orders'][$countryName]['orders'][$orderMonth]['order_type'][$orderType]['total'] += $order['grand_total'];
                    $result['orders'][$countryName]['orders'][$orderMonth]['order_type'][$orderType]['total_tax'] += $order['tax_amount'];
                    $result['orders'][$countryName]['total'] += $order['grand_total'];
                    $result['orders'][$countryName]['total_tax'] += $order['tax_amount'];
                    $result['orders'][$countryName]['order_type'][$orderType]['total'] += $order['grand_total'];
                    $result['orders'][$countryName]['order_type'][$orderType]['total_tax'] += $order['tax_amount'];
                    $result['total'] += $order['grand_total'];
                    $result['total_tax'] += $order['tax_amount'];
                    $result['order_type'][$orderType]['total'] += $order['grand_total'];
                    $result['order_type'][$orderType]['total_tax'] += $order['tax_amount'];
                }
            }
        }

        return $result;
    }

    protected function _countriesToLand($countries)
    {
        $codeList = explode(",", $countries);
        $land = array();
        foreach ($codeList as $countryCode) {
            $land[] = Mage::app()->getLocale()->getCountryTranslation($countryCode);
        }
        return implode(",", $land);
    }

    public function getCsv()
    {
        $filterData = $this->getFilterData()->getData();
        if (empty($filterData)) return array();

        $from = $filterData['from'] . " 00:00:00";
        $to = $filterData['to'] . " 23:59:59";
        $fromGMT = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s", $from);
        $toGMT = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s", $to);
        $countries = $filterData['countries'][0];
        $orderFrom = $filterData['order_fom'];

        $countryWithinSlash = "'" . str_replace(",", "','", $countries) . "'";
        $status = $filterData['order_status'][0];
        $statusWithinSlash = "'" . str_replace(",", "','", $status) . "'";
        $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = "
SELECT `order`.`created_at`, `order`.`order_type`, `address`.`lastname`, `address`.`email`, `order`.`increment_id`, `payment`.`method`, `order`.`grand_total`, `order`.`tax_amount`, `address`.`country_id`, `order`.`subtotal`, `order`.`subtotal_incl_tax`, `order`.`discount_amount`
FROM  sales_flat_order `order`
LEFT JOIN sales_flat_order_address `address` ON `order`.`billing_address_id` = `address`.`entity_id`
LEFT JOIN sales_flat_order_payment `payment` ON `order`.`entity_id` = `payment`.`parent_id`
WHERE `order`.`created_at` >= '{$fromGMT}' AND `order`.`created_at` <= '{$toGMT}'
";

        $type = $filterData['type'];
        if($type != 'all'){
            $sql .= " AND `order`.`order_type` like '{$type}%'";
        }

        if($orderFrom == 'shop'){
            $sql .= " AND `payment`.`method` != 'm2epropayment'";
        }elseif($orderFrom == 'ebay'){
            $sql .= " AND `payment`.`method` = 'm2epropayment'";
        }

        if (strpos($countries, "all") === false) {
            $sql .= " AND `address`.`country_id` IN ({$countryWithinSlash})";
        }

        if (strpos($status, "all") === false) {
            $sql .= " AND `order`.`status` IN ({$statusWithinSlash})";
        }

        $sql .= " ORDER BY created_at ASC";
        $orders = $readConnection->fetchAll($sql);

        $csvHeader = array(
            'Date',
            'Lastname',
            'Email',
            'Order Id',
            'Art',
            'Payment',
            'Netto Betrag',
            'Mwst',
            'Betrag'
        );
        $csv = $this->_getCsvRow($csvHeader);
        $_coreHelper = $this->helper('core');
        foreach ($orders as $order) {
            $tmp = array();
            $tmp['date'] = Mage::app()->getLocale()->storeDate(
                Mage::app()->getStore()->getId(),
                Varien_Date::toTimestamp($order['created_at']),
                true
            )->toString("dd.MM.YYYY");
            $tmp['lastname'] = $order['lastname'];
            $tmp['email'] = $order['email'];
            $tmp['order_id'] = $order['increment_id'];

            $type = explode('-', $order['order_type']);
            $orderTypeString = $type[0];
            if ($orderTypeString == 'box+download') {
                $orderTypeString = 'mixed';
            }
            $tmp['art'] = $orderTypeString;

            if($order['method'] == 'm2epropayment'){
                $tmp['payment_method'] = Mage::helper('adminorderreport')->__('eBay');
            }else{
                $tmp['payment_method'] = strtok(trim(Mage::helper('payment')->getMethodInstance($order['method'])->getTitle()), " ");
            }

            $tmp['order_total'] = number_format($_coreHelper->currency($order['grand_total'] - $order['tax_amount'], false, false), 2, ',', '.');
            $tmp['tax_percent'] = round(($order['tax_amount'] * 100) / ($order['subtotal_incl_tax'] - $order['tax_amount'] - abs($order['discount_amount']))) . "%";
            $tmp['tax_amount'] = number_format($_coreHelper->currency($order['tax_amount'], false, false), 2, ',', '.');
            $csv .= $this->_getCsvRow($tmp);
        }
        return $csv;
    }

    protected function _getCsvRow($dataArray)
    {
        $csvRowData = array();
        foreach ($dataArray as $item) {
            $csvRowData[] = '"' . str_replace(array('"', '\\'), array('""', '\\\\'), $item) . '"';
        }
        return implode(",", $csvRowData) . "\n";
    }

    public function exportCsv($filename)
    {
        $filterData = $this->getFilterData()->getData();
        if (empty($filterData)) return array();

        $from = $filterData['from'] . " 00:00:00";
        $to = $filterData['to'] . " 23:59:59";
        $fromGMT = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s", $from);
        $toGMT = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s", $to);
        $countries = $filterData['countries'][0];
        $orderFrom = $filterData['order_fom'];

        $countryWithinSlash = "'" . str_replace(",", "','", $countries) . "'";
        $status = $filterData['order_status'][0];
        $statusWithinSlash = "'" . str_replace(",", "','", $status) . "'";
        $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = "
SELECT `order`.`created_at`, `order`.`order_type`, `address`.`lastname`, `address`.`email`, `order`.`increment_id`, `payment`.`method`, `order`.`grand_total`, `order`.`tax_amount`, `address`.`country_id`, `order`.`subtotal`, `order`.`subtotal_incl_tax`, `order`.`discount_amount`
FROM  sales_flat_order `order`
LEFT JOIN sales_flat_order_address `address` ON `order`.`billing_address_id` = `address`.`entity_id`
LEFT JOIN sales_flat_order_payment `payment` ON `order`.`entity_id` = `payment`.`parent_id`
WHERE `order`.`created_at` >= '{$fromGMT}' AND `order`.`created_at` <= '{$toGMT}'
";

        $type = $filterData['type'];
        if($type != 'all'){
            $sql .= " AND `order`.`order_type` like '{$type}%'";
        }

        if($orderFrom == 'shop'){
            $sql .= " AND `payment`.`method` != 'm2epropayment'";
        }elseif($orderFrom == 'ebay'){
            $sql .= " AND `payment`.`method` = 'm2epropayment'";
        }

        if (strpos($countries, "all") === false) {
            $sql .= " AND `address`.`country_id` IN ({$countryWithinSlash})";
        }

        if (strpos($status, "all") === false) {
            $sql .= " AND `order`.`status` IN ({$statusWithinSlash})";
        }

        $sql .= " ORDER BY created_at ASC";
        $orders = $readConnection->fetchAll($sql);

        $csvHeader = array(
            'Date',
            'Lastname',
            'Email',
            'Order Id',
            'Art',
            'Payment',
            'Netto Betrag',
            'Mwst',
            'Betrag'
        );
        $csvData = array();

        $_coreHelper = $this->helper('core');
        foreach ($orders as $order) {
            $tmp = array();
            $tmp['date'] = Mage::app()->getLocale()->storeDate(
                Mage::app()->getStore()->getId(),
                Varien_Date::toTimestamp($order['created_at']),
                true
            )->toString("dd.MM.YYYY");
            $tmp['lastname'] = $order['lastname'];
            $tmp['email'] = $order['email'];
            $tmp['order_id'] = $order['increment_id'];

            $type = explode('-', $order['order_type']);
            $orderTypeString = $type[0];
            if ($orderTypeString == 'box+download') {
                $orderTypeString = 'mixed';
            }
            $tmp['art'] = $orderTypeString;

            if($order['method'] == 'm2epropayment'){
                $tmp['payment_method'] = Mage::helper('adminorderreport')->__('eBay');
            }else{
                $tmp['payment_method'] = strtok(trim(Mage::helper('payment')->getMethodInstance($order['method'])->getTitle()), " ");
            }

            $tmp['order_total'] = number_format($_coreHelper->currency($order['grand_total'] - $order['tax_amount'], false, false), 2, ',', '.');
            $tmp['tax_percent'] = round(($order['tax_amount'] * 100) / ($order['subtotal_incl_tax'] - $order['tax_amount'] - $order['discount_amount'])) . "%";
            $tmp['tax_amount'] = number_format($_coreHelper->currency($order['tax_amount'], false, false), 2, ',', '.');
            $csvData[] = $tmp;
        }

        // Write data to file
        $objPHPExcel = new PHPExcel_PHPExcel();

        $objPHPExcel->getActiveSheet()->SetCellValue('A1', $csvHeader[0]);
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', $csvHeader[1]);
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', $csvHeader[2]);
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', $csvHeader[3]);
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', $csvHeader[4]);
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', $csvHeader[5]);
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', $csvHeader[6]);
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', $csvHeader[7]);
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', $csvHeader[8]);

        $objPHPExcel->getActiveSheet()->fromArray($csvData, null, 'A2');

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'. $filename .'"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }
}