<?php

class Mut_Reports_Block_Adminhtml_Result extends Mage_Core_Block_Template
{
    public function getFilterResult()
    {
        $filterData = $this->getFilterData()->getData();
        if (empty($filterData)) return array();

        $from = $filterData['from'] . " 00:00:00";
        $to = $filterData['to'] . " 23:59:59";
        $fromGMT = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s", $from);
        $toGMT = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s", $to);
        $orderFrom = $filterData['order_fom'];

        $countries = $filterData['countries'][0];
        $countryWithinSlash = "'" . str_replace(",", "','", $countries) . "'";
        $productType = $filterData['product_type'];
        $sortBy = $filterData['sort_by'];

        $status = $filterData['order_status'][0];
        $statusWithinSlash = "'" . str_replace(",", "','", $status) . "'";

        $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = "
SELECT `item`.`name`, `item`.`sku`, `item`.`qty_ordered`, `item`.`row_total_incl_tax` as `price`, `item`.`row_total` as `netto_price`, `item`.`tax_amount`,
`item`.`tax_percent`, `product`.`attribute_set_id`, `item`.`order_id`, `item`.`discount_amount`, `address`.`country_id`, `order`.`status`, `order`.`shipping_amount`
FROM sales_flat_order_item `item`
LEFT JOIN sales_flat_order `order` ON `item`.`order_id` = `order`.`entity_id`
LEFT JOIN sales_flat_order_address `address` ON `order`.`billing_address_id` = `address`.`entity_id`
LEFT JOIN catalog_product_entity `product` ON `product`.`sku` = `item`.`sku`
LEFT JOIN sales_flat_order_payment `payment` ON `item`.`order_id` = `payment`.`parent_id`
WHERE `item`.`created_at` >= '{$fromGMT}' AND `item`.`created_at` <= '{$toGMT}'
";
        if ($productType != 'all') {
            if ($productType == 'physical_books') {
                $sql .= " AND `product`.`attribute_set_id` = 10"; //Bunch (Physical books)
            } else if ($productType == 'download_products') {
                $sql .= " AND `product`.`attribute_set_id` IN (9, 12)"; //E-book and Download
            } else {
                $sql .= " AND `product`.`attribute_set_id` NOT IN (9, 10, 12)"; //Physical Products
            }
        }

        if($orderFrom == 'shop'){
            $sql .= " AND `payment`.`method` != 'm2epropayment'";
        }elseif($orderFrom == 'ebay'){
            $sql .= " AND `payment`.`method` = 'm2epropayment'";
        }

        if (strpos($countries, "all") === false) {
            $sql .= " AND `address`.`country_id` IN ({$countryWithinSlash})";
        }

        if (strpos($status, "all") === false) {
            $sql .= " AND `order`.`status` IN ({$statusWithinSlash})";
        }

        $items = $readConnection->fetchAll($sql);
        $mergeItems = array();
        $result = array();
        $result['total_qty'] = 0;
        $result['total_price'] = 0;
        $result['total_price_shipping'] = 0;

        $taxGroup = array();
        $listOrderIds = array();
        foreach ($items as $item) {
            if (!isset($mergeItems[$item['sku'] . "_" . $item['tax_percent'] . "_" . $item['country_id']])) {
                $mergeItems[$item['sku'] . "_" . $item['tax_percent'] . "_" . $item['country_id']] = $item;
            } else {
                $mergeItems[$item['sku'] . "_" . $item['tax_percent'] . "_" . $item['country_id']]['qty_ordered'] += $item['qty_ordered'];
                $mergeItems[$item['sku'] . "_" . $item['tax_percent'] . "_" . $item['country_id']]['price'] += ($item['price'] - $item['discount_amount']);
                $mergeItems[$item['sku'] . "_" . $item['tax_percent'] . "_" . $item['country_id']]['netto_price'] += $item['netto_price'];
                $mergeItems[$item['sku'] . "_" . $item['tax_percent'] . "_" . $item['country_id']]['tax_amount'] += $item['tax_amount'];
            }

            if (!isset($taxGroup['tax_' . $item['tax_percent']])) {
                $taxGroup['tax_' . $item['tax_percent']] = array(
                    'percent' => $item['tax_percent'],
                    'qty' => $item['qty_ordered'],
                    'price' => ($item['price'] - $item['discount_amount']),
                    'amount' => $item['tax_amount'],
                );
            } else {
                $taxGroup['tax_' . $item['tax_percent']]['qty'] += $item['qty_ordered'];
                $taxGroup['tax_' . $item['tax_percent']]['price'] += ($item['price'] - $item['discount_amount']);
                $taxGroup['tax_' . $item['tax_percent']]['amount'] += $item['tax_amount'];
            }

            $result['total_qty'] += $item['qty_ordered'];
            $result['total_price'] += ($item['price'] - $item['discount_amount']);
            if (!in_array($item['order_id'], $listOrderIds)) {
                $listOrderIds[] = $item['order_id'];
                $result['total_price_shipping'] += $item['shipping_amount'];
            }
        }

        if ($sortBy == 'name') {
            usort($mergeItems, function($item1, $item2) {
                return strcasecmp($item1['name'], $item2['name']);
            });
        } else if ($sortBy == 'price') {
            usort($mergeItems, function($item1, $item2) {
                if ($item1['price'] == $item2['price']) return 0;
                return ($item1['price'] < $item2['price']) ? 1 : -1;
            });
        } else {
            usort($mergeItems, function($item1, $item2) {
                if ($item1['qty_ordered'] == $item2['qty_ordered']) return 0;
                return ($item1['qty_ordered'] < $item2['qty_ordered']) ? 1 : -1;
            });
        }

        ksort($taxGroup);
        $taxGroup = array_reverse($taxGroup);
        $result['items'] = $mergeItems;
        $result['tax_group'] = $taxGroup;
        $result['from'] = date('d.m.Y', strtotime($from));
        $result['to'] = date('d.m.Y', strtotime($to));
        $result['product_type'] = $productType == 'all' ? "Alle" : ($productType == 'physical_products' ? "Physical products" : ($productType == 'physical_books' ? "Physical books" : "Download products"));
        if (strpos($countries, "all") === false) {
            $result['land'] = $this->_countriesToLand($countries);
        } else {
            $result['land'] = "Alle";
            $itemsByCountry = array();
            $countryList = Mage::helper('adminorderreport')->getCountryList();
            array_shift($countryList);

            foreach ($countryList as $country) {
                $itemsByCountryValue = array();
                $itemsByCountryValue['total_qty'] = 0;
                $itemsByCountryValue['total_price'] = 0;
                $_taxGroup = array();
                $listOrderIds = array();
                foreach ($mergeItems as $_item) {
                    if ($_item['country_id'] == $country['value']) {
                        $itemsByCountryValue['items'][] = $_item;

                        if (!isset($_taxGroup['tax_' . $_item['tax_percent']])) {
                            $_taxGroup['tax_' . $_item['tax_percent']] = array(
                                'percent' => $_item['tax_percent'],
                                'qty' => $_item['qty_ordered'],
                                'price' => ($_item['price'] - $_item['discount_amount']),
                                'amount' => $_item['tax_amount'],
                            );
                        } else {
                            $_taxGroup['tax_' . $_item['tax_percent']]['qty'] += $_item['qty_ordered'];
                            $_taxGroup['tax_' . $_item['tax_percent']]['price'] += ($_item['price'] - $_item['discount_amount']);
                            $_taxGroup['tax_' . $_item['tax_percent']]['amount'] += $_item['tax_amount'];
                        }

                        $itemsByCountryValue['total_qty'] += $_item['qty_ordered'];
                        $itemsByCountryValue['total_price'] += ($_item['price'] - $_item['discount_amount']);
                        if (!in_array($_item['order_id'], $listOrderIds)) {
                            $listOrderIds[] = $_item['order_id'];
                            $itemsByCountryValue['total_price_shipping'] += $_item['shipping_amount'];
                        }
                    }
                }

                ksort($_taxGroup);
                $_taxGroup = array_reverse($_taxGroup);
                $itemsByCountryValue['tax_group'] = $_taxGroup;
                $itemsByCountry[$country['label']] = $itemsByCountryValue;
            }

            $result['items_by_country'] = array_filter($itemsByCountry);
        }

        return $result;
    }

    protected function _countriesToLand($countries)
    {
        $codeList = explode(",", $countries);
        $land = array();
        foreach ($codeList as $countryCode) {
            $land[] = Mage::app()->getLocale()->getCountryTranslation($countryCode);
        }
        return implode(",", $land);
    }

    public function getCsv()
    {
        $filterData = $this->getFilterData()->getData();
        if (empty($filterData)) return array();

        $from = $filterData['from'] . " 00:00:00";
        $to = $filterData['to'] . " 23:59:59";
        $fromGMT = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s", $from);
        $toGMT = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s", $to);
        $countries = $filterData['countries'][0];
        $countryWithinSlash = "'" . str_replace(",", "','", $countries) . "'";
        $productType = $filterData['product_type'];
        $sortBy = $filterData['sort_by'];

        $orderFrom = $filterData['order_fom'];

        $status = $filterData['order_status'][0];
        $statusWithinSlash = "'" . str_replace(",", "','", $status) . "'";

        $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = "
SELECT `item`.`name`, `item`.`sku`, `item`.`qty_ordered`, `item`.`row_total_incl_tax` as `price`, `item`.`row_total` as `netto_price`, `item`.`tax_amount`,
`item`.`tax_percent`, `product`.`attribute_set_id`, `item`.`order_id`, `item`.`order_id`, `item`.`discount_amount` ,`address`.`country_id`, `order`.`status`, `order`.`shipping_amount`
FROM sales_flat_order_item `item`
LEFT JOIN sales_flat_order `order` ON `item`.`order_id` = `order`.`entity_id`
LEFT JOIN sales_flat_order_address `address` ON `order`.`billing_address_id` = `address`.`entity_id`
LEFT JOIN catalog_product_entity `product` ON `product`.`sku` = `item`.`sku`
LEFT JOIN sales_flat_order_payment `payment` ON `item`.`order_id` = `payment`.`parent_id`
WHERE `item`.`created_at` >= '{$fromGMT}' AND `item`.`created_at` <= '{$toGMT}'
";
        if ($productType != 'all') {
            if ($productType == 'physical_books') {
                $sql .= " AND `product`.`attribute_set_id` = 10"; //Bunch (Physical books)
            } else if ($productType == 'download_products') {
                $sql .= " AND `product`.`attribute_set_id` IN (9, 12)"; //E-book and Download
            } else {
                $sql .= " AND `product`.`attribute_set_id` NOT IN (9, 10, 12)"; //Physical Products
            }
        }

        if($orderFrom == 'shop'){
            $sql .= " AND `payment`.`method` != 'm2epropayment'";
        }elseif($orderFrom == 'ebay'){
            $sql .= " AND `payment`.`method` = 'm2epropayment'";
        }

        if (strpos($countries, "all") === false) {
            $sql .= " AND `address`.`country_id` IN ({$countryWithinSlash})";
        }

        if (strpos($status, "all") === false) {
            $sql .= " AND `order`.`status` IN ({$statusWithinSlash})";
        }

        $items = $readConnection->fetchAll($sql);
        $mergeItems = array();

        foreach ($items as $item) {
            if (!isset($mergeItems[$item['sku'] . "_" . $item['tax_percent']])) {
                $mergeItems[$item['sku'] . "_" . $item['tax_percent']] = $item;
            } else {
                $mergeItems[$item['sku'] . "_" . $item['tax_percent']]['qty_ordered'] += $item['qty_ordered'];
                $mergeItems[$item['sku'] . "_" . $item['tax_percent']]['price'] += ($item['price'] - $item['discount_amount']);
                $mergeItems[$item['sku'] . "_" . $item['tax_percent']]['tax_amount'] += $item['tax_amount'];
            }
        }

        if ($sortBy == 'name') {
            usort($mergeItems, function($item1, $item2) {
                return strcasecmp($item1['name'], $item2['name']);
            });
        } else if ($sortBy == 'price') {
            usort($mergeItems, function($item1, $item2) {
                if ($item1['price'] == $item2['price']) return 0;
                return ($item1['price'] < $item2['price']) ? 1 : -1;
            });
        } else {
            usort($mergeItems, function($item1, $item2) {
                if ($item1['qty_ordered'] == $item2['qty_ordered']) return 0;
                return ($item1['qty_ordered'] < $item2['qty_ordered']) ? 1 : -1;
            });
        }

        $csvHeader = array(
            'Produkt',
            'SKU',
            'Stück',
            'Netto Betrag',
            'Mwst',
            'Betrag',
        );
        $csv = $this->_getCsvRow($csvHeader);
        $_coreHelper = $this->helper('core');
        foreach ($mergeItems as $_item) {
            $tmp = array();
            $tmp['name'] = trim($_item['name']) . ($_item['attribute_set_id'] == 9 ? ' (E-Book)' : ($_item['attribute_set_id'] == 12 ? ' (Download)' : ''));
            $tmp['sku'] = $_item['sku'];
            $tmp['qty']  = (int) $_item['qty_ordered'];
            $tmp['price'] = number_format($_coreHelper->currency($_item['netto_price'], false, false), 2, ',', '.');
            $tmp['tax_percent'] = ((int) $_item['tax_percent']) . "%";
            $tmp['tax_total'] = number_format($_coreHelper->currency($_item['tax_amount'], false, false), 2, ',', '.');
            $csv .= $this->_getCsvRow($tmp);
        }
        return $csv;
    }

    protected function _getCsvRow($dataArray)
    {
        $csvRowData = array();
        foreach ($dataArray as $item) {
            $csvRowData[] = '"' . str_replace(array('"', '\\'), array('""', '\\\\'), $item) . '"';
        }
        return implode(",", $csvRowData) . "\n";
    }

    public function exportCsv($filename)
    {
        $filterData = $this->getFilterData()->getData();
        if (empty($filterData)) return array();

        $from = $filterData['from'] . " 00:00:00";
        $to = $filterData['to'] . " 23:59:59";
        $fromGMT = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s", $from);
        $toGMT = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s", $to);
        $countries = $filterData['countries'][0];
        $countryWithinSlash = "'" . str_replace(",", "','", $countries) . "'";
        $productType = $filterData['product_type'];
        $sortBy = $filterData['sort_by'];

        $orderFrom = $filterData['order_fom'];

        $status = $filterData['order_status'][0];
        $statusWithinSlash = "'" . str_replace(",", "','", $status) . "'";

        $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = "
SELECT `item`.`name`, `item`.`sku`, `item`.`qty_ordered`, `item`.`row_total_incl_tax` as `price`, `item`.`row_total` as `netto_price`, `item`.`tax_amount`,
`item`.`tax_percent`, `product`.`attribute_set_id`, `item`.`order_id`, `item`.`discount_amount`, `address`.`country_id`, `order`.`status`, `order`.`shipping_amount`
FROM sales_flat_order_item `item`
LEFT JOIN sales_flat_order `order` ON `item`.`order_id` = `order`.`entity_id`
LEFT JOIN sales_flat_order_address `address` ON `order`.`billing_address_id` = `address`.`entity_id`
LEFT JOIN catalog_product_entity `product` ON `product`.`sku` = `item`.`sku`
LEFT JOIN sales_flat_order_payment `payment` ON `item`.`order_id` = `payment`.`parent_id`
WHERE `item`.`created_at` >= '{$fromGMT}' AND `item`.`created_at` <= '{$toGMT}'
";
        if ($productType != 'all') {
            if ($productType == 'physical_books') {
                $sql .= " AND `product`.`attribute_set_id` = 10"; //Bunch (Physical books)
            } else if ($productType == 'download_products') {
                $sql .= " AND `product`.`attribute_set_id` IN (9, 12)"; //E-book and Download
            } else {
                $sql .= " AND `product`.`attribute_set_id` NOT IN (9, 10, 12)"; //Physical Products
            }
        }

        if($orderFrom == 'shop'){
            $sql .= " AND `payment`.`method` != 'm2epropayment'";
        }elseif($orderFrom == 'ebay'){
            $sql .= " AND `payment`.`method` = 'm2epropayment'";
        }

        if (strpos($countries, "all") === false) {
            $sql .= " AND `address`.`country_id` IN ({$countryWithinSlash})";
        }

        if (strpos($status, "all") === false) {
            $sql .= " AND `order`.`status` IN ({$statusWithinSlash})";
        }

        $items = $readConnection->fetchAll($sql);
        $mergeItems = array();

        foreach ($items as $item) {
            if (!isset($mergeItems[$item['sku'] . "_" . $item['tax_percent']])) {
                $mergeItems[$item['sku'] . "_" . $item['tax_percent']] = $item;
            } else {
                $mergeItems[$item['sku'] . "_" . $item['tax_percent']]['qty_ordered'] += $item['qty_ordered'];
                $mergeItems[$item['sku'] . "_" . $item['tax_percent']]['price'] += ($item['price'] - $item['discount_amount']);
                $mergeItems[$item['sku'] . "_" . $item['tax_percent']]['netto_price'] += $item['netto_price'];
                $mergeItems[$item['sku'] . "_" . $item['tax_percent']]['tax_amount'] += $item['tax_amount'];
            }
        }

        if ($sortBy == 'name') {
            usort($mergeItems, function($item1, $item2) {
                return strcasecmp($item1['name'], $item2['name']);
            });
        } else if ($sortBy == 'price') {
            usort($mergeItems, function($item1, $item2) {
                if ($item1['price'] == $item2['price']) return 0;
                return ($item1['price'] < $item2['price']) ? 1 : -1;
            });
        } else {
            usort($mergeItems, function($item1, $item2) {
                if ($item1['qty_ordered'] == $item2['qty_ordered']) return 0;
                return ($item1['qty_ordered'] < $item2['qty_ordered']) ? 1 : -1;
            });
        }

        $csvHeader = array(
            'Produkt',
            'Status',
            'SKU',
            'Stück',
            'Summe netto',
            'Mwst',
            'Mwst.-Betrag',
            'Summe brutto',
        );
        $csvData = array();

        $_coreHelper = $this->helper('core');
        foreach ($mergeItems as $_item) {
            $tmp = array();
            $tmp['name'] = trim($_item['name']) . ($_item['attribute_set_id'] == 9 ? ' (E-Book)' : ($_item['attribute_set_id'] == 12 ? ' (Download)' : ''));
            $tmp['status'] = $_item['status'];
            $tmp['sku'] = $_item['sku'];
            $tmp['qty']  = (int) $_item['qty_ordered'];
            $tmp['netto_price'] = number_format($_coreHelper->currency($_item['netto_price'], false, false), 2, ',', '.');
            $tmp['tax_percent'] = ((int) $_item['tax_percent']) . "%";
            $tmp['tax_total'] = number_format($_coreHelper->currency($_item['tax_amount'], false, false), 2, ',', '.');
            $tmp['price'] = number_format($_coreHelper->currency($_item['price'] - $_item['discount_amount'], false, false), 2, ',', '.');
            $csvData[] = $tmp;
        }

        // Write data to file
        $objPHPExcel = new PHPExcel_PHPExcel();

        $objPHPExcel->getActiveSheet()->SetCellValue('A1', $csvHeader[0]);
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', $csvHeader[1]);
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', $csvHeader[2]);
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', $csvHeader[3]);
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', $csvHeader[4]);
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', $csvHeader[5]);
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', $csvHeader[6]);
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', $csvHeader[7]);

        $objPHPExcel->getActiveSheet()->fromArray($csvData, null, 'A2');

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'. $filename .'"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }
}