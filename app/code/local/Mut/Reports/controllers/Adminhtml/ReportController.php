<?php
class Mut_Reports_Adminhtml_ReportController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_title($this->__('Orders Reports'));
        $this->loadLayout()
            ->_setActiveMenu('report/sales/sales')
            ->_addBreadcrumb(Mage::helper('reports')->__('Reports'), Mage::helper('adminorderreport')->__('Orders Reports'));

        $filterFormBlock = $this->getLayout()->getBlock('filter.form');
        $filterResultBlock = $this->getLayout()->getBlock('filter.result');

        $this->_initReportAction(array(
            $filterFormBlock,
            $filterResultBlock
        ));

        $this->renderLayout();
    }

    public function orderAction()
    {
        $this->_title($this->__('Orders Reports'));
        $this->loadLayout()
            ->_setActiveMenu('report/sales/sales')
            ->_addBreadcrumb(Mage::helper('reports')->__('Reports'), Mage::helper('adminorderreport')->__('Orders Reports'));

        $filterFormBlock = $this->getLayout()->getBlock('filter.form');
        $filterResultBlock = $this->getLayout()->getBlock('filter.result');

        $this->_initReportAction(array(
            $filterFormBlock,
            $filterResultBlock
        ));

        $this->renderLayout();
    }

    public function _initReportAction($blocks)
    {
        if (!is_array($blocks)) {
            $blocks = array($blocks);
        }

        $requestData = Mage::helper('adminhtml')->prepareFilterString($this->getRequest()->getParam('filter'));
        $requestData = $this->_filterDates($requestData, array('from', 'to'));
        $requestData['store_ids'] = $this->getRequest()->getParam('store_ids');
        $params = new Varien_Object();

        foreach ($requestData as $key => $value) {
            if (!empty($value)) {
                $params->setData($key, $value);
            }
        }

        foreach ($blocks as $block) {
            if ($block) {
                $block->setFilterData($params);
            }
        }

        return $this;
    }

    public function printpdfAction()
    {
        require_once(Mage::getBaseDir('lib') . DS . 'html2pdf' . DS. 'html2pdf.class.php');

        $filterResultBlock = $this->getLayout()->createBlock('adminorderreport/adminhtml_result')->setTemplate('orderreport/filter_result.phtml');
        $this->_initReportAction(array(
            $filterResultBlock
        ));
        $htmlContent = $filterResultBlock->toHtml();

        $html2pdf = new HTML2PDF('L', 'A4', 'fr', true, 'UTF-8', array(5, 5, 5, 5));
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->writeHTML($htmlContent, false);
        $html2pdf->Output('Bestellungen.pdf', 'D');
    }

    public function printpdf2Action()
    {
        require_once(Mage::getBaseDir('lib') . DS . 'html2pdf' . DS. 'html2pdf.class.php');

        $filterResultBlock = $this->getLayout()->createBlock('adminorderreport/adminhtml_result2')->setTemplate('orderreport/filter_result2.phtml');
        $this->_initReportAction(array(
            $filterResultBlock
        ));
        $htmlContent = $filterResultBlock->toHtml();

        $html2pdf = new HTML2PDF('L', 'A4', 'fr', true, 'UTF-8', array(5, 5, 5, 5));
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->writeHTML($htmlContent, false);
        $html2pdf->Output('Bestellungen.pdf', 'D');
    }

    public function exportCsvAction()
    {
        $fileName = 'Bestellungen.xlsx';
        $filterResultBlock = $this->getLayout()->createBlock('adminorderreport/adminhtml_result');
        $this->_initReportAction(array(
            $filterResultBlock
        ));
        $filterResultBlock->exportCsv($fileName);
    }

    public function exportCsv2Action()
    {
        $fileName = 'Bestellungen.xlsx';
        $filterResultBlock = $this->getLayout()->createBlock('adminorderreport/adminhtml_result2');
        $this->_initReportAction(array(
            $filterResultBlock
        ));
        $filterResultBlock->exportCsv($fileName);
    }

    /**
     * Check is allowed access to action
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}
