<?php
class Mut_Catalog_Model_Observer
{
    public function productSaveAfter($observer)
    {
        $product = $observer->getProduct();
        if ($product->getTypeId() !== 'grouped') return;
        $associatedProducts = $product->getTypeInstance(true)->getAssociatedProducts($product);
        $downloadCount = 0;
        $boxCount = 0;
        foreach ($associatedProducts as $children) {
            if ($children->getTypeId() == 'downloadable') {
                $downloadCount++;
            } elseif ($children->getTypeId() == 'simple') {
                $boxCount++;
            }
        }
        if ($downloadCount != 1 || $boxCount != 1) return;

        $boxProduct = $product;
        foreach ($associatedProducts as $children) {
            if ($children->getTypeId() == 'simple') {
                $boxProduct = $children;
            }
        }
        $targetPath = $product->getUrlPath();
        $requestPath = $boxProduct->getUrlPath();

        if (!$targetPath || !$requestPath) return;

        /* @var $urlRewriteCollection Mage_Core_Model_Resource_Url_Rewrite_Collection  */
        $urlRewriteCollection = Mage::getModel('core/url_rewrite')->getCollection()
            //->addFieldToFilter('target_path', $targetPath)
            ->addFieldToFilter('request_path', $requestPath);
        if ($urlRewriteCollection->count()) return;

        $idPath = 'auto' . rand(1000, 9999) . "_" . time();
        Mage::getModel('core/url_rewrite')->setIsSystem(0)->setOptions('RP')->setIdPath($idPath)
            ->setTargetPath($targetPath)->setRequestPath($requestPath)
            ->setDescription('Automatically generated for hiding Box product!')->save();
    }

    public function checkCoupon($observer)
    {
        $quote = $observer->getEvent()->getQuote();
        $customer = $quote->getCustomer();
        if ($customer->getId()) {
            $customerEmail = $customer->getEmail();
        } else {
            $email = $quote->getBillingAddress()->getEmail();
            if (!$email) $email = $quote->getCustomerEmail();
            if (!Zend_Validate::is($email, 'EmailAddress')) {
                $customerEmail = '';
            } else {
                $customerEmail = $email;
            }

        }

        if ($customerEmail != '') {
            // validate email
            $couponCode = $quote->getCouponCode();
            $couponCodeVerify = strtolower(Mage::getStoreConfig('newsletter/subscription/coupon'));
            $couponCodeVerify = explode(',', $couponCodeVerify);
            if (!in_array(strtolower($couponCode), $couponCodeVerify)) return;

            $orderCollection = Mage::getModel('sales/order')->getCollection()
                ->addFieldToFilter('customer_email', $customerEmail)
                ->addFieldToFilter('coupon_code', strtolower($couponCode));

            if ($orderCollection->count()) {
                $quote->setCouponCode('');
            }

            $helperMailjet = new Mailjet_Iframes_Helper_SyncManager();
            $apiOverlay = $helperMailjet->getApiInstance();
            $sync = new Mailjet_Iframes_Helper_SynchronizationCustom($apiOverlay);
            $createdAt = $sync->getContactCreated($customerEmail);

            if ($createdAt != false) {
                $subscribedDate =  $createdAt;
                if (strtotime(Mage::getModel('core/date')->gmtDate()) - strtotime($subscribedDate) > 2 * 7 * 24 * 60 * 60) {
                    $quote->setCouponCode('');
                }
            }

            /*$listId = Mage::getStoreConfig(Ebizmarts_MageMonkey_Model_Config::GENERAL_LIST);
            $member = Mage::getSingleton('monkey/api')
                ->listMemberInfo($listId, $customerEmail);
            if (!is_string($member) && $member['success'] && ($member['data'][0]['status'] == 'subscribed' || $member['data'][0]['status'] == 'pending')) {
                $subscribedDate =  $member['data'][0]['timestamp_signup'];
                if (strtotime(Mage::getModel('core/date')->gmtDate()) - strtotime($subscribedDate) > 2 * 7 * 24 * 60 * 60) {
                    $quote->setCouponCode('');
                }
            } else {
                $subscriberCollection = Mage::getModel('monkey/asyncsubscribers')
                    ->getCollection()
                    ->addFieldToFilter('email', $customerEmail);
                if (!$subscriberCollection->count()) return;
                $subscriberInfo = $subscriberCollection->getFirstItem();
                $subscribedDate = $subscriberInfo->getCreatedAt();
                if (strtotime(Mage::getModel('core/date')->gmtDate()) - strtotime($subscribedDate) > 2 * 7 * 24 * 60 * 60) {
                    //voucher will be expired after customer subscribed for 2 weeks
                    $quote->setCouponCode('');
                }
            }*/
        }
    }
    public function applyCoupon($observer)
    {
        if (Mage::app()->getRequest()->getPost('coupon_code')) {
            $quote = $observer->getQuote();
            $couponCode = $quote->getCouponCode();
            $couponCodeVerify = strtolower(Mage::getStoreConfig('newsletter/subscription/coupon'));
            $couponCodeVerify = explode(',', $couponCodeVerify);
            if (!in_array(strtolower($couponCode), $couponCodeVerify)) return;

            $item = $observer->getItem();
            $attributeSetId = $item->getProduct()->getAttributeSetId();
            if ($attributeSetId == 9 || $attributeSetId == 10) {
                Mage::throwException(Mage::helper('salesrule')->__('Der Gutschein kann nicht für Bücher oder E-Books verwendet werden!'));
            }

            $customer = $quote->getCustomer();
            if ($customer->getId()) {
                $customerEmail = $customer->getEmail();
            } else {
                $email = $quote->getBillingAddress()->getEmail();
                if (!$email) $email = $quote->getCustomerEmail();
                if (!Zend_Validate::is($email, 'EmailAddress')) {
                    return;
                } else {
                    $customerEmail = $email;
                }
            }
            if ($customerEmail != '') {

                $orderCollection = Mage::getModel('sales/order')->getCollection()
                    ->addFieldToFilter('customer_email', $customerEmail)
                    ->addFieldToFilter('coupon_code', strtolower($couponCode));

                if ($orderCollection->count()) {
                    Mage::throwException(Mage::helper('salesrule')->__('Sie haben diesen Gutschein bereits eingelöst.'));
                }

                $helperMailjet = new Mailjet_Iframes_Helper_SyncManager();
                $apiOverlay = $helperMailjet->getApiInstance();
                $sync = new Mailjet_Iframes_Helper_SynchronizationCustom($apiOverlay);
                $createdAt = $sync->getContactCreated($customerEmail);
                if ($createdAt != false) {
                    $subscribedDate =  $createdAt;
                    if (strtotime(Mage::getModel('core/date')->gmtDate()) - strtotime($subscribedDate) > 2 * 7 * 24 * 60 * 60) {
                        Mage::throwException(Mage::helper('salesrule')->__('Dieser Gutschein ist nicht mehr gültig!'));
                    }
                }

                /*$listId = Mage::getStoreConfig(Ebizmarts_MageMonkey_Model_Config::GENERAL_LIST);
                $member = Mage::getSingleton('monkey/api')
                    ->listMemberInfo($listId, $customerEmail);
                if (!is_string($member) && $member['success'] && ($member['data'][0]['status'] == 'subscribed' || $member['data'][0]['status'] == 'pending')) {
                    $subscribedDate = $member['data'][0]['timestamp_signup'];
                    if (strtotime(Mage::getModel('core/date')->gmtDate()) - strtotime($subscribedDate) > 2 * 7 * 24 * 60 * 60) {
                        Mage::throwException(Mage::helper('salesrule')->__('Dieser Gutschein ist nicht mehr gültig!'));
                    }
                } else {
                    $subscriberCollection = Mage::getModel('monkey/asyncsubscribers')
                        ->getCollection()
                        ->addFieldToFilter('email', $customerEmail);
                    if (!$subscriberCollection->count()) return;
                    $subscriberInfo = $subscriberCollection->getFirstItem();
                    $subscribedDate = $subscriberInfo->getCreatedAt();
                    if (strtotime(Mage::getModel('core/date')->gmtDate()) - strtotime($subscribedDate) > 2 * 7 * 24 * 60 * 60) {
                        //voucher will be expired after customer subscribed for 2 weeks
                        Mage::throwException(Mage::helper('salesrule')->__('Dieser Gutschein ist nicht mehr gültig!'));
                    }
                }*/
            }
        }
    }
}