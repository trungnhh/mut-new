<?php

class Mut_Catalog_Model_Config extends Mage_Catalog_Model_Config {

    /**
     * Get list attribute/field use for sort
     * @return array
     */
    public function getAttributesUsedForSortBy()
    {
        $options = parent::getAttributesUsedForSortBy();
        if (!isset($options['created_at'])) {
            $newOption = new Varien_Object();
            $newOption->setAttributeCode('created_at');
            $newOption->setFrontendLabel('Date created');
            $newOption->setStoreLabel('Date created');
            $options['created_at'] = $newOption;
        }
        return $options;
    }
}