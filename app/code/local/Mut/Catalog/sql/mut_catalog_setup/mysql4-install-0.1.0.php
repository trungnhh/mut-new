<?php
/**
 * Mut Setup Attribute
 *
 * @category  Mut
 * @package   Mut_Catalog
 * @author    EB dev Team <hoang.vjet@gmail.com>
 * @copyright 2015 Ebluestore
 */


$installer = $this;
$installer->startSetup();

$attributeId = $installer->getAttributeId('catalog_product', 'system_require');
if(!$attributeId){
    $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'system_require', array(
        'group' => 'General',
        'input' => 'textarea',
        'type' => 'text',
        'label' => 'Systemvoraussetzungen',
        'backend' => '',
        'wysiwyg_enabled' => true,
        'visible' => true,
        'required' => false,
        'user_defined' => true,
        'searchable' => false,
        'filterable' => false,
        'comparable' => false,
        'visible_on_front' => false,
        'visible_in_advanced_search' => false,
        'is_html_allowed_on_front' => false,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));
}

$attributeId = $installer->getAttributeId('catalog_product', 'system_require_title');
if(!$attributeId){
    $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'system_require_title', array(
		'group' => 'General',
		'input' => 'text',
		'type' => 'text',
		'label' => 'System require title',
		'backend' => '',
		'visible' => true,
		'required' => false,
		'user_defined' => true,
		'searchable' => false,
		'filterable' => false,
		'comparable' => false,
		'visible_on_front' => false,
		'visible_in_advanced_search' => false,
		'is_html_allowed_on_front' => false,
		'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
	));
}
$installer->endSetup(); 