<?php

/**
 * Newsletter subscribe controller
 *
 * @category    Mut
 * @package     Mut_Newsletter
 * @author      Ebluestore Team <hoang.vjet@gmail.com>
 */
include Mage::getModuleDir('controllers','Mage_Newsletter').DS.'SubscriberController.php';
class Mut_Newsletter_SubscriberController extends Mage_Newsletter_SubscriberController
{
    /**
      * New subscription action
      */
    public function newAction()
    {
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('email')) {
            $session            = Mage::getSingleton('core/session');
            $customerSession    = Mage::getSingleton('customer/session');
            $email              = (string) $this->getRequest()->getPost('email');
            $type               = (int) $this->getRequest()->getPost('unsubscribe');
            if ($type != 1) {
                // unsubscribe
                $subscribe = Mage::getModel('newsletter/subscriber')->loadByEmail($email);
                if ($subscribe->getId()) {
                    $ownerId = Mage::getModel('customer/customer')
                        ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                        ->loadByEmail($email)
                        ->getId();
                    if ($ownerId !== null && $ownerId != $customerSession->getId()) {
                        $session->addError($this->__('This email address is already assigned to another user. Please login and try again.'));
                    } else {
                        $subscribe->unsubscribe();
                        $session->addSuccess($this->__('You have been unsubscribed.'));
                    }
                } else {
                    $session->addError($this->__('There was a problem with the un-subscription please try again later.'));
                }
            } else {
                try {
                    if (!Zend_Validate::is($email, 'EmailAddress')) {
                        Mage::throwException($this->__('Please enter a valid email address.'));
                    }

                    if (Mage::getStoreConfig(Mage_Newsletter_Model_Subscriber::XML_PATH_ALLOW_GUEST_SUBSCRIBE_FLAG) != 1 &&
                        !$customerSession->isLoggedIn()) {
                        Mage::throwException($this->__('Sorry, but administrator denied subscription for guests. Please <a href="%s">register</a>.', Mage::helper('customer')->getRegisterUrl()));
                    }

                    $ownerId = Mage::getModel('customer/customer')
                            ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                            ->loadByEmail($email)
                            ->getId();
                    if ($ownerId !== null && $ownerId != $customerSession->getId()) {
                        Mage::throwException($this->__('This email address is already assigned to another user.'));
                    }

                    $status = Mage::getModel('newsletter/subscriber')->subscribe($email);
                    if ($status == Mage_Newsletter_Model_Subscriber::STATUS_NOT_ACTIVE) {
                        $session->addSuccess($this->__('Confirmation request has been sent.'));
                    }
                    else {
                        $session->addSuccess($this->__('Thank you for your subscription.'));
                    }
                }
                catch (Mage_Core_Exception $e) {
                    $session->addException($e, $this->__('There was a problem with the subscription: %s', $e->getMessage()));
                }
                catch (Exception $e) {
                    $session->addException($e, $this->__('There was a problem with the subscription.'));
                }
            }
        }
        $this->_redirectReferer();
    }

    public function popupPostAction()
    {
        $session = Mage::getSingleton('core/session');
        $customerSession = Mage::getSingleton('customer/session');
        $email = (string) $this->getRequest()->getParam('email', '');
        $email = trim($email);
        if (!Zend_Validate::is($email, 'EmailAddress')) {
            $session->addError($this->__('Email is not valid.'));
            return $this->_redirect('newsletter/subscriber/form');
        }
        $ownerId = Mage::getModel('customer/customer')
            ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
            ->loadByEmail($email)
            ->getId();
        if ($ownerId !== null && $ownerId != $customerSession->getId()) {
            $session->addError($this->__('This email address is already assigned to another user. Please login and try again.'));
            return $this->_redirect('newsletter/subscriber/form');
        }
        $session->setNewsletterEmail($email);
        return $this->_redirect('newsletter/subscriber/form');
    }

    public function formAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function formPostAction()
    {
        $session = Mage::getSingleton('core/session');
        $customerSession = Mage::getSingleton('customer/session');
        $data = $this->getRequest()->getParams();
        $email = $data['email'];
        $email = trim($email);
        $session->setNewsletterData($data);
        if (!Zend_Validate::is($email, 'EmailAddress') || !$this->_validateEmailDomain($email)) {
            $session->addError($this->__('Email is not valid.'));
            return $this->_redirect('newsletter/subscriber/form');
        }
        $ownerId = Mage::getModel('customer/customer')
            ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
            ->loadByEmail($email)
            ->getId();

        $session->setNewsletterEmail($email);

        if ($ownerId !== null && $ownerId != $customerSession->getId()) {
            $session->addError($this->__('This email address is already assigned to another user. Please login and try again.'));
            return $this->_redirect('newsletter/subscriber/form');
        }

        try {
            $data['flag'] = md5($data['firstname'] . $data['lastname'] . $data['email'] . $data['prefix'] . $data['street'] . $data['postcode'] . $data['country_id'] . 'mut2016');
            $session->setNewsletterConfirmationLink($this->_getConfirmationLink($data));
            $status = Mage::getModel('newsletter/subscriber')
                ->setSubscriberFirstname($data['firstname'])
                ->setSubscriberLastname($data['lastname'])
                ->subscribe($email);
            if ($status == Mage_Newsletter_Model_Subscriber::STATUS_NOT_ACTIVE) {
                $session->setCompleteFormStatus(true);
                $session->addSuccess($this->__('Die E-Mail mit dem Verifizierungslink wurde soeben an Sie versandt. Bitte prüfen Sie Ihren Posteingang, öffnen die Mail und klicken auf den darin enthaltenen Bestätigungslink'));
            }
            else {
                $session->addSuccess($this->__('Thank you for your subscription.'));
            }
            $data = $this->getRequest()->getParams();
            $email = $data['email'];
            $email = trim($email);

            $helperMailjet = new Mailjet_Iframes_Helper_SyncManager();
            $apiOverlay = $helperMailjet->getApiInstance();
            $sync = new Mailjet_Iframes_Helper_SynchronizationCustom($apiOverlay);
            $updateOnlyGiven = false;
            $createdAt = $sync->getContactCreated($email);

            $alreadyOnList = 0;

            if ($createdAt != false) {
                $alreadyOnList = 1;
            }

            if ($alreadyOnList == 0) {

                $salutation = "Frau";
                if (isset($data['prefix'])) {
                    if (strtolower($data['prefix']) == 'herr' || strtolower($data['prefix']) =='male' || strtolower($data['prefix']) == 'frau') {
                        $salutation = "Herr";
                    }
                }

                try {
                    $contactData = array(
                        'email' => $data['email'],
                        'firstname' => $data['firstname']?$data['firstname']:'',
                        'lastname' => $data['lastname']?$data['lastname']:'',
                        'salutation' => $salutation,
                        'product_code' => 'NLA-47653000',
                        'street' => $data['street'],
                        'city' => $data['city'],
                        'state' => $data['city'],
                        'zip' => $data['postcode'],
                        'country' => Mage::getModel('directory/country')->load($data['country_id'])->getName(),
                        'language' => 'de',
                    );
                    $contactData['vorname'] = $contactData['firstname'];
                    $contactData['nachname'] = $contactData['lastname'];

                    //@TODO
                    $contactData['land'] = $contactData['country'];

                    $subscribersData = array();
                    $subscribersData[] = $contactData;
                    $result = $sync->synchronize($subscribersData, $updateOnlyGiven);

                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }

        }
        catch (Mage_Core_Exception $e) {
            $session->addException($e, $this->__('There was a problem with the subscription: %s', $e->getMessage()));
        }
        catch (Exception $e) {
            $session->addException($e, $this->__('There was a problem with the subscription.'));
        }

        return $this->_redirect('newsletter/subscriber/form');
    }

    public function newsletterconfirmAction()
    {
        // For Emailjet
        $data = $this->getRequest()->getParams();
        if ($data['flag'] != md5($data['firstname'] . $data['lastname'] . $data['email'] . $data['prefix'] . $data['street'] . $data['postcode'] . $data['country_id'] . 'mut2016')) {
            //return $this->_redirect('/');
        }
        $email = $data['email'];
        $email = trim($email);

        $helperMailjet = new Mailjet_Iframes_Helper_SyncManager();
        $apiOverlay = $helperMailjet->getApiInstance();
        $sync = new Mailjet_Iframes_Helper_SynchronizationCustom($apiOverlay);
        $updateOnlyGiven = false;
        $createdAt = $sync->getContactCreated($email);

        $alreadyOnList = 0;

        if ($createdAt != false) {
            $alreadyOnList = 1;
        }

        if ($alreadyOnList == 0) {

            $salutation = "Frau";
            $perAndree = "Sehr geehrte Frau";
            if (isset($data['prefix'])) {
                if (strtolower($data['prefix']) == 'herr' || strtolower($data['prefix']) =='male') {
                    $salutation = 'Herr';
                    $perAndree = "Sehr geehrte Herr";
                }
            }

            try {
                $registerDate = strtotime(now());
                $registerDate = date('m/d/Y H:i', $registerDate);

                //$registerDate= date("c", $registerDate);
                $contactData = array(
                    'email' => $data['email'],
                    'firstname' => $data['firstname']?$data['firstname']:'',
                    'lastname' => $data['lastname']?$data['lastname']:'',
                    'product_code' => 'NLA-47653000',
                    'salutation' => $salutation,
                    'street' => $data['street'],
                    'city' => $data['city'],
                    'state' => $data['city'],
                    'zip' => $data['postcode'],
                    'country' => Mage::getModel('directory/country')->load($data['country_id'])->getName(),
                    'language' => $this->country2lang($data['country_id']),
                    'country_iso' => $data['country_id'],
                    'pers_anrede' => $perAndree,
                    'register_sprache' => $this->country2lang($data['country_id']),
                    'opt_in' => 1,
                    'frequency' => 'daily',
                    //'registered_since' => strtotime(now())//$registerDate @TODO ask mailjet problem datetime missed
                    
                );
                //var_dump($contactData);exit;
                $contactData['vorname'] = $contactData['firstname'];
                $contactData['nachname'] = $contactData['lastname'];

                //@TODO
                $contactData['land'] = $contactData['country'];

                $subscribersData = array();
                $subscribersData[] = $contactData;
                $result = $sync->synchronize($subscribersData, $updateOnlyGiven);
            } catch (Exception $e) {
                //echo $e->getMessage();exit;
                Mage::logException($e);
            }
            
            Mage::getModel('newsletter/subscriber')->loadByEmail($email)
                ->setStatus(1)
                ->setIsStatusChanged(true)
                ->save();
            return $this->_redirect('newsletter/subscriber/success');
        }
        return $this->_redirect('/');
    }

    protected function _getConfirmationLink($data)
    {
        return Mage::getUrl('newsletter/subscriber/newsletterconfirm', array('_query' => $data));
    }

    public function successAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    protected function _validateEmailDomain($email)
    {
        return true;

        list($username, $domain) = explode('@', $email);
        if (!checkdnsrr($domain,'MX')) {
            return false;
        }
        return true;
    }

    public static function country2lang($code) {
        # http://wiki.openstreetmap.org/wiki/Nominatim/Country_Codes
        $arr = array(
            'ad' => 'ca',
            'ae' => 'ar',
            'af' => 'fa,ps',
            'ag' => 'en',
            'ai' => 'en',
            'al' => 'sq',
            'am' => 'hy',
            'an' => 'nl,en',
            'ao' => 'pt',
            'aq' => 'en',
            'ar' => 'es',
            'as' => 'en,sm',
            'at' => 'de',
            'au' => 'en',
            'aw' => 'nl,pap',
            'ax' => 'sv',
            'az' => 'az',
            'ba' => 'bs,hr,sr',
            'bb' => 'en',
            'bd' => 'bn',
            'be' => 'nl,fr,de',
            'bf' => 'fr',
            'bg' => 'bg',
            'bh' => 'ar',
            'bi' => 'fr',
            'bj' => 'fr',
            'bl' => 'fr',
            'bm' => 'en',
            'bn' => 'ms',
            'bo' => 'es,qu,ay',
            'br' => 'pt',
            'bq' => 'nl,en',
            'bs' => 'en',
            'bt' => 'dz',
            'bv' => 'no',
            'bw' => 'en,tn',
            'by' => 'be,ru',
            'bz' => 'en',
            'ca' => 'en,fr',
            'cc' => 'en',
            'cd' => 'fr',
            'cf' => 'fr',
            'cg' => 'fr',
            'ch' => 'de,fr,it,rm',
            'ci' => 'fr',
            'ck' => 'en,rar',
            'cl' => 'es',
            'cm' => 'fr,en',
            'cn' => 'zh',
            'co' => 'es',
            'cr' => 'es',
            'cu' => 'es',
            'cv' => 'pt',
            'cw' => 'nl',
            'cx' => 'en',
            'cy' => 'el,tr',
            'cz' => 'cs',
            'de' => 'de',
            'dj' => 'fr,ar,so',
            'dk' => 'da',
            'dm' => 'en',
            'do' => 'es',
            'dz' => 'ar',
            'ec' => 'es',
            'ee' => 'et',
            'eg' => 'ar',
            'eh' => 'ar,es,fr',
            'er' => 'ti,ar,en',
            'es' => 'es,ast,ca,eu,gl',
            'et' => 'am,om',
            'fi' => 'fi,sv,se',
            'fj' => 'en',
            'fk' => 'en',
            'fm' => 'en',
            'fo' => 'fo',
            'fr' => 'fr',
            'ga' => 'fr',
            'gb' => 'en,ga,cy,gd,kw',
            'gd' => 'en',
            'ge' => 'ka',
            'gf' => 'fr',
            'gg' => 'en',
            'gh' => 'en',
            'gi' => 'en',
            'gl' => 'kl,da',
            'gm' => 'en',
            'gn' => 'fr',
            'gp' => 'fr',
            'gq' => 'es,fr,pt',
            'gr' => 'el',
            'gs' => 'en',
            'gt' => 'es',
            'gu' => 'en,ch',
            'gw' => 'pt',
            'gy' => 'en',
            'hk' => 'zh,en',
            'hm' => 'en',
            'hn' => 'es',
            'hr' => 'hr',
            'ht' => 'fr,ht',
            'hu' => 'hu',
            'id' => 'id',
            'ie' => 'en,ga',
            'il' => 'he',
            'im' => 'en',
            'in' => 'hi,en',
            'io' => 'en',
            'iq' => 'ar,ku',
            'ir' => 'fa',
            'is' => 'is',
            'it' => 'it,de,fr',
            'je' => 'en',
            'jm' => 'en',
            'jo' => 'ar',
            'jp' => 'ja',
            'ke' => 'sw,en',
            'kg' => 'ky,ru',
            'kh' => 'km',
            'ki' => 'en',
            'km' => 'ar,fr',
            'kn' => 'en',
            'kp' => 'ko',
            'kr' => 'ko,en',
            'kw' => 'ar',
            'ky' => 'en',
            'kz' => 'kk,ru',
            'la' => 'lo',
            'lb' => 'ar,fr',
            'lc' => 'en',
            'li' => 'de',
            'lk' => 'si,ta',
            'lr' => 'en',
            'ls' => 'en,st',
            'lt' => 'lt',
            'lu' => 'lb,fr,de',
            'lv' => 'lv',
            'ly' => 'ar',
            'ma' => 'ar',
            'mc' => 'fr',
            'md' => 'ru,uk,ro',
            'me' => 'srp,sq,bs,hr,sr',
            'mf' => 'fr',
            'mg' => 'mg,fr',
            'mh' => 'en,mh',
            'mk' => 'mk',
            'ml' => 'fr',
            'mm' => 'my',
            'mn' => 'mn',
            'mo' => 'zh,en,pt',
            'mp' => 'ch',
            'mq' => 'fr',
            'mr' => 'ar,fr',
            'ms' => 'en',
            'mt' => 'mt,en',
            'mu' => 'mfe,fr,en',
            'mv' => 'dv',
            'mw' => 'en,ny',
            'mx' => 'es',
            'my' => 'ms,zh,en',
            'mz' => 'pt',
            'na' => 'en,sf,de',
            'nc' => 'fr',
            'ne' => 'fr',
            'nf' => 'en,pih',
            'ng' => 'en',
            'ni' => 'es',
            'nl' => 'nl',
            'no' => 'nb,nn,no,se',
            'np' => 'ne',
            'nr' => 'na,en',
            'nu' => 'niu,en',
            'nz' => 'en,mi',
            'om' => 'ar',
            'pa' => 'es',
            'pe' => 'es',
            'pf' => 'fr',
            'pg' => 'en,tpi,ho',
            'ph' => 'en,tl',
            'pk' => 'en,ur',
            'pl' => 'pl',
            'pm' => 'fr',
            'pn' => 'en,pih',
            'pr' => 'es,en',
            'ps' => 'ar,he',
            'pt' => 'pt',
            'pw' => 'en,pau,ja,sov,tox',
            'py' => 'es,gn',
            'qa' => 'ar',
            're' => 'fr',
            'ro' => 'ro',
            'rs' => 'sr',
            'ru' => 'ru',
            'rw' => 'rw,fr,en',
            'sa' => 'ar',
            'sb' => 'en',
            'sc' => 'fr,en,crs',
            'sd' => 'ar,en',
            'se' => 'sv',
            'sg' => 'en,ms,zh,ta',
            'sh' => 'en',
            'si' => 'sl',
            'sj' => 'no',
            'sk' => 'sk',
            'sl' => 'en',
            'sm' => 'it',
            'sn' => 'fr',
            'so' => 'so,ar',
            'sr' => 'nl',
            'st' => 'pt',
            'ss' => 'en',
            'sv' => 'es',
            'sx' => 'nl,en',
            'sy' => 'ar',
            'sz' => 'en,ss',
            'tc' => 'en',
            'td' => 'fr,ar',
            'tf' => 'fr',
            'tg' => 'fr',
            'th' => 'th',
            'tj' => 'tg,ru',
            'tk' => 'tkl,en,sm',
            'tl' => 'pt,tet',
            'tm' => 'tk',
            'tn' => 'ar',
            'to' => 'en',
            'tr' => 'tr',
            'tt' => 'en',
            'tv' => 'en',
            'tw' => 'zh',
            'tz' => 'sw,en',
            'ua' => 'uk',
            'ug' => 'en,sw',
            'um' => 'en',
            'us' => 'en,es',
            'uy' => 'es',
            'uz' => 'uz,kaa',
            'va' => 'it',
            'vc' => 'en',
            've' => 'es',
            'vg' => 'en',
            'vi' => 'en',
            'vn' => 'vi',
            'vu' => 'bi,en,fr',
            'wf' => 'fr',
            'ws' => 'sm,en',
            'ye' => 'ar',
            'yt' => 'fr',
            'za' => 'zu,xh,af,st,tn,en',
            'zm' => 'en',
            'zw' => 'en,sn,nd'
        );
        #----
        $code = strtolower($code);
        if ($code == 'eu') {
            return 'en';
        }
        elseif ($code == 'ap') { # Asia Pacific
            return 'en';
        }
        elseif ($code == 'cs') {
            return 'sr';
        }
        #----
        if ($code == 'uk') {
            $code = 'gb';
        }
        if (array_key_exists($code, $arr)) {
            if (strpos($arr[$code], ',') !== false) {
                $new = explode(',', $arr[$code]);
                foreach ($new as $key => $val) {
                    return $val;
                }
            }
            else {
                return $arr[$code];
            }
        }
        return 'en';
    }
}
