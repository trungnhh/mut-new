<?php

/**
 * Newsletter subscribe controller
 *
 * @category    Mut
 * @package     Mut_Newsletter
 * @author      Ebluestore Team <hoang.vjet@gmail.com>
 */
include Mage::getModuleDir('controllers','Mage_Newsletter').DS.'ManageController.php';
class Mut_Newsletter_ManageController extends Mage_Core_Controller_Front_Action
{
    /**
     * Action predispatch
     *
     * Check customer authentication for some actions
     */
    public function preDispatch()
    {
        parent::preDispatch();
        if ($this->getRequest()->getActionName() == 'import' || $this->getRequest()->getActionName() == 'unsubscribe') {
            return true;
        }
        if (!Mage::getSingleton('customer/session')->authenticate($this)) {
            $this->setFlag('', 'no-dispatch', true);
        }
    }
    public function importAction()
    {
        // For Mailjet
        $helperMailjet = new Mailjet_Iframes_Helper_SyncManager();
        $apiOverlay = $helperMailjet->getApiInstance();
        $sync = new Mailjet_Iframes_Helper_SynchronizationCustom($apiOverlay);
        $updateOnlyGiven = false;


        $data = $this->getRequest()->getParams();
        if (isset($data['email'])) {
            try {
                $salutation = "Frau";
                if (isset($data['salutation'])) {
                    if (strtolower($data['salutation']) == 'herr' || strtolower($data['salutation']) =='male' || strtolower($data['salutation']) == 'Frau' || strtolower($data['salutation']) == 'frau') {
                        $salutation = "Herr";
                    }
                }
                //customer=MuT&list=MuT Register&date=2060715&code=EDS-31770000&prm=102&email=regimp1@bitsell.de&salutation=Frau&firstName=Susanne&lastName=Müller
                //email=hoang.vjet+2308@gmail.com&firstname=test&lastname=teset&salutation=Herr&gender=Male&register_date=14.11.2016%2014:19:14&segment=Software&group=Druckerei&product=Der%20Ahnenforscher%206&NO_CACHE=1
                $magentoGroup = 'General';
                $contactData = array(
                    'email' => $data['email'],
                    'firstname' => $data['firstname']?$data['firstname']:'',
                    'lastname' => $data['lastname']?$data['lastname']:'',
                    //'salutation' => $salutation,
                    'anrede' => $data['salutation']?$data['salutation']:'',
                    'gruppe' => $magentoGroup,
                    'reg_product_name' => $data['product']?$data['product']:'',
                    'segment' => $data['segment']?$data['segment']:'',
                    'gender' => $data['gender']?$data['gender']:'',
                    'product_code' => $data['code']?$data['code']:'',
                    'prm' => $data['prm']?$data['prm']:'',
                    'language' => 'de'
                );
                $contactData['vorname'] = $contactData['firstname'];
                $contactData['nachname'] = $contactData['lastname'];
                if ($data['register_date']) {
                    $registerDate = strtotime($data['register_date']);
                    $registerDate = date('Y-m-d\TH:i:sP', $registerDate);
                    //$contactData['registered_since'] = $registerDate;@TODO
                } else {
                    //$contactData['registered_since'] = '';@TODO
                }
                $subscribersData = array();
                $subscribersData[] = $contactData;
                $result = $sync->synchronize($subscribersData, $updateOnlyGiven);
                $session            = Mage::getSingleton('core/session');
                $session->addSuccess($this->__('Imported please check on Mailjet.'));
            } catch (Exception $e) {
                $session            = Mage::getSingleton('core/session');
                $session->addError($this->__('We can not import now please try again later.'));
            }
        }
        $this->_redirectReferer();
    }

    public function indexAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');

        if ($block = $this->getLayout()->getBlock('customer_newsletter')) {
            $block->setRefererUrl($this->_getRefererUrl());
        }
        $this->getLayout()->getBlock('head')->setTitle($this->__('Newsletter Subscription'));
        $this->renderLayout();
    }

    public function saveAction()
    {
        if (!$this->_validateFormKey()) {
            return $this->_redirect('customer/account/');
        }
        try {
            Mage::getSingleton('customer/session')->getCustomer()
                ->setStoreId(Mage::app()->getStore()->getId())
                ->setIsSubscribed((boolean)$this->getRequest()->getParam('is_subscribed', false))
                ->save();
            if ((boolean)$this->getRequest()->getParam('is_subscribed', false)) {
                Mage::getSingleton('customer/session')->addSuccess($this->__('The subscription has been saved.'));
            } else {
                Mage::getSingleton('customer/session')->addSuccess($this->__('The subscription has been removed.'));
            }
        }
        catch (Exception $e) {
            Mage::getSingleton('customer/session')->addError($this->__('An error occurred while saving your subscription.'));
        }
        $this->_redirect('customer/account/');
    }

    public function unsubscribeAction()
    {
        $session = Mage::getSingleton('core/session');
        $email = (string) $this->getRequest()->getParam('email', '');
        if (!Zend_Validate::is($email, 'EmailAddress')) {
            Mage::throwException($this->__('Please enter a valid email address.'));
        }
        $session->setNewsletterUnsubscribeEmail($email);
        $this->loadLayout();
        $this->renderLayout();
    }
}
