<?php

/**
 * Newsletter subscribe controller
 *
 * @category    Mut
 * @package     Mut_Newsletter
 * @author      Ebluestore Team <hoang.vjet@gmail.com>
 */
include Mage::getModuleDir('controllers','Mage_Newsletter').DS.'SubscriberController.php';
class Mut_Newsletter_SubscriberController extends Mage_Newsletter_SubscriberController
{
    /**
      * New subscription action
      */
    public function newAction()
    {
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('email')) {
            $session            = Mage::getSingleton('core/session');
            $customerSession    = Mage::getSingleton('customer/session');
            $email              = (string) $this->getRequest()->getPost('email');
            $type               = (int) $this->getRequest()->getPost('unsubscribe');
            if ($type != 1) {
                // unsubscribe
                $subscribe = Mage::getModel('newsletter/subscriber')->loadByEmail($email);
                if ($subscribe->getId()) {
                    $ownerId = Mage::getModel('customer/customer')
                        ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                        ->loadByEmail($email)
                        ->getId();
                    if ($ownerId !== null && $ownerId != $customerSession->getId()) {
                        $session->addError($this->__('This email address is already assigned to another user. Please login and try again.'));
                    } else {
                        $subscribe->unsubscribe();
                        $session->addSuccess($this->__('You have been unsubscribed.'));
                    }
                } else {
                    $session->addError($this->__('There was a problem with the un-subscription please try again later.'));
                }
            } else {
                try {
                    if (!Zend_Validate::is($email, 'EmailAddress')) {
                        Mage::throwException($this->__('Please enter a valid email address.'));
                    }

                    if (Mage::getStoreConfig(Mage_Newsletter_Model_Subscriber::XML_PATH_ALLOW_GUEST_SUBSCRIBE_FLAG) != 1 &&
                        !$customerSession->isLoggedIn()) {
                        Mage::throwException($this->__('Sorry, but administrator denied subscription for guests. Please <a href="%s">register</a>.', Mage::helper('customer')->getRegisterUrl()));
                    }

                    $ownerId = Mage::getModel('customer/customer')
                            ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                            ->loadByEmail($email)
                            ->getId();
                    if ($ownerId !== null && $ownerId != $customerSession->getId()) {
                        Mage::throwException($this->__('This email address is already assigned to another user.'));
                    }

                    $status = Mage::getModel('newsletter/subscriber')->subscribe($email);
                    if ($status == Mage_Newsletter_Model_Subscriber::STATUS_NOT_ACTIVE) {
                        $session->addSuccess($this->__('Confirmation request has been sent.'));
                    }
                    else {
                        $session->addSuccess($this->__('Thank you for your subscription.'));
                    }
                }
                catch (Mage_Core_Exception $e) {
                    $session->addException($e, $this->__('There was a problem with the subscription: %s', $e->getMessage()));
                }
                catch (Exception $e) {
                    $session->addException($e, $this->__('There was a problem with the subscription.'));
                }
            }
        }
        $this->_redirectReferer();
    }

    public function popupPostAction()
    {
        $session = Mage::getSingleton('core/session');
        $customerSession = Mage::getSingleton('customer/session');
        $email = (string) $this->getRequest()->getParam('email', '');
        $email = trim($email);
        if (!Zend_Validate::is($email, 'EmailAddress')) {
            $session->addError($this->__('Email is not valid.'));
            return $this->_redirect('newsletter/subscriber/form');
        }
        $ownerId = Mage::getModel('customer/customer')
            ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
            ->loadByEmail($email)
            ->getId();
        if ($ownerId !== null && $ownerId != $customerSession->getId()) {
            $session->addError($this->__('This email address is already assigned to another user. Please login and try again.'));
            return $this->_redirect('newsletter/subscriber/form');
        }
        $session->setNewsletterEmail($email);
        return $this->_redirect('newsletter/subscriber/form');
    }

    public function formAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function formPostAction()
    {
        $session = Mage::getSingleton('core/session');
        $customerSession = Mage::getSingleton('customer/session');
        $data = $this->getRequest()->getParams();
        $email = $data['email'];
        $email = trim($email);
        $session->setNewsletterData($data);
        if (!Zend_Validate::is($email, 'EmailAddress') || !$this->_validateEmailDomain($email)) {
            $session->addError($this->__('Email is not valid.'));
            return $this->_redirect('newsletter/subscriber/form');
        }
        $ownerId = Mage::getModel('customer/customer')
            ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
            ->loadByEmail($email)
            ->getId();

        $session->setNewsletterEmail($email);

        if ($ownerId !== null && $ownerId != $customerSession->getId()) {
            $session->addError($this->__('This email address is already assigned to another user. Please login and try again.'));
            return $this->_redirect('newsletter/subscriber/form');
        }

        try {
            $data['flag'] = md5($data['firstname'] . $data['lastname'] . $data['email'] . $data['prefix'] . $data['street'] . $data['postcode'] . $data['country_id'] . 'mut2016');
            $session->setNewsletterConfirmationLink($this->_getConfirmationLink($data));
            $status = Mage::getModel('newsletter/subscriber')
                ->setSubscriberFirstname($data['firstname'])
                ->setSubscriberLastname($data['lastname'])
                ->subscribe($email);
            if ($status == Mage_Newsletter_Model_Subscriber::STATUS_NOT_ACTIVE) {
                $session->setCompleteFormStatus(true);
                $session->addSuccess($this->__('Die E-Mail mit dem Verifizierungslink wurde soeben an Sie versandt. Bitte prüfen Sie Ihren Posteingang, öffnen die Mail und klicken auf den darin enthaltenen Bestätigungslink'));
            }
            else {
                $session->addSuccess($this->__('Thank you for your subscription.'));
            }
        }
        catch (Mage_Core_Exception $e) {
            $session->addException($e, $this->__('There was a problem with the subscription: %s', $e->getMessage()));
        }
        catch (Exception $e) {
            $session->addException($e, $this->__('There was a problem with the subscription.'));
        }

        return $this->_redirect('newsletter/subscriber/form');
    }

    public function newsletterconfirmAction()
    {
        // For emarsys
        $data = $this->getRequest()->getParams();
        if ($data['flag'] != md5($data['firstname'] . $data['lastname'] . $data['email'] . $data['prefix'] . $data['street'] . $data['postcode'] . $data['country_id'] . 'mut2016')) {
            return $this->_redirect('/');
        }
        $client = Mage::helper('mutnewsletter')->getEmarsysClient();

        $email = $data['email'];
        $email = trim($email);

        $alreadyOnList = 0;
        $result = $client->getContactData(array('keyId' => 3, 'keyValues'=>array($email)));
        $result = $result->getData();

        if ($result['result']!= false) {
            $alreadyOnList = 1;
        }

        if ($alreadyOnList == 0) {

            $salutation = 1;
            if (isset($data['prefix'])) {
                if (strtolower($data['prefix']) == 'herr' || strtolower($data['prefix']) =='male' ) {
                    $salutation = 2;
                }
            }
            try {
                $response = $client->createContact(array(
                    'email' => $email,
                    'salutation' => $salutation,
                    'firstName' => $data['firstname'],
                    'lastName' => $data['lastname'],
                    'address' => $data['street'],
                    'city' => $data['city'],
                    'state' => $data['city'],
                    'zip' => $data['postcode'],
                    'country' => Mage::getModel('directory/country')->load($data['country_id'])->getName(),
                    'registrationLanguage' => 2//'de'//'language' => 'de',
                ));
            } catch (Exception $e) {
                Mage::logException($e);
            }

            //@TODO Update if already exist

            Mage::getModel('newsletter/subscriber')->loadByEmail($email)
                ->setStatus(1)
                ->setIsStatusChanged(true)
                ->save();
            return $this->_redirect('newsletter/subscriber/success');
        }
        return $this->_redirect('/');
        // End For emarsys

        // For mailchimp
        /*
        $data = $this->getRequest()->getParams();
        if ($data['flag'] != md5($data['firstname'] . $data['lastname'] . $data['email'] . $data['prefix'] . $data['street'] . $data['postcode'] . $data['country_id'] . 'mut2016')) {
            return $this->_redirect('/');
        }

        $vars = array();
        $vars['FNAME'] = $data['firstname'];
        $vars['LNAME'] = $data['lastname'];
        $vars['PRENAME'] = $data['prefix'];
        $vars['BILLING'] = array(
            'addr1' => $data['street'],
            'addr2' => '',
            'city' => $data['city'],
            'state' => $data['city'],
            'zip' => $data['postcode'],
            'country' => $data['country_id'],
        );
        $vars['COUNTRY'] = Mage::getModel('directory/country')->load($data['country_id'])->getName();
        $vars['ZIPCODE'] = $data['postcode'];
        $vars['STOREID'] = Mage::app()->getStore()->getId();
        $vars['WEBSITE'] = Mage::app()->getWebsite()->getId();
        $vars['CGROUP'] = "NOT LOGGED IN";
        $vars['STORECOD'] = Mage::app()->getStore()->getCode();
        $vars['MC_LANGUAGE'] = 'de';

        $groupId = 3081;
        if (strpos(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB), 'www.mut.de')) {
            $groupId = 14057;//live site
        }
        $vars['GROUPINGS'] = array(
            0 => array(
                'id' => $groupId,
                'groups' => "Deals,Download-Deals,Partner-Angebote,Angebot der Woche,Neuerscheinungen"
            )
        );

        $email = $data['email'];
        $email = trim($email);
        $listId = Mage::getStoreConfig(Ebizmarts_MageMonkey_Model_Config::GENERAL_LIST, Mage::app()->getStore()->getId());
        $alreadyOnList = Mage::getSingleton('monkey/asyncsubscribers')->getCollection()
            ->addFieldToFilter('lists', $listId)
            ->addFieldToFilter('email', $email)
            ->addFieldToFilter('processed', 0);
        if (count($alreadyOnList) == 0) {
            $subs = Mage::getModel('monkey/asyncsubscribers');
            $alreadyOnDb = $subs->getCollection()
                ->addFieldToFilter('lists', $listId)
                ->addFieldToFilter('email', $email);
            if (!$alreadyOnDb->count()) {
                $subs->setMapfields(serialize($vars))
                    ->setEmail($email)
                    ->setLists($listId)
                    ->setConfirm(1)
                    ->setProcessed(0)
                    ->setCreatedAt(Mage::getModel('core/date')->gmtDate())
                    ->save();
            }
            Mage::getSingleton('monkey/api')->listSubscribe($listId, $email, $vars, 'html', false, true, false, false);
            Mage::getModel('newsletter/subscriber')->loadByEmail($email)
                ->setStatus(1)
                ->setIsStatusChanged(true)
                ->save();
            return $this->_redirect('newsletter/subscriber/success');
        }
        return $this->_redirect('/');
        */
        // End for mailchimp
    }

    protected function _getConfirmationLink($data)
    {
        return Mage::getUrl('newsletter/subscriber/newsletterconfirm', array('_query' => $data));
    }

    public function successAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    protected function _validateEmailDomain($email)
    {
        list($username, $domain) = explode('@', $email);
        if (!checkdnsrr($domain,'MX')) {
            return false;
        }
        return true;
    }
}
