<?php
class Mut_Newsletter_Model_Subscriber extends Mage_Newsletter_Model_Subscriber
{
    public function getConfirmationLink() {
        $sessionLink = Mage::getSingleton('core/session')->getNewsletterConfirmationLink();
        if ($sessionLink) {
            Mage::getSingleton('core/session')->unsNewsletterConfirmationLink();
            return $sessionLink;
        }
        return parent::getConfirmationLink();
    }
}