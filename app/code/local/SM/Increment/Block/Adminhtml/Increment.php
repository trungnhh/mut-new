<?php
class SM_Increment_Block_Adminhtml_Increment extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_increment';
        $this->_blockGroup = 'sm_increment';
        parent::__construct();
        $this->_headerText = Mage::helper('sm_increment')->__('Increments Management');
    }
}