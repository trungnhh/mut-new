<?php
class SM_Increment_Block_Adminhtml_Increment_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm(){
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('increment_');
        $form->setFieldNameSuffix('increment');
        $this->setForm($form);
        $fieldset = $form->addFieldset('information', array('legend'=>Mage::helper('sm_increment')->__('Information')));

        if (!Mage::app()->isSingleStoreMode()) {
            $fieldset->addField('store_id', 'select', array(
                'name' => 'store_id',
                'label' => Mage::helper('sm_increment')->__('Store View'),
                'title' => Mage::helper('sm_increment')->__('Store View'),
                'required' => true,
                'values' => Mage::getSingleton('adminhtml/system_store')
                    ->getStoreValuesForForm(false, true),
            ));
        }
        else {
            $fieldset->addField('store_id', 'hidden', array(
                'name' => 'store_id',
                'value' => Mage::app()->getStore(true)->getId()
            ));
        }

        $fieldset->addField('entity_type_id', 'select', array(
            'label' => Mage::helper('sm_increment')->__('Entity type code'),
            'name'  => 'entity_type_id',
            'required'  => true,
            'class' => 'required-entry',
            'values' => Mage::getModel('sm_increment/renderer_entity')->toOptionArray()

        ));

        $fieldset->addField('increment_prefix', 'text', array(
            'label' => Mage::helper('sm_increment')->__('Prefix'),
            'name'  => 'increment_prefix',
            'required'  => true,
            'class' => 'required-entry',

        ));

        $fieldset->addField('increment_last_id', 'text', array(
            'label' => Mage::helper('sm_increment')->__('Last ID'),
            'name'  => 'increment_last_id',
            'required'  => true,
            'class' => 'required-entry',
        ));

        $formValues = Mage::registry('current_increment')->getDefaultValues();
        if (!is_array($formValues)){
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getIncrementData()){
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getIncrementData());
            Mage::getSingleton('adminhtml/session')->setIncrementData(null);
        }
        elseif (Mage::registry('current_increment')){
            $formValues = array_merge($formValues, Mage::registry('current_increment')->getData());
        }
        $form->setValues($formValues);

        return parent::_prepareForm();
    }
}
