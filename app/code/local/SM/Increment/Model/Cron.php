<?php

class SM_Increment_Model_Cron
{
    /**
     * Update invoice increment yearly
     *
     */
    public function updateInvoiceIncrementYearly()
    {
        $increment    = Mage::getModel('eav/entity_store')->load(2); //2 is invoice id
        $currentYear = date('y');
        $old_increment_prefix = $increment->getData('increment_prefix');
        $old_increment_last_id = $increment->getData('increment_last_id');
        $increment_prefix = preg_replace('#[0-9]{2}$#', $currentYear, $old_increment_prefix);
        $increment_last_id = str_replace($old_increment_prefix, $increment_prefix, $old_increment_last_id);

        $increment->setData('increment_prefix', $increment_prefix);
        $increment->setData('increment_last_id', $increment_last_id);
        $increment->save();
    }
}