<?php
class SM_Increment_Model_Entity_Resource_Store_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        parent::_construct();
        $this->_init('eav/entity_store');
    }
}