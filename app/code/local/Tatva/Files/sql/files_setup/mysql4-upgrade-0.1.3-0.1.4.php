<?php
	$installer = $this;
	$installer->startSetup();

	$installer->run("
	ALTER TABLE {$this->getTable('files')}
	    ADD COLUMN sample_type varchar(255) default 'file',
	    ADD COLUMN sample_url varchar(255)
	");

	$installer->endSetup();