<?php
	$installer = $this;
	$installer->startSetup();

	$installer->run("
	ALTER TABLE {$this->getTable('files')}
	    ADD COLUMN file_customer_group varchar(255)
	");

	$installer->endSetup();