<?php
class IWD_Opc_Block_Wrapper extends  Mage_Core_Block_Template{
	
	const XML_PATH_DEFAULT_PAYMENT = 'opc/default/payment';
	
	const XML_PATH_DEFAULT_SHIPPING = 'opc/default/shipping';
	
	const XML_PATH_GEO_COUNTRY = 'opc/geo/country';
	
	const XML_PATH_GEO_CITY = 'opc/geo/city';
	
	
	
	/**
	 * Get one page checkout model
	 *
	 * @return Mage_Checkout_Model_Type_Onepage
	 */
	public function getOnepage(){
		return Mage::getSingleton('checkout/type_onepage');
	}
	
	protected function _getReviewHtml(){
	
		$defaultPaymentMethod = Mage::getStoreConfig(self::XML_PATH_DEFAULT_PAYMENT);
		$_cart = $this->_getCart();
		$_quote = $_cart->getQuote();
		$_quote->getPayment()->setMethod($defaultPaymentMethod);
		$_quote->setTotalsCollectedFlag(false)->collectTotals();
		$_quote->save();

		$nmethod  =   '';
		$cartItems = $_quote->getAllVisibleItems();
		foreach ($cartItems as $item) {
			$product    =   $item->getProduct();
			if ($product->getTypeId()=='downloadable') {
				$nmethod  =   'checkmo';
				break;
			}
		}
		$arrayCode = array();
		$store = $_quote ? $_quote->getStoreId() : null;
		foreach (Mage::helper('payment')->getStoreMethods($store, $_quote) as $method) {
			if($method->getCode()!=$nmethod){
				$arrayCode[] = $method->getCode();
			}
		}
		if(in_array('checkmo', $arrayCode)){
			$_quote->getPayment()->setMethod('checkmo');
			$_quote->setTotalsCollectedFlag(false)->collectTotals();
			$_quote->save();
		}elseif (in_array('paypal_express', $arrayCode)) {
			$_quote->getPayment()->setMethod('paypal_express');
			$_quote->setTotalsCollectedFlag(false)->collectTotals();
			$_quote->save();
		} else {
			$_quote->getPayment()->setMethod($arrayCode[0]);
			$_quote->setTotalsCollectedFlag(false)->collectTotals();
			$_quote->save();
		}

		$layout = $this->getLayout();
		$update = $layout->getUpdate();
		$update->load('checkout_onepage_review');
		$layout->generateXml();
		$layout->generateBlocks();
		$review = $layout->getBlock('root');
		$review->setTemplate('opc/onepage/review/info.phtml');
		
		return $review->toHtml();
	}
	
	protected function _getCart(){
		return Mage::getSingleton('checkout/cart');
	}

	
	public function getJsonConfig() {
	
		$config = array ();
		$params = array (
				'_secure' => true
		);	
		$config['baseUrl'] = Mage::getBaseUrl('link', true);
		$config['isLoggedIn'] = (int) Mage::getSingleton('customer/session')->isLoggedIn();
		
		$config['geoCountry'] =  Mage::getStoreConfig(self::XML_PATH_GEO_COUNTRY) ? Mage::helper('opc/country')->get() : false;
		$config['geoCity'] =  Mage::getStoreConfig(self::XML_PATH_GEO_CITY) ? Mage::helper('opc/city')->get() : false;
		$config['comment'] = Mage::helper('opc')->isShowComment();
		
		return Mage::helper ( 'core' )->jsonEncode ( $config );
	}
	
	
}