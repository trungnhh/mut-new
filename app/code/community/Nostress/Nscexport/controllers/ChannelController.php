<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@nostresscommerce.cz so we can send you a copy immediately.
 *
 * @copyright Copyright (c) 2012 NoStress Commerce (http://www.nostresscommerce.cz)
 *
 */

/**
 * Frontend kontroler pro exportni modul
 *
 * @category Nostress
 * @package Nostress_Nscexport
 *
 */

class Nostress_Nscexport_ChannelController extends Mage_Core_Controller_Front_Action 
{
	public function loadChannelManualAction()
	{
		$url = $this->getRequest()->getParam('url',"");
		if($url == 'empty')
		{
			$html = "<br>";
			$html .= Mage::helper('nscexport')->__("Feed submission information availabe only for users with valid Support and Updates period.")."<br>";
			$html .=  Mage::helper('nscexport')->__("License Status").": ".Mage::helper('nscexport/version')->getLicenseKeyStatusHtml(false);
			$this->getResponse()->setBody($html);
		}
		else 
		{
			try
			{
				$html = Mage::helper('nscexport')->sendCurlRequest($url);
			}
			catch(Exception $e)
			{
				$html = "<br>";
				$html .= Mage::helper('nscexport')->__("Subbmision information not available for this feed.");
			}
			$this->getResponse()->setBody($html);
		}				
	}
}