<?php
/** 
* Magento Module developed by NoStress Commerce 
* 
* NOTICE OF LICENSE 
* 
* This source file is subject to the Open Software License (OSL 3.0) 
* that is bundled with this package in the file LICENSE.txt. 
* It is also available through the world-wide-web at this URL: 
* http://opensource.org/licenses/osl-3.0.php 
* If you did of the license and are unable to 
* obtain it through the world-wide-web, please send an email 
* to info@nostresscommerce.cz so we can send you a copy immediately. 
* 
* @copyright Copyright (c) 2009 NoStress Commerce (http://www.nostresscommerce.cz) 
* 
*/ 

/** 
* Sql update skript
* Table stores relation product-category, where category has max level from categories related to the product
* 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

$this->startSetup()->run("
CREATE TABLE IF NOT EXISTS {$this->getTable('nostress_export_cache_categorymaxlevel')} ( 
	`product_id` int(10) unsigned NOT NULL,
	`store_id` smallint(5) unsigned NOT NULL,  
	`category_id` int(10) unsigned NOT NULL,
	`max_level` int(11) NOT NULL DEFAULT '-1',	
	PRIMARY KEY (`product_id`,`store_id`) 
	)ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Stores relation product-category, where category has max level from categories related to the product';    
")->endSetup();

