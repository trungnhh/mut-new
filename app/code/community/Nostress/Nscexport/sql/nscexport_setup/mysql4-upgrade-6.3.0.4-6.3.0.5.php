<?php
/** 
* Magento Module developed by NoStress Commerce 
* 
* NOTICE OF LICENSE 
* 
* This source file is subject to the Open Software License (OSL 3.0) 
* that is bundled with this package in the file LICENSE.txt. 
* It is also available through the world-wide-web at this URL: 
* http://opensource.org/licenses/osl-3.0.php 
* If you did of the license and are unable to 
* obtain it through the world-wide-web, please send an email 
* to info@nostresscommerce.cz so we can send you a copy immediately. 
* 
* @copyright Copyright (c) 2009 NoStress Commerce (http://www.nostresscommerce.cz) 
* 
*/ 

/** 
* Sql instalation skript
* 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

$installer = $this;

$installer->startSetup();

$helper = Mage::helper('nscexport/data_loader');
$taxonomyModel = Mage::getModel('nscexport/taxonomy');
$taxonomySetupCollection = Mage::getModel('nscexport/taxonomy_setup')->getCollection()->load();
$updateQuery = "";
$stores = $helper->getStoresWithLocale();

//transform category taxonomy mapping identificator to hash(from id)
foreach($taxonomySetupCollection as $taxonomyItem)
{
	$code =  $taxonomyItem->getCode();
	$config = $taxonomyItem->getDecodedSetup();
	$config = $taxonomyModel->prepareTaxonomyConfig($config);
	
	//get taxonomy attribute id
	$attributeCode = $helper->createCategoryAttributeCode($code);
	$attribute = Mage::getModel('eav/entity_attribute')->loadByCode(Mage_Catalog_Model_Category::ENTITY, $attributeCode);
	$attributeId = $attribute->getId();
	
	//get value column name
	$fields = $taxonomyModel->getSelectFields($code);
	$valueColumn = 'id';
	if(isset($fields['value']))
		$valueColumn = $fields['value'];
	
	foreach($stores as $store)
	{
		$storeId = $store->getId();		
		$locale = $helper->getStoreTaxonomyLocale($code,$store['locale']);
		
		$updateQuery .= "UPDATE {$this->getTable('catalog_category_entity_text')} AS dest,
			(
			SELECT *
			FROM {$this->getTable('nostress_export_enginecategory')} 
			WHERE taxonomy_code = '{$code}'
			AND locale = '{$locale}'
			) AS src
			SET dest.value = src.hash WHERE store_id = {$storeId} AND attribute_id = '{$attributeId}' AND dest.value = src.{$valueColumn};
			";
	}
}

$installer->run($updateQuery);

$installer->endSetup(); 
