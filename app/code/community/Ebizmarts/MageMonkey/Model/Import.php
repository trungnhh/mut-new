<?php

/**
 * Install script to add more attribute to store locator
 *
 * @category  Mut
 * @package   Clarkrubber_Locator
 * @author    menhoang <hoang.vjet@gmail.com>
 * @copyright 2016 ebluestore
 */
class Ebizmarts_MageMonkey_Model_Import extends Varien_Object
{
    protected $_headers = array();
    protected $_attributeModels = array();
    protected $_attributeOptions = array();

    protected $_skipped = array();
    protected $_emails = array();

    /**
     * Start assign map data
     */
    public function __construct()
    {
        //Mail;Anrede;Vorname;Nachname;Datum;Product;
        $this->mapper = array(
            'email' => 'e-Mail',
            'anrede' => 'anrede',
            'firstname' => 'vorname',
            'lastname' => 'nachname',
            'register_date' => 'datum',
            'product' => 'product'

        );
    }

    /**
     * Run import data
     * @return void
     */
    public function run()
    {
        $filepath =  Mage::getBaseDir().'/var/mt66-subscribe.csv';
        $i = 0;
        $currentIndex = Mage::getStoreConfig('mut/import/mailchimp');
        if (!$currentIndex) {
            $currentIndex = 0;
        }
        echo $currentIndex;
        if (($handle = fopen($filepath, 'r')) !== false) {
            while (($data = fgetcsv($handle, 3000, ',')) !== false) {
                if ($i >= $currentIndex) {
                    $data = array_map("utf8_encode", $data);
                    if ($i==0) {
                        $this->setHeaders($data);
                    } else {
                        Mage::getConfig()->saveConfig('mut/import/mailchimp', $i);
                        $this->saveFromCsv($this->parseCsv($data));
                    }
                }
                $i++;
            }

            if (count($this->_skipped)) {
                echo count($this->_skipped).' stores were skipped';
            }
        } else {
            Mage::getSingleton('adminhtml/session')->addError('There is some Error');
            $this->_redirect('*/*/index');
        }
    }

    /**
     * Save data from csv
     * @param array $data data from csv
     * @throws Exception
     * @return void
     */
    public function saveFromCsv($data)
    {
        //preprocess data to manipulate values where required
        $data = $this->preprocess($data);
        if (!Zend_Validate::is($data['e-mail'], 'EmailAddress')) {
            return false;
        } else {
            $prename = $data['anrede'];
            $prename = explode(' ', $prename);
            if (is_array($prename)) {
                $prename = $prename[count($prename)-1];
            } else {
                $prename = 'Herr';
            }
            if (in_array($data['e-mail'], $this->_emails)) {
                return false;
            } else {
                $this->_emails[] = $data['e-mail'];
            }
            //$api = Mage::getSingleton('monkey/api', array('store' => 1));
            $magentoGroup = 'General';
            /*if ($data['group'] != '') {
                $magentoGroup = $data['group'];
            }*/
            $listId = Mage::getStoreConfig(Ebizmarts_MageMonkey_Model_Config::GENERAL_LIST);
            $devgroupId = 3081;
            $livegroupId = 14057;
            $mergeVars = array(
                'EMAIL' => $data['e-mail'],
                'FNAME' => $data['vorname'],
                'LNAME' => $data['nachname'],
                'PRENAME' => $prename,
                'CGROUP' => $magentoGroup,
                'REGDATE' => $data['datum'],
                'PIMPORT' => $data['product'],
                'GROUIMPORT' => $data['group'],
                'SGMIMPORT' => $data['segment'],
                'MC_LANGUAGE' => 'de',
                'GROUPINGS' => array(
                    0 => array(
                        'id' => "14057", //You have to find the number via the API or group on list
                        'groups' => "Deals,Download-Deals,Partner-Angebote,Angebot der Woche,Neuerscheinungen"
                    )
                )
            );
            echo $data['e-mail'].PHP_EOL;
            $isOnMailChimp = Mage::helper('monkey')->subscribedToList($data['e-mail'], $listId);
            if ($isOnMailChimp) {
                Mage::getSingleton('monkey/api')->listUpdateMember($listId, $data['e-mail'], $mergeVars);
            } else {
                Mage::getSingleton('monkey/api')->listSubscribe($listId, $data['e-mail'], $mergeVars, 'html', false, TRUE);
            }
        }
        return true;

        foreach ($this->mapper as $att => $col) {
            switch ($this->getAttributeModel($att)->getFrontendInput()) {
                case 'select':
                    $loc->setData($att, $this->getSelectValue($att, $data[$col]));
                    break;
                case 'multiselect':
                    $loc->setData($att, $this->getMultiSelectValue($att, $data[$col]));
                    break;
                default:
                    $loc->setData($att, $data[$col]);
                    break;
            }
        }

        $loc->save();
        echo trim($data['email']).' saved'.PHP_EOL;
    }

    /**
     * Set Header data
     * @param array $data data from csv
     * @return void
     */
    public function setHeaders($data)
    {
        foreach ($data as $col) {
            $headName = str_replace(' ', '_', strtolower($col));
            if (strpos($headName, 'e-mail')) {
                $this->_headers[] = 'e-mail';
            } else {
                $this->_headers[] = str_replace(' ', '_', strtolower($col));

            }
        }
    }

    /**
     * Get Select value
     * @param string $attributeCode magento attribute code
     * @param string $label         option label
     * @return mixed
     */
    public function getSelectValue($attributeCode, $label)
    {
        foreach ($this->getAttributeOptions($attributeCode) as $option) {
            if ($option['label'] == $label) {
                return $option['value'];
            }
        }
    }

    /**
     * Get Multi Select value
     * @param string $attributeCode magento attribute code
     * @param string $label         option label
     * @return mixed
     */
    public function getMultiSelectValue($attributeCode, $label)
    {
        $values = array();

        if (strstr($label, ' , ')) {
            $labels = explode(' , ', $label);
        } elseif (strstr($label, ' or ')) {
            //specific to trackside data as sometimes is has " or " in place of commas
            $labels = explode(' or ', $label);
        } else {
            $labels[] = trim($label);
        }

        foreach ($labels as $label) {
            foreach ($this->getAttributeOptions($attributeCode) as $option) {
                if ($option['label'] == trim($label)) {
                    $values[] = $option['value'];
                }
            }
        }

        return implode(',', $values);
    }

    /**
     * Get attribute model
     * @param string $attributeCode magento attribute code
     * @return mixed
     */
    public function getAttributeModel($attributeCode)
    {
        if (!$this->_attributeModels[$attributeCode]) {
            $attributeModel = Mage::getModel('eav/entity_attribute');
            $id = $attributeModel->getIdByCode(Ak_Locator_Model_Location::ENTITY, $attributeCode);
            $this->_attributeModels[$attributeCode] = $attributeModel->load($id);
        }

        return $this->_attributeModels[$attributeCode];
    }

    /**
     * Get attribute options
     * @param string $attributeCode magento attribute code
     * @return array
     */
    public function getAttributeOptions($attributeCode)
    {

        if (!$this->_attributeOptions[$attributeCode]) {
            $this->_attributeOptions[$attributeCode] = $this->getAttributeModel($attributeCode)
                ->getSource()->getAllOptions(false);
        }
        return $this->_attributeOptions[$attributeCode];
    }


    /**
     * parse csv row to array with column header as key
     * @param array $data data from csv
     * @return array
     */
    public function parseCsv($data)
    {
        $storeData = array();

        $col = 0;
        foreach ($data as $value) {
            $storeData[$this->_headers[$col]] = trim($value);
            $col++;
        }

        return $storeData;
    }



    /**
     * Preprocess the row to manipulate any data
     *
     * @param array $data data process
     * @return mixed
     */
    public function preprocess($data)
    {
        foreach ($data as $key => $val) {
            if ($val == 'NULL') {
                $data[$key] = '';
            }
        }
        return $data;
    }

}
