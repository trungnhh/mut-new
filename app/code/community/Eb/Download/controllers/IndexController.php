<?php
class Eb_Download_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {

    }
    public function loginAction(){
        $productId = $this->getRequest()->getParam('product');
        $_product = Mage::getModel('catalog/product')->load($productId);
        $allowDownload = $_product->getData('allow_download_type');
        $groupId = Mage::getSingleton('customer/session')->getCustomerGroupId();
        $groupCode  = Mage::getModel('customer/group')->load($groupId)->getCustomerGroupCode();
        if (Mage::getSingleton('customer/session')->isLoggedIn() && $allowDownload == 'vip' && $groupCode != 'vip'){
            $currentUrl = $this->getRequest()->getParam('current_url');
            if($currentUrl == ''){
                $currentUrl = Mage::getBaseUrl();
            }
            Mage::getSingleton('core/session')->addError(
                Mage::helper('downloadable')->__('Your account not is vip.')
            );
            $this->_redirectUrl($currentUrl);

        } else {
            Mage::getSingleton('customer/session')->setProduct($this->getRequest()->getParam('product'));
            $this->_redirectUrl(Mage::getUrl('customer/account/login'));
        }
    }
    public function downloadAction(){
        $productId  =   $this->getRequest()->getParam('product');
        $product =  Mage::getModel('catalog/product')->load($productId);
        if($product->getId()){
            $link = '';
            $allowDownload = $product->getData('allow_download_type');
            $groupId = Mage::getSingleton('customer/session')->getCustomerGroupId();
            $groupCode  = Mage::getModel('customer/group')->load($groupId)->getCustomerGroupCode();
            $displayLink = false;
            if ($allowDownload == 'free') {
                $displayLink = true;
            }
            if($allowDownload == 'login') {
                if (Mage::getSingleton('customer/session')->isLoggedIn()){
                    $displayLink = true;
                }
            }
            if($allowDownload == 'vip') {
                if (Mage::getSingleton('customer/session')->isLoggedIn() && strtolower($groupCode)=='vip'){
                    $displayLink = true;
                }
            }
            if($displayLink == true) {
                $downloadLinks = Mage::getModel('downloadable/link')
                    ->getCollection()
                    ->addProductToFilter($product->getId());
                $downloadLink   =   $downloadLinks->getFirstItem();
                if(count($downloadLink->getData())){
                    $link = $downloadLink->getLinkFile();
                }
            }
            if($link==''){
                $message    =   $this->__("You haven't access to download this item.");
                Mage::getSingleton('core/session')->addError(Mage::helper('core')->escapeHtml($message));
                $currentUrl = $this->getRequest()->getParam('current_url');
                if($currentUrl == ''){
                    $currentUrl = Mage::getBaseUrl();
                }
                $this->_redirectUrl($currentUrl);
                return false;
            }
        }
        $resource = Mage::helper('downloadable/file')->getFilePath(
            Mage_Downloadable_Model_Link::getBasePath(), $link
        );
        $resourceType = Mage_Downloadable_Helper_Download::LINK_TYPE_FILE;
        try {
            $totalDownload = (int)$product->getData('total_download');
            $product->setData('total_download', $totalDownload+1);
            $product->save();
            $this->_processDownload($resource, $resourceType);
            exit(0);
        }
        catch (Exception $e) {
            Mage::getSingleton('core/session')->addError(
                Mage::helper('downloadable')->__('An error occurred while getting the requested content. Please contact the store owner.')
            );
        }
    }
    public function codeAction(){
        if($this->getRequest()->getPost()){
            $isbn   =   $this->getRequest()->getParam('download-code');
            $link   =   '';
            if($isbn){
                $productId  =   $this->getRequest()->getParam('product');
                $product =  Mage::getModel('catalog/product')->load($productId);
                if($product->getId()){
                    if($isbn==$product->getData('isbn_product')){
                        $downloadLinks = Mage::getModel('downloadable/link')
                            ->getCollection()
                            ->addProductToFilter($product->getId());
                        $downloadLink   =   $downloadLinks->getFirstItem();
                        if(count($downloadLink->getData())){
                            $link = $downloadLink->getLinkFile();
                        }
                    }
                    if($link==''){
                        $message    =   $this->__('Invalid ISBN code. Please recheck.');
                        Mage::getSingleton('core/session')->addError(Mage::helper('core')->escapeHtml($message));
                        $currentUrl = $this->getRequest()->getParam('current_url');
                        if($currentUrl == ''){
                            $currentUrl = Mage::getBaseUrl();
                        }
                        $this->_redirectUrl($currentUrl);
                        return false;
                    }
                }
            } else {
                $currentUrl = $this->getRequest()->getParam('current_url');
                if($currentUrl == ''){
                    $currentUrl = Mage::getBaseUrl();
                }
                $this->_redirectUrl($currentUrl);
            }

            $resource = Mage::helper('downloadable/file')->getFilePath(
                Mage_Downloadable_Model_Link::getBasePath(), $link
            );
            $resourceType = Mage_Downloadable_Helper_Download::LINK_TYPE_FILE;
            try {
                $totalDownload = (int)$product->getData('total_download');
                $product->setData('total_download', $totalDownload+1);
                $product->save();
                $this->_processDownload($resource, $resourceType);
                exit(0);
            }
            catch (Exception $e) {
                Mage::getSingleton('core/session')->addError(
                    Mage::helper('downloadable')->__('An error occurred while getting the requested content. Please contact the store owner.')
                );
            }
        }
    }
/*
    public function demoAction(){
        $link   =   $this->getRequest()->getParam('link');


        $resource = Mage::getBaseDir('media').DS.$link;

        $resourceType = Mage_Downloadable_Helper_Download::LINK_TYPE_FILE;
        try {
            $this->_processDownload($resource, $resourceType);
            exit(0);
        }
        catch (Exception $e) {
            $this->_getCustomerSession()->addError(
                Mage::helper('downloadable')->__('An error occurred while getting the requested content. Please contact the store owner.')
            );
        }
    }
*/
    protected function _processDownload($resource, $resourceType)
    {
        $helper = Mage::helper('downloadable/download');
        /* @var $helper Mage_Downloadable_Helper_Download */

        $helper->setResource($resource, $resourceType);

        $fileName       = $helper->getFilename();
        $contentType    = $helper->getContentType();

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
            ->setHeader('Content-type', $contentType, true);

        if ($fileSize = $helper->getFilesize()) {
            $this->getResponse()
                ->setHeader('Content-Length', $fileSize);
        }

        if ($contentDisposition = $helper->getContentDisposition()) {
            $this->getResponse()
                ->setHeader('Content-Disposition', $contentDisposition . '; filename='.$fileName);
        }
        $this->getResponse()
            ->clearBody();
        $this->getResponse()
            ->sendHeaders();
        header('Content-Disposition: attachment; filename=' . $fileName);
        session_write_close();
        $helper->output();
    }
    /**
     * Return customer session object
     *
     * @return Mage_Customer_Model_Session
     */
    protected function _getCustomerSession()
    {
        return Mage::getSingleton('customer/session');
    }
}