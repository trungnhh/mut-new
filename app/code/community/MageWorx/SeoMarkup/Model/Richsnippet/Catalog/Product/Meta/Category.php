<?php
/**
 * MageWorx
 * MageWorx SeoMarkup Extension
 * 
 * @category   MageWorx
 * @package    MageWorx_SeoMarkup
 * @copyright  Copyright (c) 2015 MageWorx (http://www.mageworx.com/)
 */


/**
 * @see MageWorx_SeoMarkup_Model_Catalog_Product_Richsnippet_Product
 */
class MageWorx_SeoMarkup_Model_Richsnippet_Catalog_Product_Meta_Category extends MageWorx_SeoMarkup_Model_Richsnippet_Catalog_Product_Abstract
{
    protected function _addAttributeForNodes(simple_html_dom_node $node)
    {
        $categories = $this->_product->getCategoryCollection()->exportToArray();
        $currentCategory = Mage::registry('current_category');
        $useDeepestCategory = Mage::helper('seomarkup/config')->isRichsnippetCategoryDeepest();

        if(is_object($currentCategory)){
            if(count($categories) > 1){
                if($useDeepestCategory){
                    $currentId = $currentCategory->getId();
                    $currentLevel = $currentCategory->getLevel();
                    if(is_numeric($currentLevel)){
                        foreach($categories as $category){
                            if($category['level'] > $currentLevel){
                                $currentId = $category['entity_id'];
                                $currentLevel = $category['level'];
                            }
                        }
                        if($currentId != $currentCategory->getId()){
                            $categoryName = $this->getCategoryNameById($currentId);
                        }
                    }
                }
            }
            if(empty($categoryName)){
                $categoryName = $currentCategory->getName();
            }
        }else{
            if($useDeepestCategory){
                if(count($categories) > 0){
                    $currentId = 0;
                    $currentLevel = 0;
                    if(is_numeric($currentLevel)){
                        foreach($categories as $category){
                            if($category['level'] >= $currentLevel){
                                $currentId = $category['entity_id'];
                                $currentLevel = $category['level'];
                            }
                        }
                        if($currentId){
                            $categoryName = $this->getCategoryNameById($currentId);
                        }
                    }
                }else{
                    $categoryName = false;
                }
            }else{
                $categoryName = false;
            }
        }

        if (!empty($categoryName)) {
            $node->innertext = $node->innertext . '<meta itemprop="category" content="' . $categoryName . '">' . "\n";
            return true;
        }
        return false;
    }

    function getCategoryNameById($id){
        if($id){
            $category = Mage::getModel('catalog/category')->load($id);
            if(is_object($category)){
                return $category->getName();
            }
        }
        return false;
    }

    protected function _getItemConditions()
    {
        return array("*[itemtype=http://schema.org/Offer]");
    }

    protected function _checkBlockType()
    {
        return true;
    }

    protected function _isValidNode(simple_html_dom_node $node)
    {
        return true;
    }

}