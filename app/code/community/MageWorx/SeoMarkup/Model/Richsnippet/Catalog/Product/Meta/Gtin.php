<?php
/**
 * MageWorx
 * MageWorx SeoMarkup Extension
 * 
 * @category   MageWorx
 * @package    MageWorx_SeoMarkup
 * @copyright  Copyright (c) 2015 MageWorx (http://www.mageworx.com/)
 */


/**
 * @see MageWorx_SeoMarkup_Model_Catalog_Product_Richsnippet_Product
 */
class MageWorx_SeoMarkup_Model_Richsnippet_Catalog_Product_Meta_Gtin extends MageWorx_SeoMarkup_Model_Richsnippet_Catalog_Product_Abstract
{
    protected function _addAttributeForNodes(simple_html_dom_node $node)
    {
        $attributeCode = Mage::helper('seomarkup/config')->getGtinAttributeCode();
        if($attributeCode){
            $gtinValue = Mage::helper('seomarkup')->getAttributeValueByCode($this->_product, $attributeCode);
            if(preg_match('/^[0-9]+$/', $gtinValue)){
                if(strlen($gtinValue) == 8){
                    $gtinType = 'gtin8';
                }elseif(strlen($gtinValue) == 12){
                    $gtinValue = '0' . $gtinValue;
                    $gtinType = 'gtin13';
                }elseif(strlen($gtinValue) == 13){
                    $gtinType = 'gtin13';
                }elseif(strlen($gtinValue) == 14){
                    $gtinType = 'gtin14';
                }
            }

            if(!empty($gtinType)){

                $node->innertext = $node->innertext . '<meta itemprop="' . $gtinType . '" content="' . $gtinValue. '">' . "\n";
                return true;
            }
        }
        return false;
    }

    protected function _getItemConditions()
    {
        return array("*[itemtype=http://schema.org/Product]");
    }

    protected function _checkBlockType()
    {
        return true;
    }

    protected function _isValidNode(simple_html_dom_node $node)
    {
        return true;
    }

}