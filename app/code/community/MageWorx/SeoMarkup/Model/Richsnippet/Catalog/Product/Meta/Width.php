<?php
/**
 * MageWorx
 * MageWorx SeoMarkup Extension
 * 
 * @category   MageWorx
 * @package    MageWorx_SeoMarkup
 * @copyright  Copyright (c) 2015 MageWorx (http://www.mageworx.com/)
 */


/**
 * @see MageWorx_SeoMarkup_Model_Catalog_Product_Richsnippet_Product
 */
class MageWorx_SeoMarkup_Model_Richsnippet_Catalog_Product_Meta_Width extends MageWorx_SeoMarkup_Model_Richsnippet_Catalog_Product_Abstract
{
    protected function _addAttributeForNodes(simple_html_dom_node $node)
    {
        $attributeCode = Mage::helper('seomarkup/config')->getWidthAttributeCode();
        if($attributeCode){
            $width = $this->_getWidth($attributeCode);
            if($width){
                $node->innertext = $node->innertext . '<meta itemprop="width" content="'. $width. '">' . "\n";
                return true;
            }
        }
        return false;
    }

    protected function _getWidth($attributeCode)
    {
        $widthValue = Mage::helper('seomarkup')->getAttributeValueByCode($this->_product, $attributeCode);
        $unit = Mage::helper('seomarkup/config')->getRichsnippetDimensionsUnit();
        if($widthValue){
            if(is_numeric($widthValue) && $unit){
                return $widthValue . ' ' . $unit;
            }elseif(preg_match('/^([0-9]+[\s]+[a-zA-Z]+)$/', $widthValue)){
                return $widthValue;
            }
            return false;
        }
    }

    protected function _getItemConditions()
    {
        return array("*[itemtype=http://schema.org/Product]");
    }

    protected function _checkBlockType()
    {
        return true;
    }

    protected function _isValidNode(simple_html_dom_node $node)
    {
        return true;
    }

}