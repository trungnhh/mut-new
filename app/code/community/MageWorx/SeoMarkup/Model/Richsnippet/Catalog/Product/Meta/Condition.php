<?php
/**
 * MageWorx
 * MageWorx SeoMarkup Extension
 * 
 * @category   MageWorx
 * @package    MageWorx_SeoMarkup
 * @copyright  Copyright (c) 2015 MageWorx (http://www.mageworx.com/)
 */


/**
 * @see MageWorx_SeoMarkup_Model_Catalog_Product_Richsnippet_Product
 */
class MageWorx_SeoMarkup_Model_Richsnippet_Catalog_Product_Meta_Condition extends MageWorx_SeoMarkup_Model_Richsnippet_Catalog_Product_Abstract
{
    protected function _addAttributeForNodes(simple_html_dom_node $node)
    {
        $attributeCode = Mage::helper('seomarkup/config')->getConditionAttributeCode();
        $conditionByDefault = Mage::helper('seomarkup/config')->getConditionDefaultValue();

        if($attributeCode){
            $conditionValue = Mage::helper('seomarkup')->getAttributeValueByCode($this->_product, $attributeCode);
            switch($conditionValue){
                case Mage::helper('seomarkup/config')->getConditionValueForNew():
                    $node->innertext = $node->innertext . '<meta itemprop="itemCondition" content="NewCondition">' . "\n";
                    return true;
                    break;
                case Mage::helper('seomarkup/config')->getConditionValueForUsed():
                    $node->innertext = $node->innertext . '<meta itemprop="itemCondition" content="UsedCondition">' . "\n";
                    return true;
                    break;
                 case Mage::helper('seomarkup/config')->getConditionValueForRefurbished():
                    $node->innertext = $node->innertext . '<meta itemprop="itemCondition" content="RefurbishedCondition">' . "\n";
                    return true;
                    break;
                case Mage::helper('seomarkup/config')->getConditionValueForDamaged():
                    $node->innertext = $node->innertext . '<meta itemprop="itemCondition" content="DamagedCondition">' . "\n";
                    return true;
                    break;
                default:
                    if($conditionByDefault){
                        $node->innertext = $node->innertext . '<meta itemprop="itemCondition" content="'. $conditionByDefault. '">' . "\n";
                        return true;
                    }
                    break;
            }
        }elseif($conditionByDefault){
             $node->innertext = $node->innertext . '<meta itemprop="itemCondition" content="'. $conditionByDefault. '">' . "\n";
             return true;
        }
        return false;
    }



    protected function _getItemConditions()
    {
        return array("*[itemtype=http://schema.org/Product]");
    }

    protected function _checkBlockType()
    {
        return true;
    }

    protected function _isValidNode(simple_html_dom_node $node)
    {
        return true;
    }

}