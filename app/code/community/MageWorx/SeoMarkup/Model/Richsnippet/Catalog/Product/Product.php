<?php
/**
 * MageWorx
 * MageWorx SeoMarkup Extension
 * 
 * @category   MageWorx
 * @package    MageWorx_SeoMarkup
 * @copyright  Copyright (c) 2015 MageWorx (http://www.mageworx.com/)
 */


/**
 * @see MageWorx_SeoMarkup_Block_Catalog_Product_View
 */
class MageWorx_SeoMarkup_Model_Richsnippet_Catalog_Product_Product extends MageWorx_SeoMarkup_Model_Richsnippet_Catalog_Product_Abstract
{
    protected $_imageUri                 = 'seomarkup/richsnippet_catalog_product_image';
    protected $_descriptionUri           = 'seomarkup/richsnippet_catalog_product_description';
    protected $_metaDescriptionUri       = 'seomarkup/richsnippet_catalog_product_meta_description';
    protected $_metaSkuUri               = 'seomarkup/richsnippet_catalog_product_meta_sku';
    protected $_metaPaymentUri           = 'seomarkup/richsnippet_catalog_product_meta_payment';
    protected $_metaDeliveryUri          = 'seomarkup/richsnippet_catalog_product_meta_delivery';
    protected $_metaColorUri             = 'seomarkup/richsnippet_catalog_product_meta_color';
    protected $_metaManufacturerUri      = 'seomarkup/richsnippet_catalog_product_meta_manufacturer';
    protected $_metaBrandUri             = 'seomarkup/richsnippet_catalog_product_meta_brand';
    protected $_metaModelUri             = 'seomarkup/richsnippet_catalog_product_meta_model';
    protected $_metaGtinUri              = 'seomarkup/richsnippet_catalog_product_meta_gtin';
    protected $_metaHeightUri            = 'seomarkup/richsnippet_catalog_product_meta_height';
    protected $_metaWidthUri             = 'seomarkup/richsnippet_catalog_product_meta_width';
    protected $_metaDepthUri             = 'seomarkup/richsnippet_catalog_product_meta_depth';
    protected $_metaWeightUri            = 'seomarkup/richsnippet_catalog_product_meta_weight';
    protected $_metaConditionUri         = 'seomarkup/richsnippet_catalog_product_meta_condition';
    protected $_metaSellerUri            = 'seomarkup/richsnippet_catalog_product_meta_seller';
    protected $_metaCategoryUri          = 'seomarkup/richsnippet_catalog_product_meta_category';
    protected $_availabilityByContentUri = 'seomarkup/richsnippet_catalog_product_availability';

    protected function _addAttributeForNodes(simple_html_dom_node $node)
    {
        $commonNode = $this->_findCommonContainer($node);
        $parentNode = $this->_findParentContainer($node);

        if ($commonNode && $parentNode) {
            $commonNode->itemtype  = "http://schema.org/Product";
            $commonNode->itemscope = "";
            $parentNode->itemprop  = "name";
            return true;
        }
        return false;
    }

    protected function _afterRender()
    {
        //Availability must be first!
        $availability = Mage::getModel($this->_availabilityByContentUri);
        if (!$availability->render($this->_html, $this->_block)) {
            $this->_errorRenderer(Mage::helper('seomarkup/logger')->__("Product availability property wasn't added."));
        }

        //Category must be second!
        if (Mage::helper('seomarkup/config')->isRichsnippetCategoryEnabled()) {
            $category = Mage::getModel($this->_metaCategoryUri);
            if (!$category->render($this->_html, $this->_block)) {
                $this->_errorRenderer(Mage::helper('seomarkup/logger')->__("Category information wasn't added."));
            }
        }

        if (Mage::helper('seomarkup/config')->isRichsnippetPaymentEnabled()) {
            $metaPayment = Mage::getModel($this->_metaPaymentUri);
            if (!$metaPayment->render($this->_html, $this->_block)) {
                $this->_errorRenderer(Mage::helper('seomarkup/logger')->__("Product payment methods wasn't added."));
            }
        }

        if (Mage::helper('seomarkup/config')->isRichsnippetDeliveryEnabled()) {
            $metaDelivery = Mage::getModel($this->_metaDeliveryUri);
            if (!$metaDelivery->render($this->_html, $this->_block)) {
                $this->_errorRenderer(Mage::helper('seomarkup/logger')->__("Product delivery methods wasn't added."));
            }
        }

        $image = Mage::getModel($this->_imageUri);
        if (!$image->render($this->_html, $this->_block)) {
            $this->_errorRenderer(Mage::helper('seomarkup/logger')->__("Product image property wasn't added."));
        }

        $description = Mage::getModel($this->_descriptionUri);
        if (!$description->render($this->_html, $this->_block)) {
            $metaDescription = Mage::getModel($this->_metaDescriptionUri);
            if ($metaDescription->render($this->_html, $this->_block)) {
                $this->_errorRenderer(Mage::helper('seomarkup/logger')->__("Product description property wasn't added."));
            }
        }

        if (Mage::helper('seomarkup/config')->isRichsnippetSellerEnabled()) {
            $seller = Mage::getModel($this->_metaSellerUri);
            if (!$seller->render($this->_html, $this->_block)) {
                $this->_errorRenderer(Mage::helper('seomarkup/logger')->__("Seller info information wasn't added."));
            }
        }

        if (Mage::helper('seomarkup/config')->isRichsnippetSkuEnabled()) {
            $metaSku = Mage::getModel($this->_metaSkuUri);
            if (!$metaSku->render($this->_html, $this->_block)) {
                $this->_errorRenderer(Mage::helper('seomarkup/logger')->__("Product sku property wasn't added."));
            }
        }

        if (Mage::helper('seomarkup/config')->isRichsnippetManufacturerEnabled()) {
            $metaManufacturer = Mage::getModel($this->_metaManufacturerUri);
            if (!$metaManufacturer->render($this->_html, $this->_block)) {
                $this->_errorRenderer(Mage::helper('seomarkup/logger')->__("Product manufacturer property wasn't added."));
            }
        }

        if (Mage::helper('seomarkup/config')->isRichsnippetBrandEnabled()) {
            $metaBrand = Mage::getModel($this->_metaBrandUri);
            if (!$metaBrand->render($this->_html, $this->_block)) {
                $this->_errorRenderer(Mage::helper('seomarkup/logger')->__("Product brand property wasn't added."));
            }
        }

        if (Mage::helper('seomarkup/config')->isRichsnippetModelEnabled()) {
            $metaModel = Mage::getModel($this->_metaModelUri);
            if (!$metaModel->render($this->_html, $this->_block)) {
                $this->_errorRenderer(Mage::helper('seomarkup/logger')->__("Product model property wasn't added."));
            }
        }

        if (Mage::helper('seomarkup/config')->isRichsnippetGtinEnabled()) {
            $metaGtin = Mage::getModel($this->_metaGtinUri);
            if (!$metaGtin->render($this->_html, $this->_block)) {
                $this->_errorRenderer(Mage::helper('seomarkup/logger')->__("Product gtin property wasn't added."));
            }
        }

        if (Mage::helper('seomarkup/config')->isRichsnippetHeightEnabled()) {
            $metaHeight = Mage::getModel($this->_metaHeightUri);
            if (!$metaHeight->render($this->_html, $this->_block)) {
                $this->_errorRenderer(Mage::helper('seomarkup/logger')->__("Product height property wasn't added."));
            }
        }

        if (Mage::helper('seomarkup/config')->isRichsnippetWidthEnabled()) {
            $metaWidth = Mage::getModel($this->_metaWidthUri);
            if (!$metaWidth->render($this->_html, $this->_block)) {
                $this->_errorRenderer(Mage::helper('seomarkup/logger')->__("Product width property wasn't added."));
            }
        }

        if (Mage::helper('seomarkup/config')->isRichsnippetDepthEnabled()) {
            $metaDepth = Mage::getModel($this->_metaDepthUri);
            if (!$metaDepth->render($this->_html, $this->_block)) {
                $this->_errorRenderer(Mage::helper('seomarkup/logger')->__("Product depth property wasn't added."));
            }
        }

        if (Mage::helper('seomarkup/config')->isRichsnippetWeightEnabled()) {
            $metaWeight = Mage::getModel($this->_metaWeightUri);
            if (!$metaWeight->render($this->_html, $this->_block)) {
                $this->_errorRenderer(Mage::helper('seomarkup/logger')->__("Product weight property wasn't added."));
            }
        }

        if (Mage::helper('seomarkup/config')->isRichsnippetConditionEnabled()) {
            $metaCondition = Mage::getModel($this->_metaConditionUri);
            if (!$metaCondition->render($this->_html, $this->_block)) {
                $this->_errorRenderer(Mage::helper('seomarkup/logger')->__("Product condition property wasn't added."));
            }
        }

        return true;
    }

    protected function _beforeInit($html)
    {
        if (!is_object($html)) {
            $html = $this->_magentoHtmlFix($html);
        }
        return parent::_beforeInit($html);
    }

    protected function _beforeRender($html)
    {
        $priceReport = $this->_getPriceReportObject();
        if (is_object($priceReport) && $priceReport->getStatus() == 'success') {
            return parent::_beforeRender($html);
        }
        else {
            $res = $this->_renderPrice();
            if ($res != false) {
                return parent::_beforeRender($html);
            }
        }
        return false;
    }

    protected function _renderPrice()
    {
        $price = Mage::helper('seomarkup')->getPriceByProductType($this->_product->getTypeID());
        if(!$price){        	
        	$this->_errorRenderer(Mage::helper('seosuite/richsnippet')->__("Unknow product type. Richsnippets were disabled."));
        	return false;
        }
        
        $res = $price->render($this->_html, $this->_block, false);
        if ($res != false) {
            return true;
        }
        return false;
    }

    /**
     * Find highest common container for nested items: offer and rating
     * @param simple_html_dom_node $node
     * @return simple_html_dom_node | false
     */
    protected function _findCommonContainer(simple_html_dom_node $node)
    {
        $priceFlag  = false;
        //If the rating wasn't added that generation proceeds...
        $ratingFlag = true;
        if (is_object($this->_getAggregateRatingReportObject())) {
            if ($this->_getAggregateRatingReportObject()->getStatus() == "success") {
                $ratingFlag = false;
            }
        }

        $node = clone $node;
        while ($node = $node->parent) {
            if ($node->tag == "root") {
                return false;
            }
            if (in_array($node->tag, $this->_container)) {
                if (!$ratingFlag) {
                    $resultRating = $node->find('*[itemprop=aggregateRating]');
                    if (is_array($resultRating) && count($resultRating)) {
                        $ratingFlag = true;
                    }
                }
                if (!$priceFlag) {
                    $resultPrice = $node->find('*[itemprop="offers"]');
                    if (is_array($resultPrice) && count($resultPrice)) {
                        $priceFlag = true;
                    }
                }
                if ($ratingFlag && $priceFlag && $node->parent->tag == 'root') {
                    return $node;
                }
            }
        }
        return false;
    }

    protected function _isValidNode(simple_html_dom_node $node)
    {
        //will be product name property
        $parentNode = $this->_findParentContainer($node);
        if (!$parentNode) {
            return false;
        }
        //will be main product item
        if (!$this->_isNotInsideTypes($node)) {
            return false;
        }

        if (!$this->_findCommonContainer($parentNode)) {
            return false;
        }
        return $node;
    }

    /**
     * @return Varien_Object or false
     */
    protected function _getAggregateRatingReportObject()
    {
//        return new Varien_Object(array('status'=>'success', 'tag'=>'div'));
        return Mage::registry('mageworx_richsnippet_aggregate_rating_report');
    }

    /**
     * @return Varien_Object or false
     */
    protected function _getPriceReportObject()
    {
        return Mage::registry('mageworx_richsnippet_price_report');
    }

    protected function _checkBlockType()
    {
        return true;
    }

    protected function _getItemValues()
    {
        return array(Mage::helper('catalog/output')->productAttribute($this->_product, $this->_product->getName(), $this->_product->getName(), 'name'));
    }

    protected function _magentoHtmlFix($html)
    {
        /*
         * Magento code without space between |name="bundle_option[*]"| and |value="*"|
         * Html parser crop |value="*"|
         */

        /*
         * Example code:
          <div class="input-box">
          <ul class="options-list">
          <li><input type="radio" onclick="bundle.changeSelection(this)"
          class="radio validate-one-required-by-name change-container-classname"
          id="bundle-option-20-54" name="bundle_option[20]"value="54"/>
         */

        if($this->_product->getTypeId() == 'bundle'){
            $html = str_replace("\"value=\"", "\" value=\"", $html);
        }
        return $html;
    }

}