<?php
/**
 * MageWorx
 * MageWorx SeoMarkup Extension
 * 
 * @category   MageWorx
 * @package    MageWorx_SeoMarkup
 * @copyright  Copyright (c) 2015 MageWorx (http://www.mageworx.com/)
 */


class MageWorx_SeoMarkup_Model_Richsnippet_Catalog_Product_Price_Grouped extends MageWorx_SeoMarkup_Model_Richsnippet_Catalog_Product_Price_Abstract
{
    protected $_validViewBlockType = 'Mage_Catalog_Block_Product_View_Type_Grouped';

    /**
     * Return array prices (include tax / exclude tax sorted by priority)
     * of associated product have minimal price or itself prices.
     * @return array
     */
    protected function _getItemValues($product = null)
    {
        $associatedProducts = $this->_product->getTypeInstance(true)->getAssociatedProducts($this->_product);        
                
        if (count($associatedProducts)) {
            $allProductPrices = array();
            foreach ($associatedProducts as $product) {
                $productPrices = $this->_getChildItemValues($product);
                $allProductPrices[(string) $productPrices[0]] = $productPrices;
            }

            if (count($allProductPrices)) {
                ksort($allProductPrices);
                $minPrice = 0;
                $k = 0;
                foreach ($allProductPrices as $itemPrice => $itemDataPrice) {
                    ksort($itemDataPrice);
                    $priceData = (float)array_shift($itemDataPrice);
                    if ($minPrice == 0 || $priceData<$minPrice) {
                        $minPrice = $priceData;
                        $k = $itemPrice;
                    }
                }
                //return array_shift($allProductPrices);
                if ($k!=0) {
                    return $allProductPrices[$k];
                } else{
                    return array_shift($allProductPrices);
                }
            }
        }
        return parent::_getItemValues();
    }

    protected function _getChildItemValues($_product = null)
    {
        if (!$_product) {
            $_product = $this->_product;
        }

        $prices = Mage::helper('seomarkup')->getDefaultPrices($_product);

        $modPrices = array();
        if(is_array($prices)){
            foreach ($prices as $price) {
                $modPrices = array_merge($modPrices, $this->_getModifyPrices($price));
            }
        }
        ksort($modPrices);
        return array_unique($modPrices);
    }

    protected function _checkBlockType()
    {
        return true;
    }
}