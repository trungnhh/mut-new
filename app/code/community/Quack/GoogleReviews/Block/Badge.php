<?php
class Quack_GoogleReviews_Block_Badge extends Mage_Core_Block_Template
{
    /**
     * Badge is enable
     * @return bool
     */
    public function isBadgeEnable() {
        return Mage::helper('googlereviews')->isGoogleBadgeReviewsEnable();
    }

    /**
     * Is ga available
     *
     * @return bool
     */
    public function isAvailable()
    {
        return Mage::helper('googlereviews')->isGoogleReviewsAvailable();
    }

}
