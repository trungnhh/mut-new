<?php
include_once 'app/Mage.php';
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$downloadableCollection = Mage::getModel('catalog/product')
    ->getCollection()
    ->addAttributeToFilter('type_id', 'downloadable');
$downloadableIds = array();
foreach ($downloadableCollection as $product) {
    $downloadableIds[] = $product->getId();
}
$sql = "UPDATE cataloginventory_stock_item SET manage_stock = 0, use_config_manage_stock = 0 WHERE product_id IN(" . join(",", $downloadableIds) . ")";
$writeConnection = Mage::getSingleton('core/resource')->getConnection('core_write');
$writeConnection->query($sql);
echo "complete!";