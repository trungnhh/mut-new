<?php

require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'abstract.php';
class Mage_Shell_Mut_ImportOldCustomerToMailChimp extends Mage_Shell_Abstract
{
    /**
     * Run script
     * @return void
     */
    public function run()
    {
        if ($this->getArg('run')) {
            $importer = new Ebizmarts_MageMonkey_Model_ImportFromOldDb();
            $importer->run();
            echo 'Import subscribe completely!';
        } else {
            echo $this->usageHelp();
        }
    }


    /**
     * Retrieve Usage Help Message
     * @return string
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php importOldCustomerToMailChimp.php -- [options]
    --run run export script

USAGE;
    }
}

$shell = new Mage_Shell_Mut_ImportOldCustomerToMailChimp();
$shell->run();