<?php
ini_set('max_execution_time', 72000); //300 seconds = 5 minutes
require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'abstract.php';
class Mage_Shell_Mut_UpdateSalutation extends Mage_Shell_Abstract
{
    protected $_headers = array();

    /**
     * Run script
     * @return void
     */
    public function run()
    {
        if ($this->getArg('run')) {
            $helperMailjet = new Mailjet_Iframes_Helper_SyncManager();
            $apiOverlay = $helperMailjet->getApiInstance();
            $sync = new Mailjet_Iframes_Helper_SynchronizationCustom($apiOverlay);
            $updateOnlyGiven = false;
            $filepath =  Mage::getBaseDir().'/var/importexport/mj_7894.csv';
            $i = 0;
            if (($handle = fopen($filepath, 'r')) !== false) {
                while (($data = fgetcsv($handle, 3000, ',')) !== false) {
                    if ($i==0) {
                        $this->setHeaders($data);
                    } else {
                        $data = $this->parseCsv($data);
                        $email = $data['email'];
                        if (isset($data['salutation'])) {
                            if ($data['salutation'] == '1' || $data['salutation'] == '2') {
                                $salutation = 'Frau';
                                if ($data['salutation'] == '2') {
                                    $salutation = 'Herr';
                                }
                                $contactData = array(
                                    'email' => $email,
                                    'salutation' => $salutation,
                                );

                                $subscribersData = array();
                                $subscribersData[] = $contactData;
                                try {
                                    $result = $sync->synchronize($subscribersData, $updateOnlyGiven);
                                    echo 'Update success email: '.$email.PHP_EOL;
                                } catch (Exception $e) {
                                    echo $e->getMessage().PHP_EOL;
                                }
                            }
                        }
                        /*
                        $dataProperties = $sync->getContactData($email);
                        if ($dataProperties) {
                            foreach ($dataProperties as $dataPropertie) {
                                if ($dataPropertie->Name == 'salutation') {
                                    if ($dataPropertie->Value == '1' || $dataPropertie->Value == '2') {
                                        // Update Salutation Here
                                        $salutation = 'Frau';
                                        if ($dataPropertie->Value == '2') {
                                            $salutation = 'Herr';
                                        }
                                        $contactData = array(
                                            'email' => $email,
                                            'salutation' => $salutation,
                                        );
                                        $subscribersData = array();
                                        $subscribersData[] = $contactData;
                                        try {
                                            $result = $sync->synchronize($subscribersData, $updateOnlyGiven);
                                            echo 'Update success email: '.$email.PHP_EOL;
                                        } catch (Exception $e) {
                                            echo $e->getMessage().PHP_EOL;
                                        }
                                        echo $email.PHP_EOL;
                                    } else {
                                        //echo 'SKIP: '.$email.PHP_EOL;
                                    }
                                    break;
                                }
                            }
                        }
                        */
                    }
                    echo $i.PHP_EOL;
                    $i++;
                }
            } else {
                echo 'Can not get email data on csv';
            }
            echo 'Update subscribe completely!';
        } else {
            echo $this->usageHelp();
        }
    }

    /**
     * Set Header data
     * @param array $data data from csv
     * @return void
     */
    public function setHeaders($data)
    {
        foreach ($data as $col) {
            if (strpos($col, 'email') !== false) {
                $col = 'email';
            }
            $this->_headers[] = str_replace(' ', '_', strtolower($col));
        }

    }

    /**
     * parse csv row to array with column header as key
     * @param array $data data from csv
     * @return array
     */
    public function parseCsv($data)
    {
        $storeData = array();

        $col = 0;
        foreach ($data as $value) {
            $storeData[$this->_headers[$col]] = trim($value);
            $col++;
        }

        return $storeData;
    }

    /**
     * Retrieve Usage Help Message
     * @return string
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php updateSalutation.php -- [options]
    --run run export script

USAGE;
    }
}

$shell = new Mage_Shell_Mut_UpdateSalutation();
$shell->run();
