<?php

require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'abstract.php';
class Mage_Shell_Mut_SyncImport extends Mage_Shell_Abstract
{
    /**
     * Run script
     * @return void
     */
    public function run()
    {
        if ($this->getArg('run')) {
            $model = Mage::getModel('monkey/cron')->bulksyncImportSubscribers();
        } else {
            echo $this->usageHelp();
        }
    }


    /**
     * Retrieve Usage Help Message
     * @return string
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php syncImport.php -- [options]
    --run run export script

USAGE;
    }
}

$shell = new Mage_Shell_Mut_SyncImport();
$shell->run();
