<?php

require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'abstract.php';
class Mage_Shell_Mut_UpdateEbayIsbnEan extends Mage_Shell_Abstract
{
    protected $_booksAttr = array();
    protected $_otherAttr = array();
    /**
     * Run script
     * @return void
     */
    public function run()
    {
        if ($this->getArg('run')) {
            $this->getAllAttributeSet();
            echo 'Start update Book Ebay ISBN!'.PHP_EOL;
            $collection = Mage::getModel('catalog/product')
                ->getCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('attribute_set_id',$this->_booksAttr)
                ->setOrder('entity_id', 'asc');
            foreach ($collection as $product) {
                try {
                    if ($product->getAttributeText('isbn') !=''
                        && $product->getAttributeText('isbn')!=null
                        && $product->getAttributeText('isbn') !=' ') {
                        $product->getAttributeText('isbn').PHP_EOL;
                        $product->getData('isbn_product').PHP_EOL;
                        $isbn = $product->getAttributeText('isbn').'-'.$product->getData('isbn_product');
                        $product->setEbayIsbn($isbn);
                        $product->getResource()->saveAttribute($product, 'ebay_isbn');
                        Mage::log('Update product: '.$product->getId(), null, 'update_book_ebay_isbn.log');
                    } else {
                        Mage::log('Skip product: '.$product->getId(), null, 'update_book_ebay_isbn.log');
                    }
                } catch (Exception $e) {
                    Mage::log('ERROR Update product: '.$product->getId(). 'MSG:'.$e->getMessage(), null, 'update_book_ebay_isbn.log');
                }
            }
            echo 'Done Book Ebay ISBN!'.PHP_EOL;

            /********************************************/

            echo 'Start update Ebay EAN!'.PHP_EOL;
            $collection = Mage::getModel('catalog/product')
                ->getCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('attribute_set_id',$this->_otherAttr)
                ->setOrder('entity_id', 'asc');
            foreach ($collection as $product) {
                try {
                    if ($product->getAttributeText('isbn') !=''
                        && $product->getAttributeText('isbn')!=null
                        && $product->getAttributeText('isbn') !=' ') {
                        $product->getAttributeText('isbn').PHP_EOL;
                        $product->getData('isbn_product').PHP_EOL;
                        $isbn = $product->getAttributeText('isbn').'-'.$product->getData('isbn_product');
                        $product->setEbayEan($isbn);
                        $product->getResource()->saveAttribute($product, 'ebay_ean');
                        Mage::log('Update product: '.$product->getId(), null, 'update_other_ebay_ean.log');
                    } else {
                        Mage::log('Skip product: '.$product->getId(), null, 'update_other_ebay_ean.log');
                    }
                } catch (Exception $e) {
                    Mage::log('ERROR Update product: '.$product->getId(). 'MSG:'.$e->getMessage(), null, 'update_other_ebay_ean.log');
                }
            }
            echo 'Done Ebay EAN!'.PHP_EOL;

        } else {
            echo $this->usageHelp();
        }
    }

    public function getAllAttributeSet() {
        $entityType = Mage::getModel('catalog/product')->getResource()->getTypeId();
        $collection = Mage::getResourceModel('eav/entity_attribute_set_collection')->setEntityTypeFilter($entityType);
        $attributeSet = array();
        //$attributeSet[0] = $this->__('ALL');
        foreach($collection as $coll){
            //$attributeSet[$coll->getAttributeSetId()] = $coll->getAttributeSetName();
            $attributeSet[] = array(
                'label' => $coll->getAttributeSetName(),
                'value' => $coll->getAttributeSetId()
            );
            if (strtolower($coll->getAttributeSetName())=='buch' || strtolower($coll->getAttributeSetName())=='e-book') {
                $this->_booksAttr[] = $coll->getAttributeSetId();
            } else {
                $this->_otherAttr[] = $coll->getAttributeSetId();
            }
        }
        return $attributeSet;
    }


    /**
     * Retrieve Usage Help Message
     * @return string
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php updateEbayISBNEAN.php -- [options]
    --run run update script

USAGE;
    }
}

$shell = new Mage_Shell_Mut_UpdateEbayIsbnEan();
$shell->run();