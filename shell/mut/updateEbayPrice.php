<?php

require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'abstract.php';
class Mage_Shell_Mut_UpdateEbayPrice extends Mage_Shell_Abstract
{
    /**
     * Run script
     * @return void
     */
    public function run()
    {
        if ($this->getArg('run')) {
            echo 'Start update!'.PHP_EOL;
            $collection = Mage::getModel('catalog/product')
                ->getCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('ebay_price',
                    array(
                    array('eq' => ''),
                    array('null' => true),
                ))
                ->setOrder('entity_id', 'asc');
            foreach ($collection as $product) {
                try {
                    $product->setEbayPrice($product->getPrice());
                    $product->getResource()->saveAttribute($product, 'ebay_price');
                    Mage::log('Update product: '.$product->getId(), null, 'update_ebay_price.log');
                } catch (Exception $e) {
                    Mage::log('ERROR Update product: '.$product->getId(). 'MSG:'.$e->getMessage(), null, 'update_ebay_price.log');
                }
            }
            echo 'Done!'.PHP_EOL;
        } else {
            echo $this->usageHelp();
        }
    }


    /**
     * Retrieve Usage Help Message
     * @return string
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php updateEbayPrice.php -- [options]
    --run run update script

USAGE;
    }
}

$shell = new Mage_Shell_Mut_UpdateEbayPrice();
$shell->run();