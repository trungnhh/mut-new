<?php

require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'abstract.php';
class Mage_Shell_Mut_Mailjet extends Mage_Shell_Abstract
{
    /**
     * Run script
     * @return void
     */
    public function run()
    {
        if ($this->getArg('run')) {
            echo date("Y-m-d H:i:s").PHP_EOL;
            $syncManager = new Mailjet_Iframes_Helper_SyncManager();
            $syncManager->synchronize();
            echo date("Y-m-d H:i:s").PHP_EOL;
        } else {
            echo $this->usageHelp();
        }
    }


    /**
     * Retrieve Usage Help Message
     * @return string
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php syncExport.php -- [options]
    --run start sync script

USAGE;
    }
}

$shell = new Mage_Shell_Mut_Mailjet();
$shell->run();
