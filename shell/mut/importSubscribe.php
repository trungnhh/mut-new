<?php
ini_set('max_execution_time', 72000); //300 seconds = 5 minutes
require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'abstract.php';
class Mage_Shell_Mut_Importsubscribe extends Mage_Shell_Abstract
{
    /**
     * Run script
     * @return void
     */
    public function run()
    {
        if ($this->getArg('run')) {
            $importer = new Ebizmarts_MageMonkey_Model_Import();
            $importer->run();
            echo 'Import subscribe completely!';
        } else {
            echo $this->usageHelp();
        }
    }


    /**
     * Retrieve Usage Help Message
     * @return string
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php importSubscribe.php -- [options]
    --run run export script

USAGE;
    }
}

$shell = new Mage_Shell_Mut_Importsubscribe();
$shell->run();
