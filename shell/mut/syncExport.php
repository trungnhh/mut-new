<?php

require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'abstract.php';
class Mage_Shell_Mut_SyncExport extends Mage_Shell_Abstract
{
    /**
     * Run script
     * @return void
     */
    public function run()
    {
        if ($this->getArg('run')) {
echo date("Y-m-d H:i:s");
            $model = Mage::getModel('monkey/cron')->bulksyncExportSubscribers();
echo date("Y-m-d H:i:s");     
   } else {
            echo $this->usageHelp();
        }
    }


    /**
     * Retrieve Usage Help Message
     * @return string
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php syncExport.php -- [options]
    --run run export script

USAGE;
    }
}

$shell = new Mage_Shell_Mut_SyncExport();
$shell->run();
