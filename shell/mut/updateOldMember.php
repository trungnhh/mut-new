<?php

require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'abstract.php';
class Mage_Shell_Mut_UpdateOldMember extends Mage_Shell_Abstract
{
    /**
     * Run script
     * @return void
     */
    public function run()
    {
        if ($this->getArg('run')) {
            $importer = new Ebizmarts_MageMonkey_Model_UpdateOldMember();
            $importer->run();
            echo 'Update Old subscribe from completely!';
        } else {
            echo $this->usageHelp();
        }
    }


    /**
     * Retrieve Usage Help Message
     * @return string
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php updateOldMember.php -- [options]
    --run run export script

USAGE;
    }
}

$shell = new Mage_Shell_Mut_UpdateOldMember();
$shell->run();